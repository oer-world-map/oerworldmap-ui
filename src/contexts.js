import React from "react";

export const ApiContext = React.createContext(null);
export const ConfigContext = React.createContext(null);
export const EmitterContext = React.createContext(null);
export const FilterContext = React.createContext(null);
export const FormContext = React.createContext(null);
export const I18nContext = React.createContext(null);
export const PathContext = React.createContext(null);
export const UserContext = React.createContext(null);
