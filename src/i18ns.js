import dotenv from "dotenv";
import getenv from "getenv";

dotenv.config();

const i18nConfig = getenv.multi({
  supportedLanguages: ["LANG_SUPPORTED", "en"],
  defaultLanguage: ["LANG_DEFAULT", "en"]
});

const supportedLanguages = i18nConfig.supportedLanguages.trim().split(/\s+/);
const defaultLanguage = i18nConfig.defaultLanguage;
const bundles = ["ui", "iso3166-1-alpha-2", "iso639-1", "iso3166-2", "labels", "descriptions"];
const vocabs = [
  "esc",
  "isced-1997",
  "licenses",
  "organizations",
  "persons",
  "projects",
  "sectors",
  "services",
  "activities",
  "policies"
];
const extractLabels = (concepts, language) =>
  concepts.reduce((acc, cur) => {
    const name = cur.name.find((name) => name["@language"] === language);
    name && (acc[cur["@id"]] = name["@value"]);
    cur.narrower && Object.assign(acc, extractLabels(cur.narrower, language));
    return acc;
  }, {});

const getI18n = (supported_languages) => {
  const i18ns = {};
  // If we use i18ns for the static pages we can't access the environment
  // variables from the .env file so we have to pass the environment variables
  // defined for Jekyll from the outside.
  // supported_languages => static pages (Jekyll env variables)
  // supportedLanguages => React application (env variables from .env file)
  const suppLanguages = supported_languages ?? supportedLanguages;
  suppLanguages.forEach((language) => {
    i18ns[language] = {
      descriptions: {}
    };
    bundles.forEach((bundle) => {
      const obj = {};
      try {
        Object.assign(obj, require(`../docs/_data/locale/${bundle}.json`));
      } catch (e) {
        console.error(`Missing i18n file ${bundle}.json`);
        return;
      }
      // FIXME: special case descriptions, refactor so that all l10ns are segmented by bundle name
      if (bundle === "descriptions") {
        Object.assign(i18ns[language].descriptions, obj);
      } else {
        Object.assign(i18ns[language], obj);
      }
    });
    vocabs.forEach((vocab) =>
      Object.assign(
        i18ns[language],
        extractLabels(require(`./json/${vocab}.json`).hasTopConcept, language)
      )
    );
  });

  // The language of the original files is already English so we skip this language.
  suppLanguages
    .filter((language) => language !== "en")
    .forEach((language) => {
      const i18n = JSON.parse(JSON.stringify(i18ns[defaultLanguage]));
      bundles.forEach((bundle) => {
        const obj = {};
        try {
          Object.assign(obj, require(`../docs/_data/locale/${bundle}_${language}.json`));
        } catch (e) {
          console.error(`Missing i18n file ${bundle}.json`);
          return;
        }
        // FIXME: special case descriptions, refactor so that all l10ns are segmented by bundle name
        if (bundle === "descriptions") {
          Object.assign(i18n.descriptions, obj);
        } else {
          Object.assign(i18n, obj);
        }
      });
      vocabs.forEach((vocab) =>
        Object.assign(i18n, extractLabels(require(`./json/${vocab}.json`).hasTopConcept, language))
      );
      i18ns[language] = i18n;
    });
  return i18ns;
};

export default getI18n;
