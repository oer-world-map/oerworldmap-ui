/* global localStorage */

import React, { useContext, useEffect, useState } from "react";

import ReactiveMap from "./ReactiveMap";
import FilterSidebar from "./FilterSidebar";
import ModeToggle from "./ModeToggle";
import SearchResults from "./SearchResults";
import Searchbar from "./Searchbar";

import withEmitter from "./withEmitter";
import withI18n from "./withI18n";
import { quickSearchTypes, urlParser } from "../common";
import { FilterContext } from "../contexts";

import "../styles/components/ResourceIndex.pcss";

const ReactiveResourceIndex = ({
  emitter,
  iso3166 = "",
  map,
  _self,
  phrases,
  region,
  view,
  translate
}) => {
  const url = urlParser(_self);
  const home = url.pathname.endsWith("/resource/");
  const initPins =
    typeof localStorage !== "undefined" && localStorage.getItem("showPins")
      ? localStorage.getItem("showPins") === "true"
      : true;

  const { getUrlParams } = useContext(FilterContext);

  const [collapsed, setCollapsed] = useState(true);
  const [showMobileFilters, setShowMobileFilters] = useState(false);
  const [params, setParams] = useState(getUrlParams());
  const [showFeatures, setShowFeatures] = useState(initPins);
  const [isClient, setIsClient] = useState(false);

  const handlePopState = () => setParams(getUrlParams());

  useEffect(() => {
    setIsClient(true);
    window.addEventListener("popstate", handlePopState);
    emitter.on("showFeatures", setShowFeatures);

    return () => {
      window.removeEventListener("popstate", handlePopState);
      emitter.off("showFeatures", setShowFeatures);
    };
  }, []);

  let subFilters = [
    {
      componentId: "filter.about.additionalType.@id",
      dataField: "about.additionalType.@id"
    },
    {
      componentId: "filter.about.primarySector.@id",
      dataField: "about.primarySector.@id"
    },
    {
      componentId: "filter.about.secondarySector.@id",
      dataField: "about.secondarySector.@id"
    },
    {
      componentId: "filter.about.keywords",
      dataField: "about.keywords",
      showMissing: true,
      translate: false,
      size: 9999
    },
    {
      componentId: "filter.about.award",
      dataField: "about.award"
    },
    {
      componentId: "filter.about.activityField.@id",
      dataField: "about.activityField.@id"
    },
    {
      componentId: "filter.about.focus",
      dataField: "about.focus"
    },
    {
      componentId: "filter.about.about.@id",
      dataField: "about.about.@id"
    },
    {
      componentId: "filter.about.license.@id",
      dataField: "about.license.@id"
    },
    {
      componentId: "filter.about.audience.@id",
      dataField: "about.audience.@id"
    },
    {
      componentId: "filter.about.scope.@id",
      dataField: "about.scope.@id"
    },
    {
      componentId: "filter.about.spatialCoverage",
      dataField: "about.spatialCoverage"
    },
    {
      componentId: "filter.about.inLanguage",
      dataField: "about.inLanguage"
    },
    {
      componentId: "filter.about.objectIn.@type",
      dataField: "about.objectIn.@type",
      hidden: true
    },
    {
      componentId: "filter.author.keyword",
      dataField: "author.keyword",
      hidden: true
    },
    {
      componentId: "filter.about.objectIn.agent.@id",
      dataField: "about.objectIn.agent.@id",
      hidden: true
    },
    {
      componentId: "filter.about.attendee.@id",
      dataField: "about.attendee.@id",
      hidden: true
    },
    {
      componentId: "filter.about.affiliate.@id",
      dataField: "about.affiliate.@id",
      hidden: true
    }
  ];

  if (
    !iso3166 &&
    !subFilters.find(
      (filter) => filter.componentId === "filter.feature.properties.location.address.addressCountry"
    )
  ) {
    subFilters.splice(1, 0, {
      componentId: "filter.feature.properties.location.address.addressCountry",
      dataField: "feature.properties.location.address.addressCountry",
      title: "country"
    });
  }

  if (
    !region &&
    !subFilters.find(
      (filter) => filter.componentId === "filter.feature.properties.location.address.addressRegion"
    )
  ) {
    subFilters.splice(2, 0, {
      componentId: "filter.feature.properties.location.address.addressRegion",
      dataField: "feature.properties.location.address.addressRegion",
      title: "filter.feature.properties.location.address.addressRegion"
    });
  }

  const filterIDs = ["q", "size", "filter.about.@type"].concat(
    subFilters.map((filter) => filter.componentId)
  );
  subFilters = subFilters.map((filter) => {
    filter.react = {
      and: filterIDs.filter((id) => id !== filter.componentId)
    };
    return filter;
  });

  return (
    <div
      id="reactive-resource-index"
      className={`mainContent ${params.view}View${collapsed ? " collapsed" : ""}`}
    >
      {isClient && (
        <ModeToggle
          _self={_self}
          viewHash={view}
          getUrlParams={getUrlParams}
          params={params}
          showMobileFilters={showMobileFilters}
          setShowMobileFilters={setShowMobileFilters}
        ></ModeToggle>
      )}
      {isClient && (
        <div className="content">
          <FilterSidebar
            filterIDs={filterIDs}
            iso3166={iso3166}
            region={region}
            setShowMobileFilters={setShowMobileFilters}
            showMobileFilters={showMobileFilters}
            handlePopState={handlePopState}
            showFeatures={showFeatures}
            collapsed={collapsed}
            setCollapsed={setCollapsed}
            subFilters={subFilters}
          ></FilterSidebar>
          <SearchResults
            filterIDs={filterIDs}
            translate={translate}
            iso3166={iso3166}
            params={params}
            region={region}
            showMobileFilters={showMobileFilters}
            subFilters={subFilters}
          ></SearchResults>
        </div>
      )}
      <ReactiveMap
        phrases={phrases}
        emitter={emitter}
        iso3166={iso3166}
        map={map}
        home={home}
        initPins={initPins}
        region={region}
        setCollapsed={setCollapsed}
      />
      {isClient && <Searchbar viewHash={view} />}
    </div>
  );
};

export default withEmitter(withI18n(ReactiveResourceIndex));
