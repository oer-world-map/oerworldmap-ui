import React, { useState, useContext } from "react";

import withI18n from "./withI18n";
import { convertToString, isNode } from "../common";
import { UserContext } from "../contexts";

import "../styles/components/Error.pcss";

const Error = ({ error, intro, translate }) => {
  const introText = intro ?? translate("ClientTemplates.http_error.intro");
  const [copyError, setCopyError] = useState(undefined);
  const [isCopyConfirmed, setCopyConfirmed] = useState(false);
  const user = useContext(UserContext);

  const sendEmail = () => {
    // don't send the `profile` attribute, it's huge and has no valuable information
    const userProperties = Object.keys(user)
      .filter((key) => key != "profile")
      .reduce((obj, key) => {
        obj[key] = user[key];
        return obj;
      }, {});
    var email = "support@oerworldmap.org";
    var subject = translate("ClientTemplates.http_error.bugreportEmailSubject");
    var emailBody = `Message: ${error.message}\n\nDetails: ${convertToString(error.details)}\nURL: ${window.location.href}\nReferer: ${document.referrer}\nUser Agent: ${navigator.userAgent}\nUser:${convertToString(userProperties)}`;
    document.location = `mailto:${email}?subject=${subject}&body=${encodeURIComponent(emailBody)}`;
  };

  return (
    error && (
      <div className="Error">
        <div className="message">{introText}</div>
        <button className="btn addButton" onClick={sendEmail}>
          <i className="fa fa-envelope"></i>
          <span>{translate("ClientTemplates.http_error.sendBugreport")}</span>
        </button>
        {error.details && (
          <div>
            <details>
              <summary>{translate("ClientTemplates.http_error.details")}</summary>
              {!isNode && navigator?.clipboard && (
                <div className="clipboardButtonContainer">
                  <button
                    className="btn addButton clipboardButton"
                    onClick={() => {
                      navigator.clipboard
                        .writeText(`${error.message}\n${convertToString(error.details)}`)
                        .then(() => {
                          setCopyConfirmed(true);
                          setTimeout(() => {
                            setCopyConfirmed(false);
                          }, 6000);
                        })
                        .catch((error) => {
                          setCopyError(error.message);
                        });
                    }}
                  >
                    <i className="fa fa-clipboard"></i>
                    <span>{translate("ClientTemplates.http_error.copyToClipboard")}</span>
                  </button>
                  {isCopyConfirmed && (
                    <div>
                      <span>
                        {translate("ClientTemplates.http_error.copyToClipboardConfirmation")}
                      </span>
                    </div>
                  )}
                  {copyError && <div>{copyError}</div>}
                </div>
              )}
              <div>
                <b>{error.message}</b>
              </div>
              <pre>{convertToString(error.details)}</pre>
            </details>
          </div>
        )}
      </div>
    )
  );
};

export default withI18n(Error);
