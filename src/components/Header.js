/* global document */
/* global window */
/* global SUPPORTED_LANGUAGES */
/* global _paq */
import React from "react";
import PropTypes from "prop-types";
import withEmitter from "./withEmitter";
import withI18n from "./withI18n";
import Link from "./Link";
import { triggerClick, urlParser, types } from "../common";
import ConceptBlock from "./ConceptBlock";
import Icon from "./Icon";
import withUser from "./withUser";
import Searchbar from "./Searchbar";
import expose from "../expose";

import organizations from "../json/organizations.json";
import persons from "../json/persons.json";
import services from "../json/services.json";
import policyTypes from "../json/policyTypes.json";
import projects from "../json/projects.json";

import "../styles/components/Header.pcss";
import "../styles/helpers.pcss";

const organizationsConcepts = organizations.hasTopConcept;
const personsConcepts = persons.hasTopConcept;
const servicesConcepts = services.hasTopConcept;
const policyTypesConcepts = policyTypes.hasTopConcept;
const projectsConcepts = projects.hasTopConcept;

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      dropdowns: {
        find: false,
        add: false,
        info: false,
        me: false
      },
      showNotification: false
    };
    this.handleClick = this.handleClick.bind(this);
    this.setDropdown = this.setDropdown.bind(this);
    this.setLoading = this.setLoading.bind(this);
    this.newActivity = this.newActivity.bind(this);
    this.clearActivity = this.clearActivity.bind(this);
  }

  componentDidMount() {
    document.getElementById("initial-loading-background")?.remove();
    document.getElementById("initial-loading-spinner")?.remove();
    const { emitter } = this.props;
    document.addEventListener("click", this.handleClick);

    emitter.on("setLoading", this.setLoading);
    emitter.on("newActivity", this.newActivity);
    emitter.on("clearActivity", this.clearActivity);
  }

  componentWillUnmount() {
    const { emitter } = this.props;

    emitter.off("setLoading", this.setLoading);
    emitter.off("newActivity", this.newActivity);
    emitter.off("clearActivity", this.clearActivity);
    document.removeEventListener("click", this.handleClick);
  }

  setLoading() {
    if (this.dropDown) {
      this.setState({ showMobileMenu: false });
      this.setDropdown("");
    }
  }

  setDropdown(name) {
    const { dropdowns } = this.state;
    const dropdownsState = {};

    if (dropdowns[name]) return;

    let isDropdownOpen = false;
    Object.keys(dropdowns).forEach((key) => {
      if (dropdowns[key]) isDropdownOpen = true;
      dropdownsState[key] = key === name;
    });

    if (
      name === "" ||
      !isDropdownOpen ||
      (this.lastDropdownChange && Date.now() - this.lastDropdownChange > 1000)
    ) {
      this.setState({ dropdowns: dropdownsState });
      this.lastDropdownChange = Date.now();
    }
  }

  newActivity(activities) {
    const { user } = this.props;
    if (window.location.pathname !== "/activity/") {
      const showNotification =
        !user || activities.some((activity) => activity.user && activity.user["@id"] !== user.id);
      this.setState({ showNotification });
    }
  }

  clearActivity() {
    this.setState({ showNotification: false });
  }

  handleClick(e) {
    if (
      e.target !== this.menuToggle &&
      this.secondaryNav &&
      !this.secondaryNav.contains(e.target)
    ) {
      this.setState({ showMobileMenu: false });
    }
  }

  render() {
    const { _self, translate, user, locales, emitter } = this.props;
    const { showMobileMenu, dropdowns, showNotification } = this.state;
    const { pathname } = urlParser(Link.self);

    let { supportedLanguages } = this.props;
    let isStaticPage = false;
    if (!supportedLanguages) {
      supportedLanguages = SUPPORTED_LANGUAGES;
      isStaticPage = true;
    }

    const view = typeof window !== "undefined" ? window.location.hash.substr(1) : "";
    let params;
    if (typeof window !== "undefined") {
      params = new URLSearchParams(window.location.search);
    }
    const languages = supportedLanguages
      .filter((lang) => lang !== locales[0])
      .map((lang) => {
        let url = urlParser(Link.self);
        // console.log('url', url, 'Link.self', Link.self, 'Link', Link)
        if (url.pathname == "/") {
          url.pathname = "/index";
        }
        url.searchParams.set("language", lang);
        params?.forEach((value, key) => {
          if (key !== "language") url.searchParams.append(key, value);
        });

        return (
          <li key={lang}>
            <a href={`${url.href}${view && !isStaticPage ? `#${view}` : ""}`}>{translate(lang)}</a>
          </li>
        );
      });

    return (
      <header className="Header">
        <div className="headerTitle">
          <a href={`/?language=${locales[0]}`}>
            <h1 style={{ fontSize: "12px" }}>Home</h1>
          </a>

          <Link title={translate("main.map")} href={`/resource/?language=${locales[0]}`}>
            <h1>
              <i aria-hidden="true" className="fa fa-globe" />
              &nbsp;
              {translate("OER World Map")}
            </h1>
          </Link>
        </div>

        {!isStaticPage && <Searchbar viewHash={view} />}

        <button
          className="menuToggle visible-mobile-block"
          onClick={() => {
            this.setState({ showMobileMenu: !showMobileMenu });
            this.setDropdown("");
          }}
          onKeyDown={triggerClick}
          ref={(el) => (this.menuToggle = el)}
        >
          <i aria-hidden="true" className="fa fa-bars" />
        </button>

        <nav
          className={`secondaryNav${showMobileMenu ? " show" : ""}`}
          ref={(secondaryNav) => (this.secondaryNav = secondaryNav)}
        >
          <ul
            onMouseLeave={() => {
              this.setDropdown("");
            }}
          >
            <li>
              <Link href={`/activity/?language=${locales[0]}`} className="activityFeedLink">
                {translate("menu.activity")}
                {showNotification && (
                  <span className="showNotification">
                    <i className="fa fa-bell" aria-hidden="true" />
                  </span>
                )}
              </Link>
            </li>
            <li
              className={`hasDropdown${dropdowns.find ? " active" : ""}`}
              onMouseMove={() => {
                this.setDropdown("find");
              }}
            >
              <div
                tabIndex="0"
                className="btnHover"
                onKeyDown={triggerClick}
                role="button"
                onClick={() => {
                  this.setDropdown("find");
                }}
              >
                {translate("menu.find")}
              </div>
              <div
                onClick={(event) => {
                  if (event.target.tagName === "A") this.setDropdown("");
                }}
                className="dropdown"
              >
                <div className="inner">
                  <div className="popular">
                    <ul>
                      <li>
                        <Link
                          className="iconItem"
                          href={`/resource/?language=${locales[0]}&filter.about.objectIn.%40type=["LighthouseAction"]&sort=lighthouse_count`}
                        >
                          <div className="i">
                            <img
                              className="visible-hover-focus"
                              src="/public/lighthouse_16px_orange.svg"
                              alt="Lighthouse"
                            />
                            <img
                              className="hidden-hover-focus"
                              src="/public/lighthouse_16px_blue_dark.svg"
                              alt="Lighthouse"
                            />
                          </div>
                          {translate("ClientTemplates.app.lighthouses")}
                        </Link>
                      </li>
                      <li>
                        <Link
                          className="iconItem"
                          href={`/resource/?language=${locales[0]}&filter.about.objectIn.%40type=["LikeAction"]&sort=like_count`}
                        >
                          <i aria-hidden="true" className="fa fa-thumbs-up" />
                          {translate("menu.most_liked")}
                        </Link>
                      </li>
                      <li>
                        <Link
                          className="iconItem"
                          href={`/resource/?language=${locales[0]}&view=statistics`}
                        >
                          <i aria-hidden="true" className="fa fa-chart-column" />
                          {translate("ClientTemplates.app.statistics")}
                        </Link>
                      </li>
                      <li>
                        <a className="item iconItem" href={`/oerpolicies?language=${locales[0]}`}>
                          <i aria-hidden="true" className="fa fa-balance-scale" />
                          {translate("ClientTemplates.app.oerpolicies")}
                        </a>
                      </li>
                    </ul>

                    {user && (
                      <ul>
                        <li>
                          <Link
                            className="iconItem"
                            href={`/resource/?language=${locales[0]}&filter.author.keyword=["${user.id}"]`}
                          >
                            <i aria-hidden="true" className="fa fa-pencil" />
                            {translate("menu.my_entries")}
                          </Link>
                        </li>
                        {user.country && (
                          <li>
                            <Link className="iconItem" href={`/country/${user.country}`}>
                              <i aria-hidden="true" className="fa fa-flag" />
                              {translate("Countryview:")}
                              &nbsp;
                              {translate(user.country)}
                            </Link>
                          </li>
                        )}
                      </ul>
                    )}
                  </div>

                  <hr />

                  <div className="row text-small stack-700">
                    <div className="col">
                      <ConceptBlock
                        type="Organization"
                        conceptScheme={organizationsConcepts}
                        additionalTypeTemplate={`/resource/?language=${locales[0]}&filter.about.additionalType.@id=["{@id}"]`}
                        typeLink={`/resource/?language=${locales[0]}&filter.about.@type="Organization"`}
                      />
                    </div>
                    <div className="col">
                      <ConceptBlock
                        type="Person"
                        conceptScheme={personsConcepts}
                        additionalTypeTemplate={`/resource/?language=${locales[0]}&filter.about.additionalType.@id=["{@id}"]`}
                        typeLink={`/resource/?language=${locales[0]}&filter.about.@type="Person"`}
                      />
                    </div>
                    <div className="col">
                      <ConceptBlock
                        type="Service"
                        conceptScheme={servicesConcepts}
                        additionalTypeTemplate={`/resource/?language=${locales[0]}&filter.about.additionalType.@id=["{@id}"]`}
                        typeLink={`/resource/?language=${locales[0]}&filter.about.@type="Service"`}
                      />
                    </div>
                    <div className="col">
                      <ConceptBlock
                        type="Policy"
                        conceptScheme={policyTypesConcepts}
                        additionalTypeTemplate={`/resource/?language=${locales[0]}&filter.about.additionalType.@id=["{@id}"]`}
                        typeLink={`/resource/?language=${locales[0]}&filter.about.@type="Policy"`}
                      />
                    </div>
                    <div className="col">
                      <ConceptBlock
                        type="Action"
                        conceptScheme={projectsConcepts}
                        additionalTypeTemplate={`/resource/?language=${locales[0]}&filter.about.additionalType.@id=["{@id}"]`}
                        typeLink={`/resource/?language=${locales[0]}&filter.about.@type="Action"`}
                      />
                      <ConceptBlock
                        type="Product"
                        typeLink={`/resource/?language=${locales[0]}&filter.about.@type="Product"`}
                      />
                      <ConceptBlock
                        type="Event"
                        typeLink={`/resource/?language=${locales[0]}&filter.about.@type="Event"`}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </li>

            <li
              className={`addMenu hasDropdown${dropdowns.add ? " active" : ""}`}
              onMouseMove={() => {
                typeof _paq !== "undefined" &&
                  _paq.push(["trackEvent", "MainMenu", "MenuEntryClick", "Add"]);
                this.setDropdown("add");
              }}
            >
              <div
                tabIndex="0"
                className="btnHover"
                onKeyDown={triggerClick}
                role="button"
                onClick={() => {
                  typeof _paq !== "undefined" &&
                    _paq.push(["trackEvent", "MainMenu", "MenuEntryClick", "Add"]);
                  this.setDropdown("add");
                }}
              >
                {translate("menu.add")}
              </div>
              <div
                onClick={(event) => {
                  if (event.target.baseURI.includes("/?add=")) this.setDropdown("");
                }}
                className="dropdown"
              >
                <div className="inner">
                  <div className="popular">
                    <div style={{ maxWidth: "80%" }}>
                      {translate("menu.add.subtitle")}
                      <p dangerouslySetInnerHTML={{ __html: translate("menu.hint") }} />
                    </div>
                    <Link
                      className="link-grey"
                      target="_blank"
                      href="/editorsFAQ#which-data-can-i-add-or-modify"
                    >
                      {translate("needHelp")}
                    </Link>
                  </div>
                  <div
                    className="row vertical-guttered stack-700"
                    style={{ justifyContent: "start" }}
                  >
                    {types
                      .filter((type) => type !== "Person")
                      .map((type) => (
                        <div key={type} className="col one-fourth">
                          <Link
                            className="addBox"
                            href={`/resource/?language=${locales[0]}&add=${type}`}
                            additional={() => {
                              typeof _paq !== "undefined" &&
                                _paq.push(["trackEvent", "AddMenu", "TypeClick", type]);
                            }}
                          >
                            <h3 className="iconItem">
                              <Icon type={type} />
                              {translate(type)}
                            </h3>
                            <p className="text-small">{translate(`descriptions.${type}`)}</p>
                          </Link>
                        </div>
                      ))}
                  </div>
                </div>
              </div>
            </li>

            <li
              className={`hasDropdown${dropdowns.info ? " active" : ""}`}
              onMouseMove={() => {
                this.setDropdown("info");
              }}
            >
              <div
                tabIndex="0"
                className="btnHover"
                onKeyDown={triggerClick}
                role="button"
                onClick={() => {
                  this.setDropdown("info");
                }}
              >
                {translate("menu.info")}
              </div>
              <div className="dropdown">
                <div className="inner">
                  <div className="row stack-700 stack-gutter-2em">
                    <div className="col one-third">
                      <ul className="linedList border-bottom">
                        <li>
                          <h3>{translate("menu.info.about")}</h3>
                        </li>
                        <li>
                          <a className="item" href={`/about?language=${locales[0]}#the-vision`}>
                            {translate("menu.info.about.theOerWorldMap")}
                          </a>
                        </li>
                        <li>
                          <a className="item" href={`/contribute?language=${locales[0]}`}>
                            {translate("menu.info.about.contribute")}
                          </a>
                        </li>
                        <li>
                          <a className="item" href={`/FAQ?language=${locales[0]}`}>
                            {translate("menu.info.about.faq")}
                          </a>
                        </li>
                        <li>
                          <a className="item" href={`/editorsFAQ?language=${locales[0]}`}>
                            {translate("menu.info.about.faqeditors")}
                          </a>
                        </li>
                      </ul>
                    </div>
                    <div className="col one-third">
                      <ul className="linedList border-bottom">
                        <li>
                          <h3>{translate("menu.info.social")}</h3>
                        </li>
                        <li>
                          <a
                            className="item"
                            href="https://bildung.social/@oerinfo"
                            rel="noopener noreferrer"
                            target="_blank"
                          >
                            {translate("menu.info.social.fediverse")}
                          </a>
                        </li>
                        <li>
                          <a
                            className="item"
                            href="https://twitter.com/OERinfo"
                            rel="noopener noreferrer"
                            target="_blank"
                          >
                            {translate("menu.info.social.twitter")}
                          </a>
                        </li>
                        <li>
                          <a
                            className="item"
                            href="https://gitlab.com/oer-world-map/oerworldmap"
                            rel="noopener noreferrer"
                            target="_blank"
                          >
                            {translate("menu.info.social.gitlab")}
                          </a>
                        </li>
                      </ul>
                    </div>
                    <div className="col one-third">
                      <ul className="linedList border-bottom">
                        <li>
                          <h3>{translate("menu.info.legal")}</h3>
                        </li>
                        <li>
                          <a className="item" href="mailto:support@oerworldmap.org">
                            {translate("menu.info.legal.contact")}
                          </a>
                        </li>
                        <li>
                          <a className="item" href="/imprint">
                            {translate("menu.info.legal.imprint")}
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </li>

            {user ? (
              <li
                className={`hasDropdown${dropdowns.me ? " active" : ""}`}
                onMouseMove={() => {
                  this.setDropdown("me");
                }}
              >
                <div
                  tabIndex="0"
                  className="btnHover"
                  onKeyDown={triggerClick}
                  role="button"
                  onClick={() => {
                    this.setDropdown("me");
                  }}
                >
                  {translate("menu.me")}
                </div>
                <div ref={(el) => (this.dropDown = el)} className="dropdown">
                  <div className="inner">
                    <div className="row stack-700 stack-gutter-2em">
                      <div className="col one-half">
                        <ul className="linedList border-bottom">
                          <li>
                            <Link className="item" href={`/resource/?language=${locales[0]}`}>
                              <i aria-hidden="true" className="fa fa-home" />
                              <span>{translate("menu.me.home")}</span>
                            </Link>
                          </li>
                          <li>
                            <a
                              className="item"
                              href={
                                locales && locales.length > 0
                                  ? `/user/profile?language=${locales[0]}#edit`
                                  : "/user/profile#edit"
                              }
                            >
                              <i aria-hidden="true" className="fa fa-user-circle" />
                              <span>{translate("menu.me.profile")}</span>
                            </a>
                          </li>
                          <li>
                            <a className="item" href="/auth/realms/oerworldmap/account/">
                              <i aria-hidden="true" className="fa fa-cogs" />
                              <span>{translate("menu.me.settings")}</span>
                            </a>
                          </li>
                          {expose("groupAdmin", user, {}) && (
                            <li>
                              <a className="item" href="/auth/admin/oerworldmap/console/">
                                <i aria-hidden="true" className="fa fa-user-secret" />
                                <span>{translate("menu.me.userAdministration")}</span>
                              </a>
                            </li>
                          )}
                          <li>
                            <a
                              className="item"
                              href={"/oauth2callback?logout=".concat(encodeURIComponent(Link.self))}
                            >
                              <i aria-hidden="true" className="fa fa-sign-out" />
                              <span>{translate("menu.me.logout")}</span>
                            </a>
                          </li>
                        </ul>
                      </div>
                      <div className="col one-half">
                        <ul className="linedList border-bottom">
                          <li>
                            <Link
                              className="item"
                              href={`/resource/?language=${locales[0]}&filter.author.keyword=["${user.id}"]`}
                            >
                              <i aria-hidden="true" className="fa fa-pencil" />
                              <span>{translate("menu.me.entries")}</span>
                            </Link>
                          </li>
                          <li>
                            <Link
                              className="item"
                              href={`/resource/?language=${locales[0]}&filter.about.objectIn.agent.@id=["${user.id}"]&filter.about.objectIn.%40type=["LikeAction"]`}
                            >
                              <i aria-hidden="true" className="fa fa-thumbs-up" />
                              <span>{translate("menu.me.likes")}</span>
                            </Link>
                          </li>
                          <li>
                            <Link
                              className="item"
                              href={`/resource/?language=${locales[0]}&filter.about.objectIn.agent.@id=["${user.id}"]&filter.about.objectIn.%40type=["LighthouseAction"]`}
                            >
                              <div className="i">
                                <img
                                  className="visible-hover-focus"
                                  src="/public/lighthouse_16px_orange.svg"
                                  alt="Lighthouse"
                                />
                                <img
                                  className="hidden-hover-focus"
                                  src="/public/lighthouse_16px_blue_dark.svg"
                                  alt="Lighthouse"
                                />
                              </div>
                              <span>{translate("menu.me.lighthouses")}</span>
                            </Link>
                          </li>
                          <li>
                            <Link
                              className="item"
                              href={`/resource/?language=${locales[0]}&filter.about.attendee.@id=["${user.id}"]`}
                            >
                              <i aria-hidden="true" className="fa fa-calendar" />
                              <span>{translate("menu.me.events")}</span>
                            </Link>
                          </li>
                          <li>
                            <Link
                              className="item"
                              href={`/resource/?language=${locales[0]}&filter.about.affiliate.@id=["${user.id}"]`}
                            >
                              <i aria-hidden="true" className="fa fa-users" />
                              <span>{translate("menu.me.organizations")}</span>
                            </Link>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </li>
            ) : (
              <li>
                <a
                  title={translate("login")}
                  href={`/.login?continue=${Link.self}`}
                  className="loginLink"
                >
                  {translate("login")}
                </a>
              </li>
            )}
          </ul>
          <ul>
            {supportedLanguages && (
              <li className="languageSelector">
                <span className="btnHover">
                  {translate("Language")}
                  <ul>{languages}</ul>
                </span>
              </li>
            )}
          </ul>
        </nav>
      </header>
    );
  }
}

Header.propTypes = {
  emitter: PropTypes.objectOf(PropTypes.any).isRequired,
  translate: PropTypes.func.isRequired,
  user: PropTypes.objectOf(PropTypes.any),
  locales: PropTypes.arrayOf(PropTypes.any).isRequired,
  supportedLanguages: PropTypes.arrayOf(PropTypes.any).isRequired
};

Header.defaultProps = {
  user: null
};

export default withEmitter(withI18n(withUser(Header)));
