/* global document */
/* global window */
import React, { useContext } from "react";

import withI18n from "./withI18n";
import withEmitter from "./withEmitter";
import ShareExport from "./ShareExport";
import { FilterContext } from "../contexts";

const timeout = async (ms) => new Promise((resolve) => setTimeout(resolve, ms));

const ModeToggle = ({
  emitter,
  translate,
  _self,
  params,
  viewHash,
  showMobileFilters,
  setShowMobileFilters
}) => {
  const sizes = [20, 50, 100, 200, 9999];
  const { getUrlParams, setUrlParams, sorts } = useContext(FilterContext);

  const links = Object.entries({
    geojson: "application/geo+json",
    ics: "text/calendar",
    json: "application/json",
    csv: "text/csv"
  }).map(([ext, type]) => {
    const uri = new URL(_self.split("#")[0]);
    uri.searchParams.set("ext", ext);
    uri.searchParams.set("size", -1);
    uri.searchParams.delete("view");
    return { uri, type };
  });

  return (
    <div id="mode-toggle">
      <div className="rightButtons">
        {params.view === "list" && (
          <>
            <div className="select-wrapper hidden-mobile">
              <select
                id="entriesPage"
                value={params.size}
                className="btn"
                onChange={(e) => {
                  setUrlParams({ ...getUrlParams(), size: e.target.value });
                }}
              >
                {sizes.map((size) => (
                  <option key={size} value={size}>
                    {size === 9999 ? "all" : size}
                    &nbsp;
                    {translate("filters.entriesPage")}
                  </option>
                ))}
              </select>
            </div>

            <div className="select-wrapper hidden-mobile">
              <select
                value={params.sort}
                className="btn"
                onChange={(e) => {
                  setUrlParams({ ...getUrlParams(), sort: e.target.value });
                }}
              >
                {sorts.map((sort) => (
                  <option key={sort.dataField} value={sort.dataField}>
                    {sort.label}
                  </option>
                ))}
              </select>
            </div>
          </>
        )}

        <button
          disabled={params.view === "map"}
          type="button"
          className="btn mapBtn"
          onClick={async () => {
            setUrlParams({ ...getUrlParams(), view: "map" });
            await timeout(100);
            emitter.emit("resize");
          }}
        >
          <i className="fa fa-map" />
          &nbsp;
          {translate("main.map")}
        </button>
        <button
          disabled={params.view === "list"}
          type="button"
          className="btn"
          onClick={() => setUrlParams({ ...getUrlParams(), view: "list" })}
        >
          <i className="fa fa-list" />
          &nbsp;
          {translate("main.list")}
        </button>
        <button
          disabled={params.view === "statistics"}
          type="button"
          className="btn"
          onClick={() => setUrlParams({ ...getUrlParams(), view: "statistics" })}
        >
          <i className="fa fa-chart-column" />
          &nbsp;
          {translate("ClientTemplates.app.statistics")}
        </button>

        <button
          type="button"
          className={`btn hidden-desktop${showMobileFilters ? " active" : ""}`}
          onClick={() => {
            setShowMobileFilters(!showMobileFilters);
          }}
        >
          <i className="fa fa-filter" />
        </button>
      </div>
      <ShareExport _self={_self} _links={{ refs: links }} view={viewHash} />
    </div>
  );
};

export default withEmitter(withI18n(ModeToggle));
