import React from "react";
import Link from "./Link";

const LinkOverride = ({ children, title, href }) => {
  if (typeof href === "undefined") {
    return children;
  }

  if (href.startsWith("#")) {
    return (
      <Link href={href} title={title}>
        {children}
      </Link>
    );
  } else {
    return (
      <a href={href} title={title} target="_blank" rel="noopener noreferrer">
        {children}
      </a>
    );
  }
};

export default LinkOverride;
