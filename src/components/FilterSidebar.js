/* global document */
/* global window */
import React, { useState, useEffect } from "react";

import {
  MultiDropdownList,
  SelectedFilters,
  SingleDropdownList,
  ReactiveComponent
} from "@appbaseio/reactivesearch";

import { quickSearchTypes } from "../common";
import withI18n from "./withI18n";
import withEmitter from "./withEmitter";
import withConfig from "./withConfig";
import Country from "./Country";
import TotalEntries from "./TotalEntries";
import PubSub from "pubsub-js";

import "../styles/components/FilterSidebar.pcss";

const FilterSidebar = ({
  iso3166,
  filterIDs,
  region,
  setShowMobileFilters,
  showMobileFilters,
  translate,
  showFeatures,
  emitter,
  collapsed,
  setCollapsed,
  subFilters
}) => {
  const date = new Date().toJSON().split("T").shift();

  const renderLabel = (filter) => (item) =>
    Object.keys(item).length ? (
      <div>{Object.keys(item).map(translate).join(", ")}</div>
    ) : (
      translate("select", { name: translate(filter.title || filter.componentId) })
    );

  return (
    <div id="filter-sidebar" className={showMobileFilters ? "show" : ""}>
      <button
        className="toggleList"
        type="button"
        onClick={() => {
          setCollapsed(!collapsed);
        }}
      >
        <i className={`fa fa-chevron-${collapsed ? "right" : "left"}`} />
      </button>
      <aside>
        <div className="hidden-desktop close">
          <button
            type="button"
            className="btn icon"
            onClick={() => {
              setShowMobileFilters(false);
            }}
          >
            <i className="fa fa-xmark" />
          </button>
        </div>

        <TotalEntries className="hidden-mobile" />

        {iso3166 && <Country iso3166={iso3166} region={region} />}

        <SelectedFilters
          render={(data) => {
            const applied = Object.keys(data.selectedValues);
            return (
              applied.length > 0 &&
              applied
                .map((filter) => data.selectedValues[filter])
                .map((f) => f.value)
                .some((value) => value !== null && value.length > 0) && (
                <div className="selectedFilters">
                  <h2>{translate("filters.title")}</h2>
                  <ul>
                    {applied
                      .filter((f) => f !== "size")
                      .map((filter) => {
                        const isArray = Array.isArray(data.selectedValues[filter].value);
                        return (
                          data.selectedValues[filter].value !== null &&
                          filter.startsWith("filter.") && (
                            <li key={`${filter}`} className="selectedFilter">
                              {!isArray ? (
                                <button type="button" onClick={() => data.setValue(filter, null)}>
                                  <b>{translate(filter)}</b>: &nbsp;
                                  {translate(data.selectedValues[filter].value)}
                                </button>
                              ) : (
                                data.selectedValues[filter].value.map((value) => (
                                  <button
                                    type="button"
                                    key={`${filter}-${value}`}
                                    onClick={() => {
                                      data.setValue(
                                        filter,
                                        data.selectedValues[filter].value.filter((v) => v !== value)
                                      );
                                    }}
                                  >
                                    <b>{translate(filter)}</b>: &nbsp;
                                    {translate(value)}
                                  </button>
                                ))
                              )}
                            </li>
                          )
                        );
                      })}
                  </ul>
                  <button
                    type="button"
                    className="clearAll"
                    onClick={() => {
                      applied.map((filter) => {
                        data.setValue(filter, null);
                      });
                    }}
                  >
                    {translate("filters.clearAll")}
                  </button>
                </div>
              )
            );
          }}
        />

        <SingleDropdownList
          className="FilterBox"
          componentId="filter.about.@type"
          dataField="about.@type"
          title={translate("filter.about.@type")}
          placeholder={translate("select", { name: translate("filter.about.@type") })}
          renderItem={(label, count) => (
            <span>
              <span>{translate(label)}</span>
              &nbsp;
              <span className="count">({count})</span>
            </span>
          )}
          transformData={(data) => {
            return data.filter((d) => quickSearchTypes.includes(d.key));
          }}
          URLParams
        />

        <ReactiveComponent
          componentId="myCountryPicker"
          defaultQuery={() => {
            // Add `startTime` and `endTime` to `_source` to detect live events
            const query = {
              size: showFeatures ? 9999 : 0,
              _source: ["feature.*", "about.startTime", "about.endTime"],
              query: {
                bool: {
                  should: [
                    {
                      bool: {
                        must: {
                          exists: {
                            field: "feature"
                          }
                        },
                        filter: [
                          {
                            query_string: {
                              query: `about.endTime:>=${date}`
                            }
                          },
                          {
                            term: {
                              "about.@type": "Event"
                            }
                          }
                        ]
                      }
                    },
                    {
                      bool: {
                        filter: [
                          {
                            terms: {
                              "about.@type": quickSearchTypes.filter((type) => type !== "Event")
                            }
                          },
                          {
                            exists: {
                              field: "about.name"
                            }
                          },
                          {
                            exists: {
                              field: "feature"
                            }
                          }
                        ]
                      }
                    }
                  ]
                }
              },
              aggs: {}
            };

            if (iso3166) {
              query.aggs["sterms#feature.properties.location.address.addressRegion"] = {
                terms: {
                  field: "feature.properties.location.address.addressRegion",
                  size: 9999
                },
                aggs: {
                  "sterms#by_type": {
                    terms: {
                      field: "about.@type"
                    }
                  }
                }
              };
              const countryFilter = {
                term: {
                  "feature.properties.location.address.addressCountry": iso3166.toUpperCase()
                }
              };
              query.query.bool.should[0].bool.filter.push(structuredClone(countryFilter));
              query.query.bool.should[1].bool.filter.push(structuredClone(countryFilter));
            } else {
              query.aggs["sterms#feature.properties.location.address.addressCountry"] = {
                terms: {
                  field: "feature.properties.location.address.addressCountry",
                  size: 9999
                },
                aggs: {
                  "sterms#by_type": {
                    terms: {
                      field: "about.@type"
                    }
                  }
                }
              };
            }

            if (region) {
              const regionFilter = {
                term: {
                  "feature.properties.location.address.addressRegion": `${iso3166.toUpperCase()}.${region.toUpperCase()}`
                }
              };
              query.query.bool.should[0].bool.filter.push(structuredClone(regionFilter));
              query.query.bool.should[1].bool.filter.push(structuredClone(regionFilter));
            }
            return query;
          }}
          onData={({ aggregations, data = [] }) => {
            if (aggregations !== null && data !== null) {
              const features = data.map((item) => {
                // Add "about" so we can later detect if we have a live event.
                item.feature.properties.about = item.about;
                return item.feature;
              });
              PubSub.publish("mapData", { features, aggregations });
            }
          }}
          render={({ resultStats: { numberOfResults } }) => {
            emitter.emit("updateCount", numberOfResults);
          }}
          react={{
            and: filterIDs
          }}
        />

        {subFilters.map((filter) => (
          <div hidden={filter.hidden} key={filter.dataField}>
            <MultiDropdownList
              className="FilterBox"
              {...filter}
              title={filter.title ? translate(filter.title) : translate(filter.componentId)}
              renderLabel={filter.translate !== false ? renderLabel(filter) : undefined}
              placeholder={translate("select", {
                name: filter.title ? translate(filter.title) : translate(filter.componentId)
              })}
              renderItem={(label, count) => (
                <span>
                  <span>{filter.translate !== false ? translate(label) : label}</span>
                  &nbsp;
                  <span className="count">({count})</span>
                </span>
              )}
              URLParams
            />
          </div>
        ))}
      </aside>
    </div>
  );
};

export default withConfig(withEmitter(withI18n(FilterSidebar)));
