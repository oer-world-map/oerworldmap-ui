import React from "react";

import withFormData from "./withFormData";
import { objectMap } from "../../common";

const Fieldset = ({
  name,
  children,
  property,
  translate,
  formId,
  description,
  title = "",
  className = "",
  required = false,
  errors = []
}) => (
  <div
    className={`Fieldset ${property || ""} ${className} ${errors.length ? "hasError" : ""}`.trim()}
    role="group"
    aria-labelledby={`${formId}-${name}-label`}
  >
    <div className={`label ${required ? "required" : ""}`.trim()} id={`${formId}-${name}-label`}>
      {translate(title)}
    </div>
    <span className="fieldDescription">
      {description && translate(description) !== description ? translate(description) : ""}
    </span>
    {errors.map((error, index) => (
      <div className="error" key={index}>
        {translate(`Error.${error.keyword}`, objectMap(error.params, translate))}
      </div>
    ))}
    {children}
  </div>
);

export default withFormData(Fieldset);
