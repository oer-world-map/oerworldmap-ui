import React from "react";
import PropTypes from "prop-types";
import PubSub from "pubsub-js";
import jsonPointer from "json-pointer";
import withI18n from "../withI18n";
import { FormContext } from "../../contexts";
import {
  forOwn,
  isUndefined,
  isNull,
  isNaN,
  isString,
  isEmpty,
  isObject,
  isArray,
  pull,
  uniqueId
} from "lodash";

const prune = (current, preventWhitespace = false) => {
  forOwn(current, (value, key) => {
    if (
      isUndefined(value) ||
      isNull(value) ||
      isNaN(value) ||
      (isObject(value) && isEmpty(prune(value, preventWhitespace)))
    ) {
      delete current[key];
    } else if (preventWhitespace && isString(value)) {
      const trimmed = value.trim();
      if (isEmpty(trimmed)) {
        delete current[key];
      } else {
        current[key] = trimmed;
      }
    }
  });
  if (isArray(current)) {
    pull(current, undefined);
  }
  return current;
};

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      formData: props.data,
      formErrors: [],
      submitErrors: []
    };
    this.id = props.id || uniqueId();
    this.lastUpdate = "";
    this.lastOp = null;
    this.onPostResourceError = this.onPostResourceError.bind(this);
    this.submit = this.submit.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      formData: nextProps.data,
      formErrors: [],
      submitErrors: []
    });
  }

  setValue(name, value, _prune = true) {
    this.setState((prevState) => {
      this.lastOp =
        value && jsonPointer.has(prevState.formData, name)
          ? "changed"
          : value
            ? "added"
            : "removed";
      this.lastUpdate = name;
      jsonPointer.set(prevState.formData, name, value);
      const formData = _prune ? prune(prevState.formData, false) : prevState.formData;
      return { formData };
    });
  }

  getValue(name) {
    const { formData } = this.state;

    return jsonPointer.has(formData, name) ? jsonPointer.get(formData, name) : undefined;
  }

  getValidationErrors(name) {
    const { formErrors } = this.state;

    return formErrors.filter(
      (error) =>
        error.keyword !== "anyOf" &&
        (error.keyword === "required"
          ? `${error.instancePath}/${error.params.missingProperty}` === name
          : error.instancePath.endsWith("/@type")
            ? error.instancePath.replace(new RegExp("/@type$"), "") === name
            : error.instancePath === name)
    );
  }

  onPostResourceError(_, errors) {
    const { data, translate } = this.props;
    const newSubmitErrors = errors.details.filter((error) => error.level === "error");
    for (const error of newSubmitErrors) {
      if (
        data["@type"] !== undefined &&
        error.schema?.pointer !== undefined &&
        error.domain === "validation" &&
        error.message !== undefined
      ) {
        const pointer = error.schema.pointer;
        const indexOfLastSlash = pointer.lastIndexOf("/");
        const attr = pointer.slice(indexOfLastSlash + 1);
        const newMessage = translate(`${data["@type"]}.${attr}`) + " - " + error.message;
        error.message = newMessage;
      }
    }
    this.setState({ submitErrors: newSubmitErrors });
    // Wait until the UI is updated and the error is part of the DOM.
    // TODO: When migrating to function components this should be done in `useEffect`.
    setTimeout(() => {
      document.querySelector(".error")?.scrollIntoView({ behavior: "smooth", block: "center" });
    }, 300);
  }

  componentDidMount() {
    this.pubsubToken = PubSub.subscribe("post-resource-error", this.onPostResourceError);
  }

  componentWillUnmount() {
    PubSub.unsubscribe(this.pubsubToken);
  }

  shouldFormComponentUpdate(name) {
    return (
      !name ||
      this.lastUpdate.startsWith(name) ||
      name.startsWith(this.lastUpdate) ||
      (this.lastOp !== "changed" &&
        jsonPointer.parse(this.lastUpdate).shift() === jsonPointer.parse(name).shift()) ||
      this.getValidationErrors(name).length
    );
  }

  shouldFormComponentFocus(name) {
    return this.lastUpdate === name;
  }

  submit(formData) {
    const { onSubmit } = this.props;
    this.setState({
      formErrors: [],
      submitErrors: []
    });
    onSubmit(formData);
  }

  render() {
    const { action, method, validate, onError, children, translate } = this.props;
    const { formData, submitErrors } = this.state;
    const formContext = {
      formId: this.id,
      setValue: this.setValue.bind(this),
      getValue: this.getValue.bind(this),
      getValidationErrors: this.getValidationErrors.bind(this),
      shouldFormComponentUpdate: this.shouldFormComponentUpdate.bind(this),
      shouldFormComponentFocus: this.shouldFormComponentFocus.bind(this)
    };

    return (
      <FormContext.Provider value={formContext}>
        <form
          className="Form"
          action={action}
          method={method}
          onSubmit={(e) => {
            e.preventDefault();
            this.lastUpdate = "";
            this.lastOp = null;
            validate(prune(formData, true))
              ? this.submit(formData)
              : this.setState(
                  {
                    formErrors: validate.errors,
                    submitErrors: []
                  },
                  () => console.error(validate.errors) || onError(validate.errors)
                );
          }}
        >
          {children}
          {submitErrors.length > 0 && (
            <div className="Error error pulsating">
              <div>{translate("Error.error")}:</div>
              {submitErrors.map((error, index) => (
                <div>
                  {index + 1}. {error.message}
                </div>
              ))}
            </div>
          )}
        </form>
      </FormContext.Provider>
    );
  }
}

Form.propTypes = {
  data: PropTypes.objectOf(PropTypes.any),
  action: PropTypes.string,
  method: PropTypes.string,
  id: PropTypes.string,
  onSubmit: PropTypes.func,
  onError: PropTypes.func,
  validate: PropTypes.func,
  children: PropTypes.node.isRequired
};

Form.defaultProps = {
  data: {},
  action: "",
  method: "get",
  id: undefined,
  onSubmit: (formData) => console.log(formData),
  onError: (formErrors) => console.error(formErrors),
  validate: () => true
};

export default withI18n(Form);
