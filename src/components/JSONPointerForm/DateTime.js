import React, { useState } from "react";
import moment from "moment";

import withFormData from "./withFormData";
import { objectMap } from "../../common";

const DateTime = ({
  name,
  setValue,
  translate,
  formId,
  placeholder,
  description,
  required = false,
  className = "",
  title = "",
  errors = [],
  value = ""
}) => {
  const [hours, setHours] = useState(value.split("T")[1]?.split(":")[0] || "");
  const [minutes, setMinutes] = useState(value.split("T")[1]?.split(":")[1] || "");
  const hasTime = placeholder.includes(":");

  const updateDate = (newDate, newHours, newMinutes) => {
    const h = newHours.length > 1 ? `${newHours}` : `0${newHours}`;
    const m = newMinutes.length > 1 ? `${newMinutes}` : `0${newMinutes}`;
    if (newDate === "") {
      setValue("");
    } else if (
      hasTime &&
      newHours.length > 0 &&
      newHours.length < 3 &&
      newMinutes.length > 0 &&
      newMinutes.length < 3
    ) {
      const date = `${newDate.split("T")[0]}${`T${h}:${m}`}`;
      setValue(date);
    } else {
      const date = `${newDate.split("T")[0]}`;
      setValue(date);
    }
  };

  return (
    <div className={`DateTime ${className}${errors.length ? " hasErrors" : ""}`}>
      <label
        htmlFor={`${formId}-${name}`}
        dangerouslySetInnerHTML={{
          __html:
            translate(title) +
            (required
              ? `<span class="asterisk" title="${translate("Error.requiredField")}">*</span>`
              : "")
        }}
        className={required ? "required" : ""}
      />
      <span className="fieldDescription">
        {description && translate(description) !== description ? translate(description) : ""}
      </span>
      {errors.map((error, index) => (
        <div className="error" key={index}>
          {translate(`Error.${error.keyword}`, objectMap(error.params, translate))}
        </div>
      ))}
      <div className="Inputs">
        <input
          type="date"
          value={value.split("T")[0]}
          onChange={(e) => {
            const date = e.target.value;
            updateDate(date, hours, minutes);
          }}
        />

        {hasTime && value && moment(value.split("T")[0], "YYYY-MM-DD", true).isValid() && (
          <div className="Time">
            <div className="TimeInput">
              <input
                type="text"
                placeholder="hh"
                value={hours}
                onChange={(e) => {
                  const notNumberRegex = /\D+/g;
                  const targetValue = e.target.value;
                  if (notNumberRegex.test(targetValue) || targetValue.length > 2) return;
                  const newHours = Number(targetValue) > 23 ? "23" : targetValue;
                  setHours(newHours);
                  updateDate(value, newHours, minutes);
                }}
              />
              <span className="Colon">:</span>
              <input
                type="text"
                placeholder="mm"
                value={minutes}
                onChange={(e) => {
                  const notNumberRegex = /\D+/g;
                  const targetValue = e.target.value;
                  if (notNumberRegex.test(targetValue) || targetValue.length > 2) return;
                  const newMinutes = Number(targetValue) > 59 ? "59" : targetValue;
                  setMinutes(newMinutes);
                  updateDate(value, hours, newMinutes);
                }}
              />
            </div>
            <div className="fieldDescription">{translate("(Optional)")}</div>
          </div>
        )}
      </div>
      <input type="hidden" name={name} value={value} id={`${formId}-${name}`} />
    </div>
  );
};

export default withFormData(DateTime);
