import Ajv from "ajv-draft-04";

const validate = (schema) => {
  const ajv = new Ajv({
    allErrors: true,
    removeAdditional: true,
    strict: false
  });
  require("ajv-formats")(ajv);
  return ajv.compile(schema);
};

export default validate;
