import React from "react";

import withFormData from "./withFormData";

import { appendOnFocus, objectMap } from "../../common";

const castValue = (target) => {
  switch (target.type) {
    case "checkbox":
      return target.checked ? true : null;
    case "number":
      return parseFloat(target.value);
    case "integer":
      return parseInt(target.value, 10);
    default:
      return target.value;
  }
};

const Input = ({
  checked,
  name,
  setValue,
  property,
  translate,
  formId,
  placeholder,
  description,
  displayOptions,
  title = "",
  className = "",
  type = "text",
  value = "",
  shouldFormComponentFocus = false,
  required = false,
  errors = []
}) => (
  <div
    className={`Input ${type} ${property || ""} ${className} ${errors.length ? "hasError" : ""}`.trim()}
  >
    <label
      htmlFor={`${formId}-${name}`}
      dangerouslySetInnerHTML={{
        __html:
          translate(title) +
          (required
            ? `<span class="asterisk" title="${translate("Error.requiredField")}">*</span>`
            : "")
      }}
      className={required ? "required" : ""}
    />
    <span className="fieldDescription">
      {description && translate(description) !== description ? translate(description) : ""}
    </span>
    {errors.map((error, index) => (
      <div className="error" key={index}>
        {translate(`Error.${error.keyword}`, objectMap(error.params, translate))}
      </div>
    ))}
    <input
      checked={checked}
      type={type}
      name={name}
      value={value}
      id={`${formId}-${name}`}
      placeholder={translate(placeholder)}
      autoFocus={shouldFormComponentFocus}
      onFocus={appendOnFocus}
      onChange={(e) => setValue(castValue(e.target))}
    />
  </div>
);

export default withFormData(Input);
