import React from "react";

import withFormData from "./withFormData";

const ListItem = ({ children }) => <li>{children}</li>;

export default withFormData(ListItem);
