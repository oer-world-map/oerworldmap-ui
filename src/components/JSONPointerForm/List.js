import React from "react";

import ListItem from "./ListItem";
import withFormData from "./withFormData";
import { objectMap } from "../../common";

const List = ({
  name,
  children,
  property,
  translate,
  maxItems,
  formId,
  description,
  value = [],
  errors = [],
  title = "",
  className = "",
  required = false
}) => (
  <div
    className={`List ${property || ""} ${className} ${errors.length ? "hasError" : ""}`.trim()}
    role="group"
    aria-labelledby={`${formId}-${name}-label`}
  >
    <div className={`label ${required ? "required" : ""}`.trim()} id={`${formId}-${name}-label`}>
      {translate(title)}
      &nbsp;
      {required ? (
        <span className="asterisk" title={translate("Error.requiredField")}>
          *
        </span>
      ) : (
        ""
      )}
    </div>
    <span className="fieldDescription">
      {description && translate(description) !== description ? translate(description) : ""}
    </span>
    {errors.map((error, index) => (
      <div className="error" key={index}>
        {translate(`Error.${error.keyword}`, objectMap(error.params, translate))}
      </div>
    ))}
    <ul>
      {value.map((item, index) => (
        <ListItem property={index.toString()} key={index}>
          {React.cloneElement(children)}
        </ListItem>
      ))}
      {(!value.length || !maxItems || value.length < maxItems) && (
        <ListItem property={value.length.toString()} key={value.length}>
          {value.length && (!maxItems || value.length < maxItems) ? (
            <div className="newItemWrapper">
              <input
                type="checkbox"
                key={`${name}-${value.length}`}
                className="formControl"
                id={`${formId}-${name}-toggle`}
                tabIndex="-1"
              />
              <button
                onClick={(event) => {
                  event.preventDefault();
                  document.getElementById(`${formId}-${name}-toggle`).checked = true;

                  // Automatically focus next input element for better usability
                  setTimeout(() => {
                    const nextInput = document.querySelector(
                      `.List.${property || ""} .newItem input`
                    );
                    nextInput?.focus();
                  }, 0);
                }}
              >
                {translate("add", { type: translate(title) })}
              </button>
              <div className="newItem">{React.cloneElement(children)}</div>
            </div>
          ) : (
            React.cloneElement(children)
          )}
        </ListItem>
      )}
    </ul>
  </div>
);

export default withFormData(List);
