import React from "react";

import Input from "./Input";
import MarkdownArea from "./MarkdownArea";
import withFormData from "./withFormData";
import { objectMap } from "../../common";

const LocalizedString = ({
  schema,
  translate,
  value,
  setValue,
  shouldFormComponentFocus,
  description,
  formId,
  name,
  locales,
  title = "",
  required = false,
  errors = []
}) => {
  const TextInput = schema._display && schema._display.rows > 1 ? MarkdownArea : Input;
  var languagesPresent = Object.keys(schema.properties).filter(
    (lang) =>
      (schema.required && schema.required.includes(lang)) ||
      (schema.suggested && schema.suggested.includes(lang)) ||
      (locales.length && locales[0] === lang) ||
      (value && value[lang] != null)
  );
  /* Fallback to English language if none else was detected */
  languagesPresent = languagesPresent.length ? languagesPresent : ["en"];
  const languagesAvailable = Object.keys(schema.properties)
    .filter(
      (lang) =>
        !(schema.required && schema.required.includes(lang)) && !(value && value[lang] != null)
    )
    .map((key) => [key, translate(key)])
    .sort((a, b) => a[1].localeCompare(b[1]));

  return (
    <div className={`LocalizedString ${errors.length ? "hasError" : ""}`.trim()}>
      <div className={`label ${required ? "required" : ""}`.trim()} id={`${formId}-${name}-label`}>
        {translate(title)}
        &nbsp;
        {required ? (
          <span className="asterisk" title={translate("Error.requiredField")}>
            *
          </span>
        ) : (
          ""
        )}
      </div>
      <span className="fieldDescription">
        {description && translate(description) !== description ? translate(description) : ""}
      </span>
      {errors.map((error, index) => (
        <div className="error" key={index}>
          {translate(`Error.${error.keyword}`, objectMap(error.params, translate))}
        </div>
      ))}
      {languagesPresent.map((lang) => (
        <TextInput
          property={lang}
          translate={translate}
          shouldFormComponentFocus={shouldFormComponentFocus}
          title={lang}
          key={lang}
          displayOptions={schema._display}
        />
      ))}
      {languagesAvailable.length > 0 && (
        <label>
          {translate("resourceFormWidgets.localizedTextarea.addLanguage")}
          :&nbsp;
          <select
            onChange={(event) => {
              const lang = event.target.options[event.target.selectedIndex].value;
              lang && setValue(Object.assign(value || {}, { [lang]: "" }), false);
            }}
          >
            <option value="">&nbsp;</option>
            {languagesAvailable.map((lang) => (
              <option value={lang[0]} key={lang[0]}>
                {lang[1]}
              </option>
            ))}
          </select>
        </label>
      )}
    </div>
  );
};

export default withFormData(LocalizedString);
