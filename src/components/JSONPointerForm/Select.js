import React from "react";

import withFormData from "./withFormData";
import { objectMap } from "../../common";

const Select = ({
  name,
  setValue,
  options,
  property,
  translate,
  formId,
  description,
  value = "",
  errors = [],
  title = "",
  className = "",
  required = false
}) => (
  <div
    className={`Select ${property || ""} ${className} ${errors.length ? "hasError" : ""}`.trim()}
  >
    <label htmlFor={`${formId}-${name}`} className={required ? "required" : ""}>
      {translate(title)}
      &nbsp;
      {required ? (
        <span className="asterisk" title={`${translate("Error.requiredField")}`}>
          *
        </span>
      ) : (
        ""
      )}
    </label>
    <span className="fieldDescription">
      {description && translate(description) !== description ? translate(description) : ""}
    </span>
    {errors.map((error, index) => (
      <div className="error" key={index}>
        {translate(`Error.${error.keyword}`, objectMap(error.params, translate))}
      </div>
    ))}
    <select
      name={name}
      value={value}
      id={`${formId}-${name}`}
      onChange={(e) => setValue(e.target.value)}
    >
      <option value={null} />
      {options.map((option) => (
        <option key={option} value={option}>
          {translate(option)}
        </option>
      ))}
    </select>
  </div>
);

export default withFormData(Select);
