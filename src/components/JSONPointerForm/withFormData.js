import React, { memo, useContext, useState } from "react";
import jsonPointer from "json-pointer";
import { FormContext, PathContext } from "../../contexts";

const withFormData = (BaseComponent) => {
  const formComponent = memo((props) => {
    const { getValue, formId, setValue, getValidationErrors, shouldFormComponentFocus } =
      useContext(FormContext);
    const path = useContext(PathContext);
    const parents = path || [];
    const nextPath = props.property != null ? [...parents, props.property] : parents;
    const name = jsonPointer.compile(nextPath);

    return (
      <PathContext.Provider value={nextPath}>
        <BaseComponent
          name={name}
          value={getValue(name)}
          formId={formId}
          setValue={(value, prune) => setValue(name, value, prune)}
          errors={getValidationErrors(name)}
          shouldFormComponentFocus={shouldFormComponentFocus(name)}
          {...props}
        />
      </PathContext.Provider>
    );
  });

  return formComponent;
};

export default withFormData;
