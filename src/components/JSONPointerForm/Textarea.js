import React from "react";

import withFormData from "./withFormData";

import { appendOnFocus, objectMap } from "../../common";

const Textarea = ({
  name,
  setValue,
  property,
  translate,
  formId,
  description,
  required = false,
  value = "",
  title = "",
  className = "",
  errors = [],
  shouldFormComponentFocus = false
}) => (
  <div
    className={`Textarea ${property || ""} ${className} ${errors.length ? "hasError" : ""}`.trim()}
  >
    <label htmlFor={`${formId}-${name}`} className={required ? "required" : ""}>
      {translate(title)}
      &nbsp;
      {required ? (
        <span className="asterisk" title={translate("Error.requiredField")}>
          *
        </span>
      ) : (
        ""
      )}
    </label>
    <span className="fieldDescription">
      {description && translate(description) !== description ? translate(description) : ""}
    </span>
    {errors.map((error, index) => (
      <div className="error" key={index}>
        {translate(`Error.${error.keyword}`, objectMap(error.params, translate))}
      </div>
    ))}
    <textarea
      name={name}
      value={value}
      id={`${formId}-${name}`}
      placeholder={translate(title)}
      autoFocus={shouldFormComponentFocus}
      onFocus={appendOnFocus}
      onChange={(e) => setValue(e.target.value)}
    />
  </div>
);

export default withFormData(Textarea);
