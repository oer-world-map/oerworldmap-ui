/* global document */
import React from "react";
import PropTypes from "prop-types";
import Select from "react-select";

import withFormData from "./withFormData";
import { objectMap } from "../../common";

class DropdownSelect extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      filter: "",
      dropdown: false,
      labels: props.options.reduce(
        (acc, curr) => Object.assign(acc, { [curr]: props.translate(curr).toLowerCase() }),
        {}
      )
    };
    this.optionFilter = this.optionFilter.bind(this);
  }

  optionFilter() {
    const { filter, labels } = this.state;
    return (option) =>
      !filter ||
      option.toLowerCase().search(filter.trim().toLowerCase()) !== -1 ||
      labels[option].search(filter.trim().toLowerCase()) !== -1;
  }

  render() {
    const {
      name,
      property,
      value,
      options,
      setValue,
      errors,
      title,
      translate,
      className,
      formId,
      required,
      description
    } = this.props;

    return (
      <div
        ref={(el) => (this.wrapper = el)}
        className={`DropdownSelect ${property || ""} ${className}${errors.length ? " hasErrors" : ""}`.trim()}
        aria-labelledby={`${formId}-${name}-label`}
      >
        <div
          className={`label ${required ? "required" : ""}`.trim()}
          id={`${formId}-${name}-label`}
        >
          {translate(title)}
          &nbsp;
          {required ? (
            <span className="asterisk" title={translate("Error.requiredField")}>
              *
            </span>
          ) : (
            ""
          )}
        </div>
        <span className="fieldDescription">
          {description && translate(description) !== description ? translate(description) : ""}
        </span>
        {errors?.length > 0 && (
          <div className="error">
            {errors
              .map((error) =>
                translate(`Error.${error.keyword}`, objectMap(error.params, translate))
              )
              .join(", ")}
          </div>
        )}
        <Select
          inputId={`${formId}-${name}-${value}`}
          name={name}
          value={
            value
              ? {
                  label: translate(value),
                  value: value
                }
              : undefined
          }
          options={options.filter(this.optionFilter()).map((option) => {
            return {
              label: translate(option),
              value: option
            };
          })}
          onChange={(selected) => {
            setValue(selected?.value);
          }}
          onInputChange={(input) => {
            this.setState({ filter: input });
          }}
          placeholder={translate(title)}
          arrowRenderer={() => <i aria-hidden="true" className="fa fa-chevron-down" />}
          isClearable={true}
          formatCreateLabel={(label) => `${translate("create")}: ${label}`}
          styles={{
            control: (baseStyles, state) => {
              return {
                ...baseStyles,
                "&:hover": {
                  borderColor: state.isFocused ? "var(--orange)" : "var(--blue-dark)"
                },
                borderColor: state.isFocused ? "var(--orange)" : "var(--blue-dark)",
                borderRadius: 2,
                boxShadow: "none",
                cursor: state.isDisabled ? "not-allowed" : "pointer"
              };
            },
            option: (styles, { data, isDisabled, isFocused, isSelected }) => {
              return {
                ...styles,
                backgroundColor: isFocused ? "var(--orange)" : "white",
                color: isFocused ? "white" : "var(--blue)",
                cursor: isDisabled ? "not-allowed" : "pointer"
              };
            }
          }}
        />
      </div>
    );
  }
}

DropdownSelect.propTypes = {
  name: PropTypes.string.isRequired,
  value: PropTypes.string,
  property: PropTypes.string,
  options: PropTypes.arrayOf(PropTypes.string).isRequired,
  setValue: PropTypes.func.isRequired,
  errors: PropTypes.arrayOf(PropTypes.object),
  title: PropTypes.string,
  translate: PropTypes.func.isRequired,
  className: PropTypes.string,
  formId: PropTypes.string.isRequired,
  required: PropTypes.bool,
  description: PropTypes.string
};

DropdownSelect.defaultProps = {
  value: "",
  property: undefined,
  errors: [],
  title: "",
  className: "",
  required: false,
  description: undefined
};

export default withFormData(DropdownSelect);
