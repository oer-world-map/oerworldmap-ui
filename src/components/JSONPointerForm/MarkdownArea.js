/* global MutationObserver */
/* global document */
import React from "react";
import SimpleMdeReact from "react-simplemde-editor";
import Markdown from "markdown-to-jsx";
import ReactDOMServer from "react-dom/server";

import "simplemde/dist/simplemde.min.css";

import withFormData from "./withFormData";
import LinkOverride from "../LinkOverride";
import { objectMap } from "../../common";

const MarkdownArea = ({
  name,
  setValue,
  property,
  translate,
  formId,
  description,
  displayOptions,
  value = "",
  errors = [],
  title = "",
  className = "",
  shouldFormComponentFocus = false,
  required = false
}) => (
  <div
    className={`Textarea ${property || ""} ${className} ${errors.length ? "hasError" : ""}`.trim()}
  >
    <label htmlFor={`${formId}-${name}`} className={required ? "required" : ""}>
      {translate(title)}
      &nbsp;
      {required ? (
        <span className="asterisk" title={translate("Error.requiredField")}>
          *
        </span>
      ) : (
        ""
      )}
    </label>
    <span className="fieldDescription">
      {description && translate(description) !== description ? translate(description) : ""}
    </span>
    {errors.map((error, index) => (
      <div className="error" key={index}>
        {translate(`Error.${error.keyword}`, objectMap(error.params, translate))}
      </div>
    ))}
    <SimpleMdeReact
      name={name}
      value={value}
      id={`${formId}-${name}`}
      onChange={(value) => setValue(value)}
      className="SimpleMDE"
      getMdeInstance={(instance) => {
        const mo = new MutationObserver((e) => {
          const mutation = e.shift();
          if (
            mutation &&
            mutation.attributeName === "class" &&
            !mutation.target.classList.contains("hidden")
          ) {
            instance.codemirror.refresh();
          }
        });

        document &&
          document.getElementById("edit") &&
          mo.observe(document.getElementById("edit"), { attributes: true });

        instance.codemirror.on("focus", (i, e) => !e && i.setCursor(i.getValue().length));
      }}
      options={{
        autofocus: shouldFormComponentFocus,
        status: false,
        spellChecker: false,
        placeholder: translate(title),
        previewRender(value) {
          return ReactDOMServer.renderToString(
            <Markdown
              options={{
                overrides: {
                  a: {
                    component: LinkOverride
                  }
                }
              }}
            >
              {value}
            </Markdown>
          );
        },
        minHeight: (displayOptions.rows || "5").toString() + "em"
      }}
    />
  </div>
);

export default withFormData(MarkdownArea);
