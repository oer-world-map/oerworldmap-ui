/* global document */
import React from "react";
import PropTypes from "prop-types";
import { debounce } from "lodash";
import Select from "react-select";

import withFormData from "./withFormData";
import withApi from "../withApi";
import withI18n from "../withI18n";
import { getURL, objectMap } from "../../common";

class RemoteSelect extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      filter: "",
      options: []
    };
    this.updateOptions = debounce(this.updateOptions.bind(this), 200);
    this.arraySortByTranslatedName = this.arraySortByTranslatedName.bind(this);
  }

  componentDidMount() {
    this.updateOptions();
  }

  arraySortByTranslatedName(a, b) {
    return this.props.translate(a.name).localeCompare(this.props.translate(b.name));
  }

  updateOptions() {
    const { schema, api } = this.props;
    const { filter } = this.state;

    let apiCall;

    if (schema.properties.inScheme) {
      apiCall = api.vocab(schema.properties.inScheme.properties["@id"].enum[0]);
    } else {
      const params = {
        q: `${filter}*`,
        "filter.about.@type": schema.properties["@type"].enum
      };
      const url = getURL({
        path: "/resource/",
        params
      });
      apiCall = api.get(url);
    }
    apiCall.then((result) =>
      this.setState({ options: result.member.map((member) => member.about) })
    );
  }

  render() {
    const {
      name,
      property,
      value,
      setValue,
      errors,
      title,
      translate,
      className,
      formId,
      required,
      schema,
      description
    } = this.props;
    const { options } = this.state;
    const sortOptions = name.split("/")[1] === "activityField";

    const sortedOptions = sortOptions ? options.sort(this.arraySortByTranslatedName) : options;

    return (
      <div
        ref={(el) => (this.wrapper = el)}
        className={`RemoteSelect ${property || ""} ${className} ${errors.length ? "hasError" : ""}`.trim()}
        aria-labelledby={`${formId}-${name}-label`}
        onKeyDown={(e) => {
          if (e.keyCode === 27) {
            this.setState({ options: [] });
          }
        }}
        role="group"
        tabIndex="0"
      >
        <div
          className={`label ${required ? "required" : ""}`.trim()}
          id={`${formId}-${name}-label`}
        >
          {translate(title)}
        </div>
        <span className="fieldDescription">
          {description && translate(description) !== description ? translate(description) : ""}
        </span>
        {errors.map((error, index) => (
          <div className="error" key={index}>
            {translate(`Error.${error.keyword}`, objectMap(error.params, translate))}
          </div>
        ))}
        <Select
          name={name}
          value={
            value
              ? {
                  label: translate(value.name),
                  value: value?.["@id"]
                }
              : undefined
          }
          options={sortedOptions.map((option) => {
            return {
              label: translate(option.name),
              option: option,
              value: option["@id"]
            };
          })}
          onChange={(selected) => {
            setValue(selected?.option);
          }}
          onFocus={() => schema.properties.inScheme && this.updateOptions()}
          onInputChange={(input) => {
            this.setState({ filter: input });
            if (input || schema.properties.inScheme) {
              this.updateOptions();
            }
          }}
          placeholder={translate("ClientTemplates.resource_typehead.search")
            .concat(" ")
            .concat(
              (schema.properties["@type"].enum || [])
                .map((type) => translate(type))
                .join(` ${translate("ClientTemplates.resource_typehead.or")} `)
            )}
          arrowRenderer={() => <i aria-hidden="true" className="fa fa-chevron-down" />}
          isClearable={true}
          formatCreateLabel={(label) => `${translate("create")}: ${label}`}
          styles={{
            control: (baseStyles, state) => {
              return {
                ...baseStyles,
                "&:hover": {
                  borderColor: state.isFocused ? "var(--orange)" : "var(--blue-dark)"
                },
                borderColor: state.isFocused ? "var(--orange)" : "var(--blue-dark)",
                borderRadius: 2,
                boxShadow: "none",
                cursor: state.isDisabled ? "not-allowed" : "pointer"
              };
            },
            option: (styles, { data, isDisabled, isFocused, isSelected }) => {
              return {
                ...styles,
                backgroundColor: isFocused ? "var(--orange)" : "white",
                color: isFocused ? "white" : "var(--blue)",
                cursor: isDisabled ? "not-allowed" : "pointer"
              };
            }
          }}
        />
      </div>
    );
  }
}

RemoteSelect.propTypes = {
  name: PropTypes.string.isRequired,
  value: PropTypes.objectOf(PropTypes.any),
  property: PropTypes.string,
  setValue: PropTypes.func.isRequired,
  errors: PropTypes.arrayOf(PropTypes.object),
  title: PropTypes.string,
  translate: PropTypes.func.isRequired,
  schema: PropTypes.objectOf(PropTypes.any).isRequired,
  api: PropTypes.objectOf(PropTypes.any).isRequired,
  className: PropTypes.string,
  formId: PropTypes.string.isRequired,
  required: PropTypes.bool,
  description: PropTypes.string
};

RemoteSelect.defaultProps = {
  value: undefined,
  property: undefined,
  errors: [],
  title: "",
  className: "",
  required: false,
  description: undefined
};

export default withI18n(withApi(withFormData(RemoteSelect)));
