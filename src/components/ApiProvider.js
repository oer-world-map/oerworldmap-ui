import React from "react";
import PropTypes from "prop-types";

import Api from "../api";
import { ApiContext } from "../contexts";

class ApiProvider extends React.Component {
  render() {
    const { children, config } = this.props;
    const api = new Api(config);
    return <ApiContext.Provider value={api}>{React.Children.only(children)}</ApiContext.Provider>;
  }
}

ApiProvider.propTypes = {
  children: PropTypes.node.isRequired,
  config: PropTypes.objectOf(PropTypes.any).isRequired
};

export default ApiProvider;
