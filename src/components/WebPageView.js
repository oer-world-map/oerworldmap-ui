import React from "react";
import Markdown from "markdown-to-jsx";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import { parseTemplate } from "url-template";
import ResourceImage from "./ResourceImage";

import withI18n from "./withI18n";
import withUser from "./withUser";
import withConfig from "./withConfig";
import Block from "./Block";
import ItemList from "./ItemList";
import Link from "./Link";
import ConceptTree from "./ConceptTree";
import WebPageUserActions from "./WebPageUserActions";
import SocialLinks from "./SocialLinks";
import PublicationProfiles from "./PublicationProfiles";
import Comments from "./Comments";
import Topline from "./Topline";
import Lighthouses from "./Lighthouses";
import LinkOverride from "./LinkOverride";
import { useEffect } from 'react';

import markerIcon from "../../node_modules/leaflet/dist/images/marker-icon.png";

import { formatURL, formatDate, isNode } from "../common";
import centroids from "../json/centroids.json";
import expose from "../expose";
import "../styles/components/WebPageView.pcss";

const WebPageView = ({ about, feature = null, locales, expandAll = false, isLiveEvent = undefined, moment, schema, translate, user = null, view, _self }) => {

  const lighthouses = (about.objectIn || []).filter((action) => action["@type"] === "LighthouseAction");
  const likes = (about.objectIn || []).filter((action) => action["@type"] === "LikeAction");

  const sortedNames = about.name
    ? [
        ...Object.keys(about.name)
        .filter((n) => locales.includes(n))
        .sort((a, b) => locales.indexOf(a) - locales.indexOf(b)),
        ...Object.keys(about.name).filter((n) => !locales.includes(n))
      ]
    : [];

  const sortedDescriptions = about.description
    ? [
        ...Object.keys(about.description)
        .filter((n) => locales.includes(n))
        .sort((a, b) => locales.indexOf(a) - locales.indexOf(b)),
        ...Object.keys(about.description).filter((n) => !locales.includes(n))
      ]
    : [];

  const country =
    (about &&
      about.location &&
      about.location[0] &&
      about.location[0].address &&
      about.location[0].address.addressCountry) ||
    null;

  const geometry = (feature && feature.geometry) || null;

  const centroid = (country && centroids[country]) || null;

  // FIXME: The map maybe should be a separate component so it doesn't have to be reloaded whenever e.g. the user gives a Like (that changes "about", which this depends on)
  useEffect(() => {
    if (!document.getElementById("static-map")) return;
    const L = require("leaflet");

    const map = L.map("static-map", { attributionControl: false });
    L.tileLayer("https://tile.openstreetmap.org/{z}/{x}/{y}.png", {
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
      maxZoom: 19,
      minZoom: 2
    }).addTo(map);

    if (geometry) {
      const feature2 = {
        type: "Feature",
        geometry
      };
      const pointGeojson = L.geoJSON(feature2, {
        pointToLayer: function (feat, latlng) {
          const marker = L.marker(latlng, {
            icon: L.icon({
              iconUrl: markerIcon,
              iconSize: [25, 41],
              iconAnchor: [12, 41]
            })
          });
          return marker;
        }
      });
      pointGeojson.addTo(map);
      map.setView([geometry.coordinates[1], geometry.coordinates[0]], 4);
    } else if (centroid) {
      map.setView(centroid);
    }
    return () => {map.remove();};
  }, [about, feature]);

  return (
    <div className={`WebPageView ${about["@type"]}`}>
      <div className="WebPageViewHeader">
        <h2>
          {about.name && Object.keys(about.name).length > 1 ? (
            <Tabs>
              {sortedNames.map((lang) => (
                <TabPanel className="inline" key={`panel-${lang}`}>
                  {about.name[lang]}
                </TabPanel>
              ))}

              {about.alternateName && (
                <span className="alternate">&nbsp; ({translate(about.alternateName)})</span>
              )}

              <br />
              <span className="hint">
                {translate("WebPageView.alsoAvailableIn")}
                &nbsp;
              </span>
              <TabList>
                {sortedNames.map((lang, i) => (
                  <Tab key={`tab-${lang}`}>
                    {translate(lang)}
                    {i !== Object.keys(about.name).length - 1 && (
                      <React.Fragment>&nbsp;</React.Fragment>
                    )}
                  </Tab>
                ))}
              </TabList>
            </Tabs>
          ) : (
            <React.Fragment>
              {translate(about.name)}

              {about.alternateName && (
                <span className="alternate">&nbsp; ({translate(about.alternateName)})</span>
              )}
            </React.Fragment>
          )}
          {about?.location?.[0]?.online === true && (
            <span className="online-indicator">
              <i className="fa fa-headset" />
              <span>Online</span>
            </span>
          )}
        </h2>

        <Topline about={about} />

        {isLiveEvent && (
          <div className="isLiveEvent">
            <a
              href={`https://twitter.com/hashtag/${about.hashtag.replace("#", "")}`}
              target="_blank"
              rel="noopener noreferrer"
              className="liveBtn"
            >
              <button className="btn">
                <i className="fa fa-external-link" />
                &nbsp;
                {translate("WebPageView.takingPlace")}
              </button>
            </a>
          </div>
        )}

        {expose("userActions", user) && (
          <WebPageUserActions about={about} view={view} schema={schema} />
        )}
      </div>

      <div className="row stack-700 stack-gutter-2em">
        <div className="col two-third">
          <div
            className="text-large"
            // style={{ paddingTop: '2em' }}
          >
            {about.sameAs && <SocialLinks links={about.sameAs} />}

            <Block
              className="first description"
              title={translate(`${about["@type"]}.description`)}
              type={about["@type"]}
            >
              {about.description ? (
                Object.keys(about.description).length > 1 ? (
                  <Tabs>
                    {sortedDescriptions.map((lang) => (
                      <TabPanel key={`panel-${lang}`}>
                        <Markdown
                          options={{
                            overrides: {
                              a: {
                                component: LinkOverride
                              }
                            }
                          }}
                        >
                          {about.description[lang]}
                        </Markdown>
                      </TabPanel>
                    ))}

                    <span className="hint">
                      {translate("WebPageView.alsoAvailableIn")}
                      &nbsp;
                    </span>
                    <TabList>
                      {sortedDescriptions.map((lang, i) => (
                        <Tab key={`tab-${lang}`}>
                          <span>{translate(lang)}</span>
                          {i !== Object.keys(about.description).length - 1 && (
                            <React.Fragment>&nbsp;</React.Fragment>
                          )}
                        </Tab>
                      ))}
                    </TabList>
                  </Tabs>
                ) : (
                  <Markdown
                    options={{
                      overrides: {
                        a: {
                          component: LinkOverride
                        }
                      }
                    }}
                  >
                    {translate(about.description)}
                  </Markdown>
                )
              ) : (
                <p>
                  <i>
                    {translate("WebPageView.missingDescription")}
                    {expose("editEntry", user, about) && (
                      <Link href="#edit">
                        &nbsp;
                        {translate("WebPageView.helpUs")}
                      </Link>
                    )}
                    {!user && about["@type"] !== "Person" && (
                      <a href={`/.login?continue=${_self}`}>
                        &nbsp;
                        {translate("WebPageView.helpUs")}
                      </a>
                    )}
                  </i>
                </p>
              )}
            </Block>

            {about.url && (
              <>
                <h4>{translate("link")}</h4>
                <p>
                  <a
                    href={about.url}
                    target="_blank"
                    rel="noopener noreferrer"
                    className="boxedLink"
                  >
                    <i className="fa fa-external-link" /> {formatURL(about.url)}
                  </a>
                </p>
              </>
            )}

            {about.apiDocumentation && (
              <>
                <h4>{translate("apiDocumentation")}</h4>
                <p>
                  <a
                    href={about.url}
                    target="_blank"
                    rel="noopener noreferrer"
                    className="boxedLink"
                  >
                    <i className="fa fa-external-link" /> {formatURL(about.apiDocumentation)}
                  </a>
                </p>
              </>
            )}

            <hr className="border-grey" />

            {about.keywords && (
              <Block
                title={translate(`${about["@type"]}.keywords`)}
                addButton={expose("editEntry", user, about) && ".KeywordSelect.keywords"}
              >
                <ul className="spaceSeparatedList">
                  {about.keywords
                    .sort((a, b) => a > b)
                    .map((keyword) => (
                      <li key={keyword}>
                        <Link
                          href={`/resource/?filter.about.keywords=["${encodeURIComponent(keyword.toLowerCase())}"]`}
                        >
                          {keyword}
                        </Link>
                      </li>
                    ))}
                </ul>
              </Block>
            )}

            {about.about && (
              <Block
                className="list"
                title={translate(`${about["@type"]}.about`)}
                addButton={expose("editEntry", user, about) && ".List.about"}
              >
                <ConceptTree
                  concepts={require("../json/esc.json").hasTopConcept}
                  include={about.about.map((concept) => concept["@id"])}
                  className="ItemList recursive"
                  linkTemplate={'/resource/?filter.about.about.@id=["{@id}"]'}
                />
              </Block>
            )}

            {about.audience && (
              <Block
                className="list"
                title={translate(`${about["@type"]}.audience`)}
                addButton={expose("editEntry", user, about) && ".List.audience"}
              >
                <ConceptTree
                  concepts={require("../json/isced-1997.json").hasTopConcept}
                  include={about.audience.map((concept) => concept["@id"])}
                  className="ItemList"
                  linkTemplate={'/resource/?filter.about.audience.@id=["{@id}"]'}
                />
              </Block>
            )}

            {["primarySector", "secondarySector"].map(
              (prop) =>
                about[prop] && (
                  <Block
                    key={prop}
                    className="list"
                    title={translate(`${about["@type"]}.${prop}`)}
                    addButton={expose("editEntry", user, about) && `.List.${prop}`}
                  >
                    <ConceptTree
                      concepts={require("../json/sectors.json").hasTopConcept}
                      include={about[prop].map((concept) => concept["@id"])}
                      className="ItemList"
                      linkTemplate={`/resource/?filter.about.${prop}.@id=["{@id}"]`}
                    />
                  </Block>
                )
            )}

            {about.isFundedBy && about.isFundedBy.some((grant) => grant.isAwardedBy) && (
              <Block
                collapsible={
                  !expandAll &&
                  [].concat(
                    ...about.isFundedBy
                      .filter((grant) => grant.isAwardedBy)
                      .map((grant) => grant.isAwardedBy)
                  ).length > 3
                }
                collapsibleType="show-all"
                className="list"
                title={translate(`${about["@type"]}.isFundedBy`)}
                addButton={expose("editEntry", user, about) && ".List.isAwardedBy"}
              >
                <ItemList
                  listItems={[]
                    .concat(
                      ...about.isFundedBy
                        .filter((grant) => grant.isAwardedBy)
                        .map((grant) => grant.isAwardedBy)
                    )
                    .sort((a, b) => translate(a.name) > translate(b.name))}
                  className="prominent"
                />
              </Block>
            )}

            {about.isFundedBy && about.isFundedBy.some((grant) => grant.hasMonetaryValue) && (
              <Block title={translate(`${about["@type"]}.budget`)}>
                <ul className="commaSeparatedList">
                  {about.isFundedBy
                    .filter((grant) => grant.hasMonetaryValue)
                    .map((grant, i) => (
                      <li key={i}>{grant.hasMonetaryValue}</li>
                    ))}
                </ul>
              </Block>
            )}

            {about.contactPoint && (
              <Block
                className="list"
                title={translate(`${about["@type"]}.contactPoint`)}
                addButton={expose("editEntry", user, about) && ".List.contactPoint"}
              >
                <ItemList
                  listItems={about.contactPoint.sort(
                    (a, b) => translate(a.name) > translate(b.name)
                  )}
                  className="prominent"
                />
              </Block>
            )}

            {about.isPartOf && (
              <Block
                className="list"
                title={translate(`${about["@type"]}.isPartOf`)}
                addButton={expose("editEntry", user, about) && ".RemoteSelect.isPartOf"}
              >
                <ItemList
                  listItems={[about.isPartOf].sort(
                    (a, b) => translate(a.name) > translate(b.name)
                  )}
                  className="prominent"
                />
              </Block>
            )}

            {about.hasPart && (
              <Block
                collapsible={!expandAll && about.hasPart.length > 3}
                collapsibleType="show-all"
                className="list"
                title={translate(`${about["@type"]}.hasPart`)}
                addButton={expose("editEntry", user, about) && ".RemoteSelect.hasPart"}
              >
                <ItemList
                  listItems={about.hasPart.sort((a, b) => translate(a.name) > translate(b.name))}
                  className="prominent"
                />
              </Block>
            )}

            {["result", "resultOf", "provides", "provider", "agent"].map(
              (prop) =>
                about[prop] && (
                  <Block
                    key={prop}
                    collapsible={!expandAll && about[prop].length > 3}
                    collapsibleType="show-all"
                    className="list"
                    title={translate(`${about["@type"]}.${prop}`)}
                  >
                    <ItemList
                      listItems={about[prop].sort(
                        (a, b) => translate(a.name) > translate(b.name)
                      )}
                      className="prominent"
                    />
                  </Block>
                )
            )}

            {about.agentIn && about.agentIn.some((item) => item["@type"] === "Action") && (
              <Block
                collapsible={
                  !expandAll &&
                  about.agentIn.filter((item) => item["@type"] === "Action").length > 3
                }
                collapsibleType="show-all"
                className="list"
                title={translate(`${about["@type"]}.agentIn`)}
              >
                <ItemList
                  listItems={about.agentIn
                    .filter((item) => item["@type"] === "Action")
                    .sort((a, b) => translate(a.name) > translate(b.name))}
                  className="prominent"
                />
              </Block>
            )}

            {about.agentIn &&
              about.agentIn.some((item) => item["@type"] === "LighthouseAction") && (
                <Block
                  collapsible={
                    !expandAll &&
                    about.agentIn.filter((item) => item["@type"] === "LighthouseAction").length >
                      3
                  }
                  collapsibleType="show-all"
                  className="list"
                  title={translate("Lighthouses")}
                >
                  <ItemList
                    listItems={about.agentIn
                      .filter((action) => action["@type"] === "LighthouseAction")
                      .map((lighthouseAction) => lighthouseAction.object)
                      .sort((a, b) => translate(a["@id"]) > translate(b["@id"]))}
                    className="prominent"
                  />
                </Block>
              )}

            {about.agentIn && about.agentIn.some((item) => item["@type"] === "LikeAction") && (
              <Block
                collapsible={
                  !expandAll &&
                  about.agentIn.filter((item) => item["@type"] === "LikeAction").length > 3
                }
                collapsibleType="show-all"
                className="list"
                title={translate("Likes")}
              >
                <ItemList
                  listItems={about.agentIn
                    .filter((action) => action["@type"] === "LikeAction")
                    .filter((LikeAction) => !!LikeAction.object)
                    .map((LikeAction) => LikeAction.object)
                    .sort((a, b) => translate(a["@id"]) > translate(b["@id"]))}
                  className="prominent"
                />
              </Block>
            )}

            {about.awards && about.awards.some((grant) => grant.funds) && (
              <Block
                collapsible={
                  !expandAll &&
                  [].concat(
                    ...about.awards.filter((grant) => grant.funds).map((grant) => grant.funds)
                  ).length > 3
                }
                collapsibleType="show-all"
                className="list"
                title={translate(`${about["@type"]}.funds`)}
              >
                <ItemList
                  listItems={[]
                    .concat(
                      ...about.awards.filter((grant) => grant.funds).map((grant) => grant.funds)
                    )
                    .sort((a, b) => translate(a.name) > translate(b.name))}
                  className="prominent"
                />
              </Block>
            )}

            {["participant", "participantIn"].map(
              (prop) =>
                about[prop] && (
                  <Block
                    key={prop}
                    collapsible={!expandAll && about[prop].length > 3}
                    collapsibleType="show-all"
                    className="list"
                    title={translate(`${about["@type"]}.${prop}`)}
                    addButton={expose("editEntry", user, about) && `.List.${prop}`}
                  >
                    <ItemList
                      listItems={about[prop].sort(
                        (a, b) => translate(a.name) > translate(b.name)
                      )}
                      className="prominent"
                    />
                  </Block>
                )
            )}

            {[
              "member",
              "memberOf",
              "affiliation",
              "affiliate",
              "organizer",
              "organizerFor",
              "performer",
              "performerIn",
              "attendee",
              "attends",
              "created",
              "creator",
              "publication",
              "publisher",
              "manufacturer",
              "manufactured",
              "instrument",
              "instrumentIn",
              "isRelatedTo",
              "isBasedOn",
              "isBasisFor",
              "item"
            ].map(
              (prop) =>
                about[prop] && (
                  <Block
                    key={prop}
                    collapsible={!expandAll && about[prop].length > 3}
                    collapsibleType="show-all"
                    className="list"
                    title={translate(`${about["@type"]}.${prop}`)}
                    addButton={expose("editEntry", user, about) && `.List.${prop}`}
                  >
                    <ItemList
                      listItems={about[prop].sort(
                        (a, b) => translate(a.name) > translate(b.name)
                      )}
                      className="prominent"
                    />
                  </Block>
                )
            )}

            {lighthouses.length > 0 && about["@id"] && (
              <Block id="lighthouses" title={translate("ResourceIndex.read.lighthouses.title")}>
                <Lighthouses lighthouses={lighthouses} about={about} />
              </Block>
            )}

            {about["@id"] && about["@type"] !== "Person" && (
              <Block title={translate("ResourceIndex.read.comments")}>
                <Comments comments={about.comment} about={about} schema={schema} _self={_self} />
              </Block>
            )}
          </div>
        </div>

        <aside className="col one-third">
          <ResourceImage about={about} className="logo" view={view} disableDefault />

          {(lighthouses.length > 0 ||
            likes.length > 0 ||
            about["@type"] === "Policy") && (
            <div className="Block" style={{ marginTop: "0px" }}>
              <ul className="ItemList prominent">
                {lighthouses.length > 0 && (
                  <li>
                    <a href="#lighthouses">
                      <div className="item lighthouses">
                        <i aria-hidden="true" className="bg-highlight-color bg-important">
                          <img src="/public/lighthouse_16px_white.svg" alt="Lighthouse" />
                        </i>
                        <span>
                          {translate("Lighthouses")}
                          &nbsp; ({lighthouses.length})
                        </span>
                      </div>
                    </a>
                  </li>
                )}
                {likes.length > 0 && (
                  <li>
                    <div className="item">
                      <i
                        aria-hidden="true"
                        className="fa fa-thumbs-up bg-highlight-color bg-important"
                      />
                      <span>
                        {translate("Likes")}
                        &nbsp; ({likes.length})
                      </span>
                    </div>
                  </li>
                )}
              </ul>
            </div>
          )}

          {about.additionalType && (
            <Block title={translate(`${about["@type"]}.additionalType`)}>
              {about.additionalType.map((type) => (
                <React.Fragment key={type["@id"]}>
                  <Link
                    href={parseTemplate(
                      '/resource/?filter.about.additionalType.@id=["{@id}"]'
                    ).expand(type)}
                  >
                    {translate(type.name)}
                  </Link>
                  <br />
                </React.Fragment>
              ))}
            </Block>
          )}

          {about.scope && (
            <Block title={translate(`${about["@type"]}.scope`)}>
              {about.scope.map((scope) => (
                <React.Fragment key={scope["@id"]}>
                  <Link
                    href={parseTemplate('/resource/?filter.about.scope.@id=["{@id}"]').expand(
                      scope
                    )}
                  >
                    {translate(scope.name)}
                  </Link>
                  <br />
                </React.Fragment>
              ))}
            </Block>
          )}

          {about.focus && (
            <Block title={translate(`${about["@type"]}.focus`)}>
              {about.focus.map((focus) => (
                <React.Fragment key={focus}>
                  <Link href={`/resource/?filter.about.focus.keyword=["${focus}"]`}>
                    {translate(focus)}
                  </Link>
                  <br />
                </React.Fragment>
              ))}
            </Block>
          )}

          {about.award && (
            <Block className="list" title={translate(`${about["@type"]}.award`)}>
              <ul className="ItemList award">
                {about.award.map((award) => (
                  <li key={award}>
                    <a className="item" href={award} target="_blank" rel="noopener noreferrer">
                      <img
                        src={award}
                        className="awardImage"
                        alt={translate(`${about["@type"]}.award`)}
                      />
                    </a>
                  </li>
                ))}
              </ul>
            </Block>
          )}

          {about.publicationProfiles && (
            <Block className="list" title={translate(`${about["@type"]}.publicationProfiles`)}>
              <PublicationProfiles links={about.publicationProfiles} />
            </Block>
          )}

          {about.email && !isNode() && (
            <Block title={translate(`${about["@type"]}.email`)}>
              <p>
                <a href={`mailto:${about.email}`}>{about.email}</a>
              </p>
            </Block>
          )}

          {about.identifier && (
            <Block title={translate(`${about["@type"]}.identifier`)}>{about.identifier}</Block>
          )}

          {about.location &&
            about.location
              .filter((location) => !!location.address)
              .map((location, i) => (
                <Block title={translate(`${about["@type"]}.location`)} key={i}>
                  <>
                    {geometry && (
                      <div id="static-map"></div>
                    )}
                    <p>
                      {location.address.streetAddress && [
                        location.address.streetAddress,
                        <br key="br" />
                      ]}
                      {location.address.postalCode}
                      {location.address.postalCode && location.address.addressLocality && (
                        <span>,&nbsp;</span>
                      )}
                      {location.address.addressLocality}
                      {(location.address.postalCode || location.address.addressLocality) && (
                        <br />
                      )}
                      {location.address.addressRegion && [
                        <Link href={`/country/${location.address.addressRegion.replace('.', '/')}`}>
                          {translate(location.address.addressRegion)}<br />
                        </Link>
                      ]}
                      {location.address.addressCountry && (
                        <Link href={`/country/${location.address.addressCountry}`}>
                          {translate(location.address.addressCountry)}
                        </Link>
                      )}
                    </p>
                  </>
                </Block>
              ))}

          {about.activityField && (
            <Block className="list" title={translate(`${about["@type"]}.activityField`)}>
              <ConceptTree
                concepts={require("../json/activities.json").hasTopConcept}
                include={about.activityField.map((concept) => concept["@id"])}
                className="ItemList recursive"
                linkTemplate={'/resource/?filter.about.activityField.@id=["{@id}"]'}
              />
            </Block>
          )}

          {about.spatialCoverage && (
            <Block title={translate(`${about["@type"]}.spatialCoverage`)}>
              <Link href={`/resource/?filter.about.spatialCoverage=["${about.spatialCoverage}"]`}>
                {translate(about.spatialCoverage)}
              </Link>
            </Block>
          )}

          {about.startTime && (
            <Block title={translate(`${about["@type"]}.time`)}>
              {formatDate(about.startTime, moment)}
              {about.endTime && (
                <span>
                  &nbsp;–&nbsp;
                  {formatDate(about.endTime, moment)}
                </span>
              )}
            </Block>
          )}

          {about.startDate && (
            <Block title={translate(`${about["@type"]}.date`)}>
              {formatDate(about.startDate, moment)}
              {about.endDate && (
                <span>
                  &nbsp;–&nbsp;
                  {formatDate(about.endDate, moment)}
                </span>
              )}
            </Block>
          )}

          {about.datePublished && [
            <Block title={translate(`${about["@type"]}.datePublished`)}>
              {formatDate(about.datePublished, moment)}
            </Block>,
            about.endDate && (
              <Block title={translate(`${about["@type"]}.endDate`)}>
                {formatDate(about.endDate, moment)}
              </Block>
            )
          ]}

          {about.inLanguage && (
            <Block className="list" title={translate(`${about["@type"]}.inLanguage`)}>
              <ul className="commaSeparatedList">
                {about.inLanguage.map((lang) => (
                  <li key={lang}>
                    <Link href={`/resource/?filter.about.inLanguage=["${lang}"]`}>
                      {translate(lang)}
                    </Link>
                  </li>
                ))}
              </ul>
            </Block>
          )}

          {about.hashtag && (
            <Block title={translate(`${about["@type"]}.hashtag`)}>
              <a
                href={`https://twitter.com/hashtag/${about.hashtag.replace("#", "")}`}
                rel="noopener noreferrer"
                target="_blank"
              >
                {about.hashtag}
              </a>
            </Block>
          )}

          {about.license && (
            <Block title={translate(`${about["@type"]}.license`)}>
              <ul className="spaceSeparatedList">
                {about.license.map((license) => (
                  <li key={license["@id"]}>
                    <img
                      className="licenseImage"
                      src={license.image}
                      alt={translate(license.name)}
                    />
                  </li>
                ))}
              </ul>
            </Block>
          )}
        </aside>
      </div>
    </div>
  );

};

export default withConfig(withI18n(withUser(WebPageView)));
