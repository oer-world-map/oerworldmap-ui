/* global document */
/* global window */
import React, { useContext, useState } from "react";

import { ReactiveList, ReactiveComponent, StateProvider } from "@appbaseio/reactivesearch";

import { quickSearchTypes } from "../common";
import withI18n from "./withI18n";
import withEmitter from "./withEmitter";
import withConfig from "./withConfig";
import ResultList from "./ResultList";
import Calendar from "./Calendar";
import { FilterContext } from "../contexts";

const SearchResults = ({
  emitter,
  translate,
  filterIDs,
  iso3166,
  params,
  region,
  showMobileFilters,
  subFilters
}) => {
  const { sorts } = useContext(FilterContext);
  const [showPastEvents, setShowPastEvents] = useState(false);
  const [onlyOnline, setOnlyOnline] = useState(false);

  if (params.view === "statistics") {
    subFilters.unshift({ componentId: "filter.about.@type", dataField: "about.@type" });
  }

  return (
    <div className="searchResults">
      {params.view === "list" && (
        <StateProvider
          componentIds={["filter.about.@type"]}
          strict={false}
          render={({ searchState }) => {
            const eventSelected =
              (searchState &&
                searchState["filter.about.@type"] &&
                searchState["filter.about.@type"].value === "Event") ||
              false;

            if (eventSelected) {
              return (
                <ReactiveComponent
                  componentId="filter.about.startTime"
                  defaultQuery={() => {
                    if (eventSelected) {
                      const query = {
                        size: 0,
                        aggs: {
                          Calendar: {
                            date_histogram: {
                              min_doc_count: 1,
                              field: "about.startTime",
                              interval: "month"
                            },
                            aggs: {
                              "top_hits#about.@id": {
                                top_hits: {
                                  size: 100,
                                  _source: [
                                    "about.@id",
                                    "about.@type",
                                    "about.name",
                                    "about.startTime",
                                    "about.endDate",
                                    "about.location"
                                  ]
                                }
                              }
                            }
                          }
                        }
                      };

                      if (eventSelected && !showPastEvents) {
                        query.query = {
                          range: {
                            "about.startTime": {
                              gte: "now/d"
                            }
                          }
                        };
                      }
                      if (eventSelected && onlyOnline) {
                        query.query = {
                          terms: {
                            "about.location.online":[true]
                          }
                        };
                      }

                      return query;
                    }
                  }}
                  react={{
                    and: filterIDs
                  }}
                  render={({ aggregations }) => {
                    if (aggregations !== null) {
                      const entries =
                        (aggregations && aggregations.Calendar && aggregations.Calendar.buckets) ||
                        [];
                      return (
                        <>
                          {eventSelected && (
                            <div>
                              <label>
                                <input
                                  type="checkbox"
                                  value={showPastEvents}
                                  onClick={() => setShowPastEvents(!showPastEvents)}
                                  style={{
                                    position: "relative",
                                    top: "2px",
                                    marginRight: "10px",
                                    display: "inline"
                                  }}
                                />
                                {translate("calendar.show.past")}
                              </label>
                              <label>
                                <input
                                  type="checkbox"
                                  value={onlyOnline}
                                  onClick={() => setOnlyOnline(!onlyOnline)}
                                  style={{
                                    position: "relative",
                                    top: "2px",
                                    marginRight: "10px",
                                    display: "inline"
                                  }}
                                />
                                {translate("calendar.onlyOnline")}
                              </label>
                            </div>
                          )}
                          <div>
                            <Calendar entries={entries} />
                          </div>
                        </>
                      );
                    }

                    return <div>Loading...</div>;
                  }}
                />
              );
            }

            return (
              <ReactiveList
                className={`listResults${showMobileFilters ? " hidden-mobile" : ""}`}
                componentId="SearchResult"
                title="Results"
                defaultQuery={() => {
                  const query = {
                    query: {
                      bool: {
                        filter: [
                          {
                            terms: {
                              "about.@type": quickSearchTypes
                            }
                          },
                          {
                            exists: {
                              field: "about.name"
                            }
                          }
                        ]
                      }
                    }
                  };

                  if (iso3166) {
                    query.query.bool.filter.push({
                      term: {
                        "feature.properties.location.address.addressCountry": iso3166.toUpperCase()
                      }
                    });
                  }

                  if (region) {
                    query.query.bool.filter.push({
                      term: {
                        "feature.properties.location.address.addressRegion": `${iso3166.toUpperCase()}.${region.toUpperCase()}`
                      }
                    });
                  }

                  return query;
                }}
                dataField={params.sort}
                sortBy={sorts.find((s) => s.dataField === params.sort)?.sortBy}
                showResultStats={false}
                from={0}
                size={+params.size}
                pagination
                loader={
                  <div className="Loading">
                    <div className="loadingCircle" />
                  </div>
                }
                showLoader
                react={{
                  and: filterIDs
                }}
                render={({ data, resultStats: { numberOfResults } }) => {
                  const items = data || [];
                  emitter.emit("updateCount", numberOfResults);
                  return <ResultList listItems={items} />;
                }}
              />
            );
          }}
        />
      )}

      {params.view === "statistics" && (
        <div className={showMobileFilters ? "hidden-mobile" : ""}>
          <StateProvider
            render={({ searchState }) => {
              const filters = Object.entries(searchState)
                .filter(
                  ([field, { value }]) => field.startsWith("filter.") && value && value.length
                )
                .map(
                  ([field, { value }]) => `${field}=${encodeURIComponent(JSON.stringify(value))}`
                )
                .join("&");
              return (
                <div>
                  {subFilters
                    .filter((filter) => !filter["hidden"])
                    .map(({ dataField, title, componentId }) => (
                      <div key={dataField} className="graphContainer">
                        <h2>{title ? translate(title) : translate(componentId)}</h2>
                        <object
                          loading="lazy"
                          type="image/svg+xml"
                          data={`/stats?field=${dataField}`
                            .concat(
                              searchState.q && searchState.q.value
                                ? `&q=${searchState.q.value}`
                                : ""
                            )
                            .concat(filters ? `&${filters}` : "")
                            .concat(
                              iso3166
                                ? `&filter.feature.properties.location.address.addressCountry=["${iso3166}"]`
                                : ""
                            )
                            .concat(
                              region
                                ? `&filter.feature.properties.location.address.addressRegion=["${iso3166}.${region}"]`
                                : ""
                            )
                            .concat(
                              iso3166
                                ? region
                                  ? `&basePath=/country/${iso3166}/${region}`
                                  : `&basePath=/country/${iso3166}`
                                : ""
                            )}
                        >
                          {translate("graphs.noData")}
                        </object>
                        <div className="graphControls">
                          <button
                            type="button"
                            className="btn"
                            onClick={async () => {
                              const text =
                                `${location.protocol}//${location.host}/stats?field=${dataField}`
                                  .concat(
                                    searchState.q && searchState.q.value
                                      ? `&q=${searchState.q.value}`
                                      : ""
                                  )
                                  .concat(filters ? `&${filters}` : "")
                                  .concat(
                                    iso3166
                                      ? `&filter.feature.properties.location.address.addressCountry=["${iso3166}"]`
                                      : ""
                                  )
                                  .concat(
                                    region
                                      ? `&filter.feature.properties.location.address.addressRegion=["${iso3166}.${region}"]`
                                      : ""
                                  )
                                  .concat(
                                    iso3166
                                      ? region
                                        ? `&basePath=/country/${iso3166}/${region}`
                                        : `&basePath=/country/${iso3166}`
                                      : ""
                                  );
                              navigator.clipboard.writeText(text);
                            }}
                            title={translate("graphs.copyToClipboard")}
                          >
                            <i aria-hidden="true" className="fa fa-clipboard" />
                          </button>
                          <a
                            title={translate("graphs.downloadSVG")}
                            className="btn"
                            href={`/stats?field=${dataField}`
                              .concat(
                                searchState.q && searchState.q.value
                                  ? `&q=${searchState.q.value}`
                                  : ""
                              )
                              .concat(filters ? `&${filters}` : "")
                              .concat(
                                iso3166
                                  ? `&filter.feature.properties.location.address.addressCountry=["${iso3166}"]`
                                  : ""
                              )
                              .concat(
                                region
                                  ? `&filter.feature.properties.location.address.addressRegion=["${iso3166}.${region}"]`
                                  : ""
                              )
                              .concat(
                                iso3166
                                  ? region
                                    ? `&basePath=/country/${iso3166}/${region}`
                                    : `&basePath=/country/${iso3166}`
                                  : ""
                              )
                              .concat(
                                `&download=true&filename=${title ? translate(title) : translate(componentId)}`
                              )}
                          >
                            <i aria-hidden="true" className="fa fa-download" />
                          </a>
                        </div>
                      </div>
                    ))}
                </div>
              );
            }}
          />
        </div>
      )}
    </div>
  );
};

export default withConfig(withEmitter(withI18n(SearchResults)));
