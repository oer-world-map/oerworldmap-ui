import React from "react";
import PropTypes from "prop-types";
import { I18nContext } from "../contexts";

class I18nProvider extends React.Component {
  render() {
    const { children, i18n } = this.props;
    const value = {
      locales: i18n.locales,
      translate: i18n.translate,
      moment: i18n.moment
    };
    return (
      <I18nContext.Provider value={value}>{React.Children.only(children)}</I18nContext.Provider>
    );
  }
}

I18nProvider.propTypes = {
  children: PropTypes.node.isRequired,
  i18n: PropTypes.objectOf(PropTypes.any).isRequired
};

export default I18nProvider;
