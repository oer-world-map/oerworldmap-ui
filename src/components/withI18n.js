import React, { useContext } from "react";
import { I18nContext } from "../contexts";

const withI18n = (BaseComponent) => {
  const LocalizedComponent = (props, context) => {
    const { translate, locales, moment } = useContext(I18nContext);
    return <BaseComponent translate={translate} locales={locales} moment={moment} {...props} />;
  };

  return LocalizedComponent;
};

export default withI18n;
