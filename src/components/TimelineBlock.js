import React from "react";
import Markdown from "markdown-to-jsx";
import withI18n from "./withI18n";
import Link from "./Link";
import ResourcePreview from "./ResourcePreview";
import LinkOverride from "./LinkOverride";
import { formatDate } from "../common";

import "../styles/components/TimelineBlock.pcss";

const TimelineBlock = ({ entry, moment, translate, prominent = false, withBorder = false }) => {
  let { user } = entry || {};
  let message;
  let resource;
  let location;
  if (user && user.location && user.location[0] && user.location[0].address) {
    const href = user.location[0].address.addressCountry;
    location = `${translate("TimelineBlock.from")} [${translate(user.location[0].address.addressCountry)}](/country/${href})`;
  } else {
    location = "";
  }
  let username;
  if (user && user.name) username = `[${translate(user.name)}](/resource/${user["@id"]})`;
  else username = translate("TimelineBlock.someone");

  if (entry.about["@type"] === "LighthouseAction") {
    message = "TimelineBlock.markedAsLighthouse";
    resource = entry.about.object;
  } else if (entry.about["@type"] === "Comment") {
    message = "TimelineBlock.commentedOn";
    resource = entry.about.commentOn;
  } else if (entry.action.type === "add" && entry.about["@type"] === "Person") {
    user = entry.about;
    message = "TimelineBlock.joinedTheOERWorldMap";
    resource = entry.about;
  } else if (entry.action.type === "add" && entry.about["@type"] === "LikeAction") {
    message = "TimelineBlock.liked";
    resource = entry.about.object;
  } else if (entry.action.type === "add") {
    message = "TimelineBlock.added";
    resource = entry.about;
  } else if (entry.action.type === "edit" && entry.about["@type"] === "Person") {
    user = entry.about;
    message = "TimelineBlock.updatedTheirProfile";
    resource = entry.about;
  } else if (entry.action.type === "edit") {
    message = "TimelineBlock.edited";
    resource = entry.about;
  }

  user = user || {};
  resource = resource || {};
  const resourceLink = `[${translate(resource.name)}](/resource/${resource["@id"]})`;

  return (
    <div className={`TimelineBlock${prominent ? " prominent" : ""}`}>
      <React.Fragment>
        <div className="timelineBlockTitle">
          {entry.action.time && (
            <span
              className="timelineBlockMetadataTime"
              title={formatDate(entry.action.time, moment)}
            >
              {moment(entry.action.time).fromNow()}
            </span>
          )}
          &nbsp;
          <Markdown
            options={{
              overrides: {
                a: {
                  component: LinkOverride
                }
              }
            }}
          >
            {translate(message, {
              username: username,
              location: location,
              resource: resourceLink
            })}
          </Markdown>
        </div>

        <div className={`timelineBlockContent${withBorder ? " withBorder" : ""}`}>
          <ResourcePreview about={resource} />

          {entry.about["@type"] === "Comment" && entry.about.text && (
            <div className="comment">
              <div className="commentMetadata">
                <Link href={user["@id"]}>{translate(user.name)}</Link>
              </div>
              <div className="commentText">
                <Markdown
                  options={{
                    overrides: {
                      a: {
                        component: LinkOverride
                      }
                    }
                  }}
                >
                  {translate(entry.about.text)}
                </Markdown>
              </div>
            </div>
          )}
        </div>
      </React.Fragment>
    </div>
  );
};

export default withI18n(TimelineBlock);
