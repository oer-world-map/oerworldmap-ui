import React from "react";

import "../styles/components/SocialLinks.pcss";

const extractDomain = (link) => {
  return new URL(link).host.split(".").slice(-2).join(".");
};

const mastodonPattern = new RegExp("^https://.*?.social/@");

const SocialLinks = ({ links }) => (
  <div className="SocialLinks">
    {links.map((link) => {
      let icon = "fa-solid fa-link";
      let domain = extractDomain(link);

      if (domain == "facebook.com") {
        icon = "fa-brands fa-facebook";
      } else if (domain == "twitter.com" || domain == "x.com") {
        icon = "fa-brands fa-x-twitter";
      } else if (domain == "linkedin.com") {
        icon = "fa-brands fa-linkedin";
      } else if (domain == "instagram.com") {
        icon = "fa-brands fa-instagram";
      } else if (domain == "youtube.com" || domain == "youtube.de") {
        icon = "fa-brands fa-youtube";
      } else if (domain == "gitlab.com") {
        icon = "fa-brands fa-gitlab";
      } else if (domain == "xing.com") {
        icon = "fa-brands fa-xing";
      } else if (domain == "reddit.com") {
        icon = "fa-brands fa-reddit-alien";
      } else if (domain == "slack.com") {
        icon = "fa-brands fa-slack";
      } else if (domain == "soundcloud.com") {
        icon = "fa-brands fa-soundcloud";
      } else if (domain == "stackoverflow.com") {
        icon = "fa-brands fa-stack-overflow";
      } else if (domain == "vimeo.com") {
        icon = "fa-brands fa-vimeo";
      } else if (domain == "wikipedia.org") {
        icon = "fa-brands fa-wikipedia-w";
      } else if (domain == "wordpress.com") {
        icon = "fa-brands fa-wordpress";
      } else if (domain == "slideshare.com") {
        icon = "fa-brands fa-slideshare";
      } else if (domain == "pinterest.com") {
        icon = "fa-brands fa-pinterest-p";
      } else if (domain == "bildung.social") {
        icon = "fediverse";
      } else if (mastodonPattern.test(link)) {
        icon = "fa-brands fa-mastodon";
      }

      return (
        <a key={link} href={link} rel="noopener noreferrer" target="_blank">
          <i aria-hidden="true" className={`${icon}`} />
        </a>
      );
    })}
  </div>
);

export default SocialLinks;
