import React, { useEffect, useState } from "react";

import JsonSchema from "./JSONPointerForm/JsonSchema";
import Form from "./JSONPointerForm/Form";
import Builder from "./JSONPointerForm/Builder";
import validate from "./JSONPointerForm/validate";

import FullModal from "./FullModal";
import withEmitter from "./withEmitter";
import withI18n from "./withI18n";
import withUser from "./withUser";
import { convertToString } from "../common";
import Error from "./Error";

import "../styles/components/WebPageUserActions.pcss";

const WebPageUserActions = ({ user, about, emitter, view, translate, schema }) => {
  const [error, setError] = useState(undefined);

  useEffect(() => {
    emitter.on("post-resource-error", (error) => {
      setError(error);
    });
    return () => {
      emitter.off("post-resource-error");
    };
  }, [error]);

  const lighthouses =
    (about.objectIn || []).filter((action) => action["@type"] === "LighthouseAction") || [];

  const likes = (about.objectIn || []).filter((action) => action["@type"] === "LikeAction") || [];

  const lighthouse =
    lighthouses.find(
      (action) => action.agent && action.agent.some((agent) => user && agent["@id"] === user.id)
    ) ||
    (user
      ? {
          "@type": "LighthouseAction",
          description: { en: "" },
          startTime: new Date().toISOString()
        }
      : null);

  lighthouse &&
    Object.assign(lighthouse, {
      object: {
        "@id": about["@id"],
        "@type": about["@type"]
      },
      agent: [{ "@id": user.id, "@type": "Person" }]
    });

  const like = likes.find(
    (action) => action.agent && action.agent.some((agent) => user && agent["@id"] === user.id)
  );

  const isAttendee = (about.attendee || []).some((attendee) => user && attendee["@id"] === user.id);

  const isPerformer = (about.performer || []).some(
    (performer) => user && performer["@id"] === user.id
  );

  const isAffiliate = (about.affiliate || []).some(
    (affiliate) => user && affiliate["@id"] === user.id
  );

  const toggleLike = () => {
    setError(undefined);
    if (like) {
      emitter.emit("delete", {
        url: `/resource/${like["@id"]}`,
        redirect: { url: `/resource/${about["@id"]}` }
      });
    } else {
      emitter.emit("submit", {
        url: "/resource/",
        redirect: { url: `/resource/${about["@id"]}` },
        data: {
          "@type": "LikeAction",
          object: about,
          agent: [
            {
              "@id": user.id,
              "@type": "Person"
            }
          ],
          startTime: new Date().toISOString()
        }
      });
    }
  };

  const toggle = (property, active) => {
    setError(undefined);
    const data = JSON.parse(JSON.stringify(about));
    if (active) {
      data[property] = data[property].filter((entry) => entry["@id"] !== user.id);
    } else {
      data[property]
        ? data[property].push({ "@id": user.id })
        : (data[property] = [{ "@id": user.id }]);
    }
    emitter.emit("submit", { url: `/resource/${about["@id"]}`, data });
  };

  return (
    <div className="WebPageUserActionsContainer">
      <div className="WebPageUserActions">
        {["Organization", "Action", "Service", "Product", "Event", "WebPage", "Policy"].includes(
          about["@type"]
        ) && (
          <div className="action">
            <form onSubmit={(e) => e.preventDefault() || toggleLike()}>
              <button
                className={`btn ${like ? "active" : ""}`}
                type="submit"
                title={translate("Like")}
              >
                <i aria-hidden="true" className="fa fa-thumbs-up" />
                {translate("Like")}
              </button>
            </form>
          </div>
        )}

        {["Organization", "Action", "Service", "Product", "Event", "WebPage", "Policy"].includes(
          about["@type"]
        ) && (
          <div className="action">
            <a href="#addLighthouse" className={`btn ${lighthouse["@id"] ? "active" : ""}`}>
              <img
                className="i blueLighthouse"
                src="/public/lighthouse_16px_blue.svg"
                alt="Lighthouse"
              />
              <img
                className="i whiteLighthouse"
                src="/public/lighthouse_16px_white.svg"
                alt="Lighthouse"
              />
              {translate("ResourceIndex.read.lightHouse")}
            </a>
          </div>
        )}

        {view === "addLighthouse" && (
          <FullModal className="Lighthouse" closeLink={`/resource/${about["@id"]}`}>
            <Form
              data={lighthouse}
              validate={validate(JsonSchema(schema).get("#/definitions/LighthouseAction"))}
              onSubmit={(data) =>
                emitter.emit("submit", {
                  url: `/resource/${lighthouse["@id"] || ""}`,
                  redirect: { url: `/resource/${about["@id"]}` },
                  data
                })
              }
            >
              <h2>
                {translate("ResourceIndex.read.lightHouse")}
                &nbsp;
                {translate(about["@type"])}
              </h2>
              <p>
                <em>
                  {translate("descriptions.LighthouseAction.description")}
                  &nbsp;
                  <a
                    href="https://oerworldmap.wordpress.com/2017/11/27/identifying-lighthouses/"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    {translate("needHelp")}
                  </a>
                </em>
              </p>
              <hr />
              <Builder schema={JsonSchema(schema).get("#/definitions/LighthouseAction")} />
              <div className="buttons">
                <button className="btn" type="submit">
                  {translate("publish")}
                </button>
              </div>
            </Form>
          </FullModal>
        )}

        {about["@type"] === "Event" && (
          <div className="action">
            <form onSubmit={(e) => e.preventDefault() || toggle("attendee", isAttendee)}>
              <button
                className={`btn ${isAttendee ? "active" : ""}`}
                type="submit"
                title={translate("I'm attending")}
              >
                <i aria-hidden="true" className="fa fa-flag" />
                {translate("entry.action.attending")}
              </button>
            </form>
          </div>
        )}

        {about["@type"] === "Event" && (
          <div className="action">
            <form onSubmit={(e) => e.preventDefault() || toggle("performer", isPerformer)}>
              <button
                className={`btn ${isPerformer ? "active" : ""}`}
                type="submit"
                title={translate("I'm presenting")}
              >
                <i aria-hidden="true" className="fa fa-bullhorn" />
                {translate("entry.action.presenting")}
              </button>
            </form>
          </div>
        )}

        {(about["@type"] === "Organization" || about["@type"] === "Action") && (
          <div className="action">
            <form onSubmit={(e) => e.preventDefault() || toggle("affiliate", isAffiliate)}>
              <button
                className={`btn ${isAffiliate ? "active" : ""}`}
                type="submit"
                title={translate("I'm a member")}
              >
                <i aria-hidden="true" className="fa fa-sitemap" />
                {translate("entry.action.member")}
              </button>
            </form>
          </div>
        )}
      </div>
      <Error error={error}></Error>
    </div>
  );
};

export default withI18n(withEmitter(withUser(WebPageUserActions)));
