/* global window */
import React, { useState } from "react";
import { ReactiveComponent } from "@appbaseio/reactivesearch";

import "../styles/components/Country.pcss";

import Link from "./Link";
import { triggerClick } from "../common";
import withI18n from "./withI18n";
import Icon from "./Icon";

const Country = ({ iso3166, region, translate }) => {
  const [showReports, setShowReports] = useState(false);

  return (
    <div>
      {iso3166 && (
        <div className="FilterBox Country">
          <span className={`countryFlag fi fi-${iso3166.toLowerCase()}`}></span>
          {region ? (
            <>
              <h3>{translate(`${iso3166}.${region}`)}</h3>
              <Link href={`/country/${iso3166}${window.location.search}`} className="closePage">
                &times;
              </Link>
            </>
          ) : (
            <>
              <h3>{translate(iso3166)}</h3>
              <Link href={`/resource/${window.location.search}`} className="closePage">
                &times;
              </Link>
            </>
          )}
          <ReactiveComponent
            componentId="countryReports "
            defaultQuery={() => ({
              _source: ["about.@id", "about.name", "about.@type"],
              query: {
                bool: {
                  filter: {
                    term: {
                      "about.keywords": `countryreport:${iso3166}`
                    }
                  }
                }
              }
            })}
            render={({ data }) =>
              data.length > 0 && (
                <div>
                  <h4
                    onKeyDown={triggerClick}
                    tabIndex="0"
                    role="button"
                    onClick={() => setShowReports(!showReports)}
                  >
                    {translate("CountryIndex.read.countryReports")}
                    <i aria-hidden="true" className={`fa fa-${showReports ? "minus" : "plus"}`} />
                  </h4>

                  <div hidden={!showReports}>
                    {data
                      .sort((a, b) => a.about.dateCreated < b.about.dateCreated)
                      .map((report) => (
                        <div className="countryBlock" key={report.about["@id"]}>
                          <div className="frame">
                            <i aria-hidden="true" className="fa fa-book" />
                          </div>
                          <div className="text">
                            <Link href={`/resource/${report.about["@id"]}`}>
                              {translate(report.about.name)}
                            </Link>
                          </div>
                        </div>
                      ))}
                  </div>
                </div>
              )
            }
          />
        </div>
      )}
    </div>
  );
};

export default withI18n(Country);
