import React from "react";
import PropTypes from "prop-types";
import { ConfigContext } from "../contexts";

class ConfigProvider extends React.Component {
  render() {
    const { children, config } = this.props;
    return (
      <ConfigContext.Provider value={config}>
        {React.Children.only(children)}
      </ConfigContext.Provider>
    );
  }
}

ConfigProvider.propTypes = {
  children: PropTypes.node.isRequired,
  config: PropTypes.objectOf(PropTypes.any).isRequired
};

export default ConfigProvider;
