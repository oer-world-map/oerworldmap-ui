/* global document */
import React from "react";
import withEmitter from "./withEmitter";

import "../styles/components/Page.pcss";
import "../styles/components/WebPage.pcss";

const Page = ({ className, children }) => {
  return (
    <div className={`webPageWrapper ${className || ""}`} role="presentation">
      <div className="WebPage">{children}</div>
    </div>
  );
};

export default withEmitter(Page);
