import React from "react";

import "../styles/components/Log.pcss";

const Log = ({ entries }) => (
  <div className="Log">
    {entries &&
      entries.map((entry) => (
        <div>
          <h2 id={entry.commit}>{entry.commit}</h2>
          <dl>
            <dt>Author</dt>
            <dd>{entry.author}</dd>
            <dt>Date</dt>
            <dd>{entry.date}</dd>
            <dt>Primary Topic</dt>
            <dd>
              <a href={`/resource/${entry.primary_topic}`}>{entry.primary_topic}</a>
            </dd>
          </dl>
        </div>
      ))}
  </div>
);

export default Log;
