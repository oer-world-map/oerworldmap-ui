/* global document */
/* global confirm */
import React from "react";

import WebPageView from "./WebPageView";
import WebPageEdit from "./WebPageEdit";
import WebPageHeader from "./WebPageHeader";

import expose from "../expose";
import withEmitter from "./withEmitter";
import withI18n from "./withI18n";
import withUser from "./withUser";
import Link from "./Link";

import "../styles/components/WebPage.pcss";
import "../styles/components/FormStyle.pcss";

const WebPage = ({
  user,
  about,
  contributor,
  dateModified,
  view,
  feature,
  _self,
  schema,
  emitter,
  translate,
  showOptionalFields = true,
  _links = { refs: [] },
  onSubmit = (formdata) => console.log(formdata)
}) => {
  const date = new Date().toJSON().split("T").shift();
  const isLiveEvent =
    about.startTime &&
    about.startTime <= date &&
    about.endTime &&
    about.endTime >= date &&
    !!about.hashtag;

  return (
    <div
      className="webPageWrapper"
      role="presentation"
      onClick={(e) => {
        const modalDialog = document.querySelector(".WebPage");
        if (!modalDialog.contains(e.target)) {
          if (view !== "edit") {
            emitter.emit("navigate", Link.home);
          }
        }
      }}
    >
      <div className="WebPage">
        <WebPageHeader
          user={user}
          about={about}
          contributor={contributor}
          dateModified={dateModified}
          emitter={emitter}
          translate={translate}
          view={view}
          _self={_self}
          _links={_links}
        />

        <div className="webPageContent">
          {expose("editEntry", user, about) && view === "edit" ? (
            <div id="edit" data-userroles={user && user.groups ? user.groups.join(" ") : ""}>
              <WebPageEdit
                about={about}
                action={about["@id"] ? "edit" : "add"}
                schema={schema}
                closeLink={about["@id"] ? _self : undefined}
                showOptionalFields={showOptionalFields}
                _self={_self}
                onSubmit={onSubmit}
              />
            </div>
          ) : (
            <div id="view">
              <WebPageView
                id="view"
                about={about}
                view={view}
                action={about["@id"] ? "edit" : "add"}
                schema={schema}
                _self={_self}
                isLiveEvent={isLiveEvent}
                feature={feature}
              />
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default withEmitter(withI18n(withUser(WebPage)));
