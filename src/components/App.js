import React from "react";
import "normalize.css";
import "@fortawesome/fontawesome-free/css/all.css";
import "../styles/fonts.pcss";
import "../styles/main.pcss";
import "flag-icons/css/flag-icons.min.css";

import Header from "./Header";
import Loading from "./Loading";
import withUser from "./withUser";

const App = ({ locales, supportedLanguages, children, iso3166, region }) => (
  <div id="wrapper">
    <Header
      locales={locales}
      supportedLanguages={supportedLanguages}
      iso3166={iso3166}
      region={region}
    />
    {children}
    <Loading />
  </div>
);

export default withUser(App);
