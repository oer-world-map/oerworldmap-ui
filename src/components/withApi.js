import React, { useContext } from "react";
import { ApiContext } from "../contexts";

const withApi = (BaseComponent) => {
  const ApiComponent = (props, context) => {
    const api = useContext(ApiContext);
    return <BaseComponent api={api} {...props} />;
  };

  return ApiComponent;
};

export default withApi;
