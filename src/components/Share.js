import React from "react";

import withI18n from "./withI18n";

import "../styles/components/Share.pcss";

const Share = ({ _self, translate, title }) => {
  // share of resources: title is defined, share in list/map view: title undefined
  let subject =
    typeof title === "undefined" ? "OER World Map" : `OER World Map: ${encodeURIComponent(title)}`;

  return (
    <div className="Share">
      <h2>{translate("share.shareResource")}</h2>
      <div className="shareContent">
        <span className="title">{translate("share.permalink")}</span>
        <div className="content">{_self}</div>
      </div>

      <br />

      <div className="embedContent">
        <span className="title">{translate("embed")}</span>
        <div className="content">
          {`<iframe src="${_self}" width="560" height="315" frameborder="0"></iframe>`}
        </div>
      </div>

      <div className="shareIcons">
        <a
          target="_blank"
          rel="noopener noreferrer"
          title="Fediverse"
          href={`https://bildung.social/share?text=@oerinfo ${subject} ${encodeURIComponent(_self)}`}
        >
          <i aria-hidden="true" className="fediverse" />
        </a>
        <a
          target="_blank"
          rel="noopener noreferrer"
          title="X (Twitter)"
          href={`https://x.com/intent/post?text=@OERinfo ${subject} ${encodeURIComponent(_self)}`}
        >
          <i aria-hidden="true" className="fa-brands fa-twitter" />
        </a>
        <a
          target="_blank"
          rel="noopener noreferrer"
          title="Facebook"
          href={`https://www.facebook.com/sharer.php?u=${subject} ${encodeURIComponent(_self)}`}
        >
          <i aria-hidden="true" className="fa-brands fa-facebook" />
        </a>
        <a
          target="_blank"
          rel="noopener noreferrer"
          title="Reddit"
          href={`https://reddit.com/submit?url=${encodeURIComponent(_self)}&title=${subject}`}
        >
          <i aria-hidden="true" className="fa-brands fa-reddit-alien" />
        </a>
        <a
          target="_blank"
          rel="noopener noreferrer"
          title="LinkedIn"
          href={`https://www.linkedin.com/sharing/share-offsite/?url=${encodeURIComponent(_self)}&title=${subject}`}
        >
          <i aria-hidden="true" className="fa-brands fa-linkedin-in" />
        </a>
        <a
          target="_blank"
          rel="noopener noreferrer"
          title="E-Mail"
          href={`mailto:?subject=${subject}&body=${encodeURIComponent(_self)}`}
        >
          <i aria-hidden="true" className="fa fa-envelope" />
        </a>
      </div>
    </div>
  );
};

export default withI18n(Share);
