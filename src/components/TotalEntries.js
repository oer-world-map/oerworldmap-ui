import React, { useEffect, useState } from "react";

import withI18n from "./withI18n";
import withEmitter from "./withEmitter";

const TotalEntries = ({ translate, emitter, className }) => {
  const [total, setTotal] = useState(null);
  useEffect(() => {
    const updateCount = (total) => {
      if (typeof document !== "undefined" && total) {
        document.title = `${total} ${translate("entries")} - OER World Map`;
      }
      setTotal(total);
    };
    emitter.on("updateCount", updateCount);
    return () => emitter.off("updateCount", updateCount);
  }, []);

  return (
    <h3 className={className}>
      {total}
      &nbsp;
      {translate("CountryIndex.read.entriesShown")}
    </h3>
  );
};

export default withEmitter(withI18n(TotalEntries));
