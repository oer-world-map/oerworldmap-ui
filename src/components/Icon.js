import React from "react";

const icons = {
  Service: "desktop",
  Person: "user",
  Organization: "users",
  Action: "gears",
  Concept: "tag",
  ConceptScheme: "sitemap",
  Event: "calendar",
  WebPage: "file",
  Product: "wrench",
  Policy: "balance-scale"
};

const Icon = ({ type }) => (
  <i aria-hidden="true" className={`fa fa-${icons[type] || "question"}`} />
);

export default Icon;
