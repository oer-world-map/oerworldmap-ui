/* global _paq */
/* global confirm */
import React from "react";

import Link from "./Link";
import ShareExport from "./ShareExport";
import Metadata from "./Metadata";
import withEmitter from "./withEmitter";
import withI18n from "./withI18n";
import withUser from "./withUser";

import expose from "../expose";

import "../styles/components/WebPageHeader.pcss";

const WebPageHeader = ({ user, about, dateModified, emitter, translate, view, _self, _links }) => (
  <div className="WebPageHeader">
    <Metadata type={about["@type"]} about={about} dateModified={dateModified} _self={_self} />
    <div className="webPageActions print-display-none">
      <div>
        {about["@id"] && [
          view !== "edit" && (
            <div className="action" key="share">
              <ShareExport
                _self={_self}
                _links={_links}
                view={view}
                title={translate(about["name"])}
              />
            </div>
          ),
          expose("editEntry", user, about) && (
            <div className="action" key="view">
              {view === "edit" ? (
                <button
                  onClick={() => {
                    confirm(translate("WebPageView.leaveEditView")) &&
                      emitter.emit("navigate", _self || Link.home);
                  }}
                  className="btn icon"
                  title="Close"
                >
                  <i aria-hidden="true" className="fa fa-fw fa-xmark" />
                </button>
              ) : (
                <Link
                  href="#edit"
                  additional={() => {
                    typeof _paq !== "undefined" &&
                      _paq.push(["trackEvent", "EntryDetailOverlay", "ButtonClick", "Edit"]);
                  }}
                >
                  <i aria-hidden="true" className="fa fa-pencil" />
                </Link>
              )}
            </div>
          )
        ]}

        {(view !== "edit" || !about["@id"]) && (
          <div className="action">
            <button
              onClick={() => {
                emitter.emit(
                  "navigate",
                  Link.back && Link.back.includes("/feed/") ? Link.back : Link.home
                );
              }}
              className="btn icon"
              title="Close"
            >
              <i aria-hidden="true" className="fa fa-fw fa-xmark" />
            </button>
          </div>
        )}
      </div>
    </div>
  </div>
);

export default withEmitter(withI18n(withUser(WebPageHeader)));
