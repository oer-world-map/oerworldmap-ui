import React from "react";
import { ReactiveBase } from "@appbaseio/reactivesearch";
import I18nProvider from "./I18nProvider";
import EmittProvider from "./EmittProvider";
import ApiProvider from "./ApiProvider";
import UserProvider from "./UserProvider";
import ConfigProvider from "./ConfigProvider";
import FilterProvider from "./FilterProvider";
import App from "./App";

const Init = ({
  i18n,
  children,
  config,
  supportedLanguages,
  iso3166,
  region,
  emitter = {
    on: (event, handler) => console.log("Registered", event, handler),
    off: (event, handler) => console.log("Unregistered", event, handler),
    emit: (event, payload) => console.log("Triggered", event, payload)
  }
}) => (
  <ConfigProvider config={config}>
    <I18nProvider i18n={i18n}>
      <EmittProvider emitter={emitter}>
        <ApiProvider config={config.publicConfig}>
          <UserProvider>
            <FilterProvider translate={i18n.translate}>
              <ReactiveBase
                app={config.elasticsearchConfig.index}
                url={config.elasticsearchConfig.url}
                className="reactiveBase"
              >
                <App
                  locales={i18n.locales}
                  supportedLanguages={supportedLanguages}
                  iso3166={iso3166}
                  region={region}
                >
                  {children}
                </App>
              </ReactiveBase>
            </FilterProvider>
          </UserProvider>
        </ApiProvider>
      </EmittProvider>
    </I18nProvider>
  </ConfigProvider>
);

export default Init;
