import React from "react";

import "../styles/components/Feedback.pcss";

const Feedback = ({ children }) => <div className="Feedback">{children}</div>;

export default Feedback;
