import React from "react";

import Icon from "./Icon";

const labels = {
  Organization: "Organizations",
  Service: "Services",
  Action: "Projects",
  Person: "People",
  Event: "Events",
  Product: "Tools"
};

const Overview = ({ buckets }) =>
  buckets.map((bucket) => (
    <div className="col" key={bucket.key}>
      <a href={`/resource/?filter.about.@type=${bucket.key}`}>
        <Icon type={bucket.key} />
        <br />
        {labels[bucket.key]}
        <br />
        <span className="large">{bucket.doc_count}</span>
      </a>
    </div>
  ));

export default Overview;
