import React from "react";

import Link from "./Link";

import "../styles/components/PublicationProfiles.pcss";

const publicationProfiles = ({ title, links }) => (
  <ul className="ItemList">
    {links.map((link, index) => {
      const parsedUrl = new URL(link);
      let text = parsedUrl.host + parsedUrl.pathname;
      switch (parsedUrl.host) {
        case "orcid.org":
          text = parsedUrl.pathname.slice(1) || "ORCID";
          break;
        case "www.researchgate.net":
          text = parsedUrl.pathname.replace(/^\/profile\//, "") || "ResearchGate";
          break;
      }

      return (
        <li key={index}>
          <Link href={link}>{text}</Link>
        </li>
      );
    })}
  </ul>
);

export default publicationProfiles;
