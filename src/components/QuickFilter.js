import React, { useEffect } from "react";
import { ToggleButton } from "@appbaseio/reactivesearch";
import { quickSearchTypes } from "../common";
import withI18n from "./withI18n";

const QuickFilter = ({ translate }) => {
  useEffect(() => {
    const buttons = document.querySelectorAll(".typeButtons > a");
    for (let i = 0; i < buttons.length; i++) {
      if (buttons[i].textContent !== "") {
        buttons[i].title = buttons[i].textContent;
        buttons[i].textContent = "";
      }
    }
    return;
  });

  return (
    <ToggleButton
      componentId="filter.about.@type"
      URLParams
      className="typeButtons"
      dataField="about.@type"
      multiSelect={false}
      data={quickSearchTypes.map((type) => ({
        label: translate(type),
        value: type
      }))}
    />
  );
};

export default withI18n(QuickFilter);
