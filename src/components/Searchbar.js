/* global document */
/* global window */
import React, { useContext, useState, useEffect } from "react";

import { DataSearch, StateProvider } from "@appbaseio/reactivesearch";

import withI18n from "./withI18n";
import withEmitter from "./withEmitter";
import { FilterContext } from "../contexts";

import "../styles/components/Searchbar.pcss";

const Searchbar = ({ translate, iso3166 = "", region }) => {
  const { getUrlParams, sorts } = useContext(FilterContext);
  const [params, setParams] = useState(getUrlParams());

  const [isClient, setIsClient] = useState(false);

  useEffect(() => {
    setIsClient(true);
    return;
  }, []);

  if (isClient) {
    let subFilters = [
      {
        componentId: "filter.about.additionalType.@id",
        dataField: "about.additionalType.@id"
      },
      {
        componentId: "filter.about.primarySector.@id",
        dataField: "about.primarySector.@id"
      },
      {
        componentId: "filter.about.secondarySector.@id",
        dataField: "about.secondarySector.@id"
      },
      {
        componentId: "filter.about.keywords",
        dataField: "about.keywords",
        showMissing: true,
        translate: false,
        size: 9999
      },
      {
        componentId: "filter.about.award",
        dataField: "about.award"
      },
      {
        componentId: "filter.about.activityField.@id",
        dataField: "about.activityField.@id"
      },
      {
        componentId: "filter.about.focus",
        dataField: "about.focus"
      },
      {
        componentId: "filter.about.scope.@id",
        dataField: "about.scope.@id"
      },
      {
        componentId: "filter.about.audience.@id",
        dataField: "about.audience.@id"
      },
      {
        componentId: "filter.about.about.@id",
        dataField: "about.about.@id"
      },
      {
        componentId: "filter.about.license.@id",
        dataField: "about.license.@id"
      },
      {
        componentId: "filter.about.spatialCoverage",
        dataField: "about.spatialCoverage"
      },
      {
        componentId: "filter.about.inLanguage",
        dataField: "about.inLanguage"
      },
      {
        componentId: "filter.about.objectIn.@type",
        dataField: "about.objectIn.@type",
        hidden: true
      },
      {
        componentId: "filter.author.keyword",
        dataField: "author.keyword",
        hidden: true
      },
      {
        componentId: "filter.about.objectIn.agent.@id",
        dataField: "about.objectIn.agent.@id",
        hidden: true
      },
      {
        componentId: "filter.about.attendee.@id",
        dataField: "about.attendee.@id",
        hidden: true
      },
      {
        componentId: "filter.about.affiliate.@id",
        dataField: "about.affiliate.@id",
        hidden: true
      }
    ];

    if (
      !iso3166 &&
      !subFilters.find(
        (filter) =>
          filter.componentId === "filter.feature.properties.location.address.addressCountry"
      )
    ) {
      subFilters.splice(1, 0, {
        componentId: "filter.feature.properties.location.address.addressCountry",
        dataField: "feature.properties.location.address.addressCountry",
        title: "country"
      });
    }

    if (
      !region &&
      !subFilters.find(
        (filter) =>
          filter.componentId === "filter.feature.properties.location.address.addressRegion"
      )
    ) {
      subFilters.splice(2, 0, {
        componentId: "filter.feature.properties.location.address.addressRegion",
        dataField: "feature.properties.location.address.addressRegion",
        title: "filter.feature.properties.location.address.addressRegion"
      });
    }

    const filterIDs = ["q", "size", "filter.about.@type"].concat(
      subFilters.map((filter) => filter.componentId)
    );
    subFilters = subFilters.map((filter) => {
      filter.react = {
        and: filterIDs.filter((id) => id !== filter.componentId)
      };
      return filter;
    });

    return (
      <div id="reactive-searchbar">
        <StateProvider
          componentIds={["filter.about.@type"]}
          render={({ searchState }) => {
            const filter =
              (searchState &&
                searchState["filter.about.@type"] &&
                searchState["filter.about.@type"].value &&
                !Array.isArray(searchState["filter.about.@type"].value) &&
                searchState["filter.about.@type"].value) ||
              false;

            let searchPlaceholder = translate("search.entries");
            if (iso3166) {
              filter
                ? (searchPlaceholder = translate("search.entries.country.filter", {
                    country: translate(region ? `${iso3166}.${region}` : iso3166),
                    filter: translate(filter).toLowerCase()
                  }))
                : (searchPlaceholder = translate("search.entries.country", {
                    country: translate(region ? `${iso3166}.${region}` : iso3166)
                  }));
            } else if (filter) {
              if (filter === "Policy") {
                searchPlaceholder = translate("search.entries.filter.policy");
              } else {
                searchPlaceholder = translate("search.entries.filter", {
                  filter: translate(filter)
                });
              }
            }

            return (
              <DataSearch
                className="nameSearch"
                componentId="q"
                customQuery={(value) =>
                  value && {
                    query: {
                      query_string: {
                        query: value,
                        fields: [
                          "about.name.*^10",
                          "about.alternateName.*^8",
                          "about.hashtag^8",
                          "about.description^5",
                          "about.keywords^5",
                          "about.*.name.*^3"
                        ],
                        default_operator: "AND"
                      }
                    }
                  }
                }
                debounce={200}
                includeFields={[  // auto complete fields https://docs.reactivesearch.io/docs/reactivesearch/react/v3/search/datasearch/#includefields
                  "about.name.*",
                  "about.description.*",
                  "about.alternateName.*",
                  "about.keywords",
                  "about.hashtag",
                  "about.*.name.*"
                ]}
                placeholder={searchPlaceholder}
                onValueSelected={(value) => {
                  value &&
                    setParams({
                      ...getUrlParams(),
                      sort: sorts[1].dataField
                    });
                }}
                URLParams
                react={{
                  and: filterIDs.filter((id) => id !== "q")
                }}
              />
            );
          }}
        />
      </div>
    );
  }
  return null;
};

export default withEmitter(withI18n(Searchbar));
