/* global window */

import React from "react";
import PropTypes from "prop-types";

import { isNode } from "../common";
import { UserContext } from "../contexts";

class UserProvider extends React.Component {
  render() {
    const { children } = this.props;
    const user = isNode() ? undefined : window.__APP_USER__;
    return (
      <UserContext.Provider value={user}>{React.Children.only(children)}</UserContext.Provider>
    );
  }
}

UserProvider.propTypes = {
  children: PropTypes.node.isRequired
};

export default UserProvider;
