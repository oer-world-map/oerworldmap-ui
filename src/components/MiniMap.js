/* global window */
/* global Event */
/* global document */
/* global MutationObserver */
/* global requestAnimationFrame */
/* global cancelAnimationFrame */

import React from "react";
import PropTypes from "prop-types";
import withConfig from "./withConfig";
import { emptyGeometry } from "../common";

import "leaflet/dist/leaflet.css";

class MiniMap extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      zoom: props.zoom
    };

    this.mouseDown = this.mouseDown.bind(this);
    this.mouseMove = this.mouseMove.bind(this);
    this.mouseUp = this.mouseUp.bind(this);
    this.mouseEnter = this.mouseEnter.bind(this);
    this.mouseLeave = this.mouseLeave.bind(this);
    this.updateMap = this.updateMap.bind(this);
    this.animateMarker = this.animateMarker.bind(this);
  }

  componentDidMount() {
    const mo = new MutationObserver((e) => {
      const mutation = e.shift();
      if (
        mutation &&
        mutation.attributeName === "class" &&
        !mutation.target.classList.contains("hidden")
      ) {
        window.dispatchEvent(new Event("resize"));
        mo.disconnect();
      }
    });

    document &&
      document.getElementById("edit") &&
      mo.observe(document.getElementById("edit"), { attributes: true });

    const { geometry, boxZoom, draggable, isLiveEvent } = this.props;
    this.L = require("leaflet");

    this.MiniMap = this.L.map("mini-map", {
      attributionControl: false
    }).setView([51.505, -0.09], 2);
    this.L.tileLayer("https://tile.openstreetmap.org/{z}/{x}/{y}.png", {
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
      center: [0, 0],
      maxZoom: 19,
      minZoom: 2,
      zoomControl: draggable
    }).addTo(this.MiniMap);

    this.isDragging = false;

    // Set circle layers properties
    this.initialRadius = window.innerWidth <= 700 ? 10 : 7;
    this.radius = this.initialRadius;
    this.framesPerSecond = 15;
    this.initialOpacity = 0.9;
    this.opacity = this.initialOpacity;
    this.maxRadius = this.initialRadius * 20;
    this.animatingMarkers;
    this.start = null;

    this.MiniMap.on("mouseout", () => {
      this.selected = null;
      this.isDragging = false;
      this.MiniMap.dragging.enable();
    });

    var geojsonMarkerOptions = {
      radius: this.initialRadius,
      fillColor: "#051d2e",
      color: "#fff",
      weight: 1,
      opacity: 1,
      fillOpacity: this.initialOpacity
    };

    const self = this;
    this.pointsGeojson = this.L.geoJSON(geometry || emptyGeometry, {
      pointToLayer: function (feature, latlng) {
        const marker = self.L.circleMarker(latlng, geojsonMarkerOptions);
        marker.on("mousedown", (e) => self.mouseDown(e));
        return marker;
      }
    });
    this.pointsGeojson.addTo(this.MiniMap);

    this.eventsGeojson = this.L.geoJSON(isLiveEvent ? geometry : emptyGeometry, {
      pointToLayer: function (feature, latlng) {
        const marker = self.L.circleMarker(latlng, geojsonMarkerOptions);
        return marker;
      }
    });
    this.pointsGeojson.addTo(this.MiniMap);

    this.updateMap(this.props);
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps != this.props) {
      this.updateMap(nextProps);
    }
  }

  shouldComponentUpdate() {
    return false;
  }

  animateMarker(timestamp) {
    if (!this.start) this.start = timestamp;
    const progress = timestamp - this.start;

    if (progress > 1000 / this.framesPerSecond) {
      this.radius += (this.maxRadius - this.radius) / (1000 / this.framesPerSecond);
      this.opacity -= 0.9 / this.framesPerSecond;

      //this.MiniMap.setPaintProperty('EventsGlow', 'circle-radius', this.radius)
      //this.MiniMap.setPaintProperty('EventsGlow', 'circle-opacity', this.opacity)

      if (this.opacity <= 0.1) {
        this.radius = this.initialRadius;
        this.opacity = this.initialOpacity;
      }
      this.start = null;
    }

    this.animatingMarkers = requestAnimationFrame(this.animateMarker);
  }

  mouseMove(e) {
    if (!this.isDragging) return;
    const coords = e.latlng;

    if (this.selected) {
      const data = this.selected.feature.geometry;

      if (data.type === "Point") {
        data.coordinates = [coords.lng, coords.lat];
      }
      this.pointsGeojson.removeLayer(this.selected._leaflet_id);
      this.pointsGeojson.addData(data);
      const layers = this.pointsGeojson.getLayers();
      this.selected = layers[layers.length - 1];
    }
  }

  mouseUp(e) {
    const { onFeatureDrag } = this.props;

    this.selected = null;
    if (!this.isDragging) {
      return;
    }

    this.isDragging = false;
    this.MiniMap.dragging.enable();

    this.setState({ zoom: this.MiniMap.getZoom() });

    onFeatureDrag &&
      onFeatureDrag({
        type: "Point",
        coordinates: e.latlng
      });
  }

  mouseDown(e) {
    this.selected = e.target;
    this.isDragging = true;
    this.MiniMap.dragging.disable();
  }

  mouseEnter() {
    this.pointsGeojson.setStyle((feature) => {
      return {
        color: "#3bb2d0"
      };
    });
    this.MiniMap.dragging.disable();
  }

  mouseLeave() {
    this.pointsGeojson.setStyle((feature) => {
      return {
        color: "#3887be"
      };
    });
    this.MiniMap.dragging.enable();
  }

  updateMap(props) {
    const { geometry, draggable, zoomable, center, isLiveEvent } = props;
    const { zoom } = this.state;

    this.pointsGeojson.clearLayers();
    this.pointsGeojson.addData(geometry || emptyGeometry);

    this.eventsGeojson.clearLayers();
    if (isLiveEvent) {
      this.eventsGeojson.addData(geometry);
      this.animatingMarkers = requestAnimationFrame(this.animateMarker);
    } else {
      this.eventsGeojson.addData(emptyGeometry);
      cancelAnimationFrame(this.animatingMarkers);
    }

    setTimeout(() => {
      if (center || geometry) {
        const latLng = center
          ? this.L.latLng(center[1], center[0])
          : this.L.latLng(geometry.coordinates[1], geometry.coordinates[0]);
        const leafletBounds = this.L.latLngBounds(latLng, latLng);

        this.MiniMap.fitBounds(leafletBounds, {
          padding: this.L.point(20, 20),
          maxZoom: zoom || 1
        });
      } else {
        const latlng = L.latLng(0, 0);
        this.MiniMap.flyTo(latlng, {
          zoom: zoom || 1
        });
      }
    }, 0);

    this.MiniMap.off("mouseenter", this.mouseEnter, this.pointsGeojson);
    this.MiniMap.off("mouseout", this.mouseLeave, this.pointsGeojson);
    //this.MiniMap.off('mousedown', this.mouseDown, this.pointsGeojson)
    this.MiniMap.off("mousemove", this.mouseMove);
    this.MiniMap.off("mouseup", this.mouseUp);

    if (draggable) {
      this.MiniMap.on("mouseenter", this.mouseEnter, this.pointsGeojson);
      this.MiniMap.on("mouseout", this.mouseLeave, this.pointsGeojson);
      //this.MiniMap.on('mousedown', this.mouseDown, this.pointsGeojson)
      this.MiniMap.on("mousemove", this.mouseMove);
      this.MiniMap.on("mouseup", this.mouseUp);
    }

    if (zoomable) {
      this.MiniMap.scrollWheelZoom.enable();
      this.MiniMap.doubleClickZoom.enable();
    } else {
      this.MiniMap.scrollWheelZoom.disable();
      this.MiniMap.doubleClickZoom.disable();
    }
  }

  render() {
    return (
      <div
        ref={(map) => {
          this.MiniMap = map;
        }}
        id="mini-map"
        className="MiniMap"
        style={{
          position: "absolute",
          width: "100%",
          height: "100%",
          top: 0,
          left: 0
        }}
      />
    );
  }
}

MiniMap.propTypes = {
  config: PropTypes.objectOf(PropTypes.any).isRequired,
  center: PropTypes.arrayOf(PropTypes.any), // eslint-disable-line react/no-unused-prop-types
  geometry: PropTypes.objectOf(PropTypes.any),
  draggable: PropTypes.bool,
  zoomable: PropTypes.bool, // eslint-disable-line react/no-unused-prop-types
  onFeatureDrag: PropTypes.func,
  boxZoom: PropTypes.bool,
  zoom: PropTypes.number,
  isLiveEvent: PropTypes.bool
};

MiniMap.defaultProps = {
  center: undefined,
  geometry: undefined,
  draggable: false,
  zoomable: false,
  onFeatureDrag: null,
  boxZoom: false,
  zoom: null,
  isLiveEvent: undefined
};

export default withConfig(MiniMap);
