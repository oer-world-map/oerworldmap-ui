/* global document */
/* global window */
/* global navigator */
/* global requestAnimationFrame */
/* global cancelAnimationFrame */
/* global localStorage */

import React from "react";
import PropTypes from "prop-types";
import { createRoot } from "react-dom/client";

import { geoContains, scaleLog, quantile, interpolateHcl, color as d3color, formatHex } from "d3";
import bbox from "@turf/bbox";
import centerOfMass from "@turf/center-of-mass";
import PubSub from "pubsub-js";

import "leaflet/dist/leaflet.css";
import * as countriesGeojsonFile from "../geojson/countries.geojson";
import * as regionsGeojsonFile from "../geojson/regions.geojson";

import centroids from "../json/centroids.json";

import Icon from "./Icon";
import Link from "./Link";
import withI18n from "./withI18n";
import withConfig from "./withConfig";
import EmittProvider from "./EmittProvider";
import bounds from "../json/bounds.json";
import ResourcePreview from "./ResourcePreview";
import I18nProvider from "./I18nProvider";
import i18n from "../i18n";
import MapLeyend from "./MapLeyend";
import TogglePoints from "./TogglePoints";
import QuickFilter from "./QuickFilter";

import "../styles/components/ReactiveMap.pcss";

const timeout = async (ms) => new Promise((resolve) => setTimeout(resolve, ms));

const unselectedCountryFillColor = "#b7d8f2";
const unselectedCountryFillOpacity = 0.8;
const initialLatLng = [0, 0];
const initialZoom = 2;
const date = new Date().toJSON().split("T").shift();

let resizeTimer;

const calculateTypes = (features) => {
  const types = [];
  features.forEach((feature) => {
    if (types[feature.properties["@type"]]) {
      types[feature.properties["@type"]] += 1;
    } else {
      types[feature.properties["@type"]] = 1;
    }
  });
  return Object.keys(types).map((key) => (
    <span className="item" key={key}>
      <Icon type={key} />
      &nbsp;
      {types[key]}
    </span>
  ));
};

const renderTypes = (types) =>
  types.map((type) => (
    <div key={type.key}>
      <Icon type={type.key} />
      <span>{type.doc_count}</span>
    </div>
  ));

class Map extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      aggregations: {}
    };

    this.updatePoints = this.updatePoints.bind(this);
    this.updateActiveCountry = this.updateActiveCountry.bind(this);
    this.mouseMovePoints = this.mouseMovePoints.bind(this);
    this.mouseMove = this.mouseMove.bind(this);
    // this.moveEnd = this.moveEnd.bind(this)
    this.mouseOut = this.mouseOut.bind(this);
    this.clickPoints = this.clickPoints.bind(this);
    this.clickCountries = this.clickCountries.bind(this);
    this.clickRegions = this.clickRegions.bind(this);
    this.choroplethStopsFromBuckets = this.choroplethStopsFromBuckets.bind(this);
    this.setPinSize = this.setPinSize.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.animateMarker = this.animateMarker.bind(this);
    this.setMapData = this.setMapData.bind(this);
    this.selectCountry = this.selectCountry.bind(this);
    this.addFakeGeometry = this.addFakeGeometry.bind(this);
    this.isReady = false;
    this.data = {};
  }

  componentDidMount() {
    const { iso3166, emitter, region } = this.props;

    this.eventsDataToken = PubSub.subscribe("mapData", this.setMapData);

    this.L = require("leaflet");

    this.map = this.L.map("map", {
      maxZoom: 19,
      minZoom: 2,
      zoomControl: false,
      attributionControl: false
    }).setView(initialLatLng, initialZoom);
    this.map.setMaxBounds([
      [-90, -240],
      [90, 240]
    ]);

    this.map.on("click", (event) => {
      this.handleClick(event);
    });

    this.map.on("mousemove", this.mouseMove);
    this.map.getContainer().addEventListener("mouseleave", this.mouseOut);

    this.tooltip = this.L.tooltip({
      className: "tooltip",
      opacity: 1,
      permanent: true
    });

    this.map.on("mouseout", () => {
      this.tooltip.close();
    });

    this.addCountries();

    this.regionLayerGroup = this.L.layerGroup().addTo(this.map);
    this.hoveredPoints = [];

    this.initialRadius = window.innerWidth <= 700 ? 10 : 5;
    this.radius = this.initialRadius;
    this.framesPerSecond = 15;
    this.initialOpacity = 0.9;
    this.opacity = this.initialOpacity;
    this.maxRadius = this.initialRadius * 20;
    this.animatingMarkers = null;
    this.start = null;

    // Initialize choropleth layers
    if (iso3166 && region) {
      this.addRegions(iso3166);
    }
    this.updateActiveCountry(iso3166, region);
    this.L.control
      .zoom({
        position: "bottomleft"
      })
      .addTo(this.map);

    window.addEventListener("resize", () => {
      clearTimeout(resizeTimer);
      resizeTimer = setTimeout(this.setPinSize, 250);
    });

    // Update URL values
    // this.map.on('moveend', this.moveEnd)

    this.map.on("click", this.handleClick);

    this.isReady = true;
  }

  componentDidUpdate(nextProps) {
    this.map.invalidateSize(false);
    const { iso3166, region } = this.props;

    if (iso3166 !== nextProps.iso3166 || region !== nextProps.region) {
      this.updateActiveCountry(iso3166, region);
    }
  }

  componentWillUnmount() {
    const { emitter } = this.props;
    this.map.off("mousemove", this.mouseMove);
    // this.map.off('moveend', this.moveEnd)
    this.map.getContainer().removeEventListener("mouseleave", this.mouseOut);
    this.map.off("click", this.handleClick);
    PubSub.unsubscribe(this.eventsDataToken);
  }

  async setMapData(_, data) {
    if (this.isReady && data) {
      data.features = this.addFakeGeometry(data.features);
      this.updateChoropleth(data.aggregations);
      this.updatePoints(data.features);
      this.setState(data);
    } else {
      await timeout(10);
      this.setMapData(data);
    }
  }

  addRegions(countryCode) {
    this.regionsGeojson = this.L.geoJSON(regionsGeojsonFile, {
      style: {
        color: "#222222",
        opacity: 0.7,
        weight: 1,
        fillColor: "#349900",
        fillOpacity: 0
      },
      filter: (feature) => {
        if (!countryCode) return false;
        const country = feature.properties.iso_3166_2.split("-")[0];
        return country === countryCode;
      }
    });
    this.removeRegions();
    this.regionsGeojson.addTo(this.regionLayerGroup);
    this.regionsGeojson.on("click", (event) => {
      this.L.DomEvent.stopPropagation(event);
      this.onClickedRegion(event);
    });

    this.regionsGeojson.on("mousemove", (event) => {
      const [country, region] = event.sourceTarget.feature.properties["code_hasc"].split(".");
      if (country && country !== -99) {
        this.hoveredCountry = country;
      } else {
        this.hoveredCountry = undefined;
      }
      this.hoveredRegion = region;
    });

    this.regionsGeojson.on("mouseout", () => {
      this.hoveredCountry = undefined;
      this.hoveredRegion = undefined;
    });
  }

  removeRegions() {
    this.regionLayerGroup.clearLayers();
  }

  addCountries() {
    this.countriesGeojson = this.L.geoJSON(countriesGeojsonFile, {
      style: {
        color: "#222222",
        weight: 1,
        fillColor: unselectedCountryFillColor,
        fillOpacity: unselectedCountryFillOpacity
      }
    });

    this.countriesGeojson.addTo(this.map);
    this.countriesGeojson.on("click", (event) => {
      this.onClickedCountry(event);
      this.L.DomEvent.stopPropagation(event);
    });

    this.countriesGeojson.on("mousemove", (event) => {
      const country = event.sourceTarget.feature.properties["ISO_A2"];
      if (country && country !== -99) {
        this.hoveredCountry = country;
      } else {
        this.hoveredCountry = undefined;
      }
      this.hoveredRegion = undefined;
    });

    this.countriesGeojson.on("mouseout", () => {
      this.hoveredCountry = undefined;
    });
  }

  // Ensure that all features have geometry and are shown on the map so users
  // can access all data from the map.
  addFakeGeometry(features) {
    const { iso3166, region } = this.props;
    const newFeatures = [];
    features.forEach((f) => {
      const feature = structuredClone(f);
      if (!feature.geometry) {
        let fakeCenter;
        if (!region) {
          const country = feature.properties.location?.address?.addressCountry ?? iso3166;
          fakeCenter = centroids[country];
        } else {
          const regionLayer = this.getRegionLayer(region);
          const centerOfRegion = centerOfMass(regionLayer.feature);
          fakeCenter = centerOfRegion.geometry.coordinates;
        }

        if (fakeCenter) {
          feature.geometry = {
            coordinates: [fakeCenter[0], fakeCenter[1]],
            type: "Point"
          };
          feature.properties.location.geo = {
            lon: fakeCenter[0],
            lat: fakeCenter[1]
          };
        }
      }
      newFeatures.push(feature);
    });
    return newFeatures;
  }

  onClickedRegion(event) {
    if (this.hoveredPoints && this.hoveredPoints.length > 0) {
      this.clickPoints(event);
    } else {
      const [country, region] = event.propagatedFrom.feature.properties["code_hasc"].split(".");
      if (country && region) {
        const { emitter } = this.props;
        emitter.emit("navigate", `/country/${country}/${region}${window.location.search}`);
      }
    }
  }

  selectRegion(country, region) {
    if (country && region) {
      this.regionsGeojson.setStyle((feature) => {
        const featureRegion = feature.properties["code_hasc"].split(".")[1];
        if (featureRegion === region) {
          return { weight: 2 };
        } else {
          return { weight: 1 };
        }
      });
    }
    this.flyToRegion(region);
  }

  flyToRegion(region) {
    const regionLayer = this.getRegionLayer(region);
    const boundingBox = bbox(regionLayer.feature);
    const corner1 = this.L.latLng(boundingBox[1], boundingBox[0]);
    const corner2 = this.L.latLng(boundingBox[3], boundingBox[2]);
    const regionBounds = this.L.latLngBounds(corner1, corner2);
    this.map.flyToBounds(regionBounds);
  }

  onClickedCountry(event) {
    if (this.hoveredPoints && this.hoveredPoints.length > 0) {
      this.clickPoints(event);
    } else {
      const countryCode = event.propagatedFrom.feature.properties["ISO_A2"];
      if (countryCode) {
        const { emitter } = this.props;
        emitter.emit("navigate", `/country/${countryCode.toLowerCase()}${window.location.search}`);
      }
    }
  }

  selectCountry(countryCode) {
    this.countriesGeojson.setStyle((feature) => {
      if (feature.properties["ISO_A2"] !== -99 && feature.properties["ISO_A2"] === countryCode) {
        return { fillOpacity: 1 };
      } else {
        return { fillOpacity: unselectedCountryFillOpacity };
      }
    });
    this.addRegions(countryCode);
    this.flyToCountry(countryCode);
  }

  flyToCountry(countryCode) {
    if (countryCode in bounds) {
      const latLng = this.L.latLng(bounds[countryCode][1], bounds[countryCode][0]);
      this.map.flyTo(latLng, bounds[countryCode][2]);
    } else if (countryCode in centroids) {
      const latLng = this.L.latLng(centroids[countryCode][1], centroids[countryCode][0]);
      this.map.flyTo(latLng, 4.8);
    } else {
      const layers = this.countriesGeojson._layers;
      const keys = Object.keys(layers);
      let layer;
      keys.forEach((key) => {
        if (layers[key].feature.properties.ISO_A2 === countryCode) {
          layer = layers[key];
        }
      });
      if (layer) {
        const latLng = this.L.latLng(
          (layer._bounds._northEast.lat + layer._bounds._southWest.lat) / 2,
          (layer._bounds._northEast.lng + layer._bounds._southWest.lng) / 2
        );
        this.map.flyTo(latLng, 4);
      }
    }
  }

  getBucket(location, aggregation) {
    const { aggregations } = this.state;

    return (
      (aggregations &&
        aggregations[aggregation] &&
        aggregations[aggregation].buckets.find((agg) => agg.key === location)) ||
      null
    );
  }

  setPinSize() {
    this.initialRadius = window.innerWidth <= 700 ? 10 : 5;
    this.maxRadius = this.initialRadius * 20;
  }

  animateMarker(timestamp) {
    if (!this.start) this.start = timestamp;
    const progress = timestamp - this.start;

    if (progress > 1000 / this.framesPerSecond) {
      this.radius += (this.maxRadius - this.radius) / (1000 / this.framesPerSecond);
      this.opacity -= 0.9 / this.framesPerSecond;

      if (this.opacity <= 0.1) {
        this.radius = this.initialRadius;
        this.opacity = this.initialOpacity;
      }
      this.start = null;
    }

    this.animatingMarkers = requestAnimationFrame(this.animateMarker);
  }

  mouseMovePoints(e) {
    const ids = e.features.map((feat) => feat.properties["@id"]);
    this.map.setFilter("points-hover", ["in", "@id"].concat(ids));
    if (ids.length) {
      this.map.getCanvas().style.cursor = "pointer";
    }
  }

  /**
   * Calculates the distance in pixels between a container point and a geographical point.
   * @param {*} geo geo object inside a feature object
   * @param {*} containerPoint (see for example https://leafletjs.com/reference.html#mouseevent-containerpoint)
   * @returns The distance in pixels
   */
  calculateDistance(geo, containerPoint) {
    const latlng = this.L.latLng(geo.lat, geo.lon);
    const secondContainerPoint = this.map.latLngToContainerPoint(latlng);
    return Math.sqrt(
      Math.pow(containerPoint.x - secondContainerPoint.x, 2) +
        Math.pow(containerPoint.y - secondContainerPoint.y, 2)
    );
  }

  getGeoObjects(location) {
    const geoObjects = [];
    if (!location.length && location.geo) {
      geoObjects.push(location.geo);
    } else if (location.length) {
      location.forEach((element) => {
        geoObjects.push(...this.getGeoObjects(element));
      });
    }
    return geoObjects;
  }

  updateHoveredPoints(event) {
    if (!this.pointsGeojson) return [];
    this.hoveredPointLayers = [];
    this.pointsGeojson.eachLayer((layer) => {
      const distances = [];
      const geoObjects = this.getGeoObjects(layer.feature.properties.location);
      geoObjects.forEach((geo) => {
        distances.push(this.calculateDistance(geo, event.containerPoint));
      });

      if (distances.length === 0) return [];

      if (Math.min(distances) <= 10) {
        this.hoveredPointLayers.push(layer);
      }
    });
    this.hoveredPoints = [];
    this.hoveredPointLayers.forEach((layer) => {
      this.hoveredPoints.push(layer.feature);
    });
    return this.hoveredPoints;
  }

  mouseMove(event) {
    if (this.popup?.isOpen()) return;
    const { overlayList } = this.state;

    if (!overlayList) {
      const { translate, iso3166, phrases, locales, emitter, region } = this.props;
      const { aggregations } = this.state;
      const hoveredPoints = this.updateHoveredPoints(event);
      const hasc = `${this.hoveredCountry}.${this.hoveredRegion}`;
      if (!this.hoveredCountry && !hoveredPoints.length) {
        // Water since there is no country
        this.tooltip.close();
      } else {
        let popupContent;
        if (hoveredPoints.length > 0) {
          if (hoveredPoints.length > 6) {
            popupContent = (
              <ul>
                <li
                  style={{
                    display: "flex",
                    justifyContent: "space-evenly",
                    fontSize: "var(--font-size-xs)"
                  }}
                >
                  {calculateTypes(hoveredPoints)}
                </li>
              </ul>
            );
            // More than 6 points
          } else if (hoveredPoints.length > 1 && hoveredPoints.length <= 6) {
            // More than 1 point, less than 6
            popupContent = (
              <ul className="list">
                {hoveredPoints.map((point) => (
                  <li key={point.properties["@id"]}>
                    <Icon type={point.properties["@type"]} />
                    <span>{translate(point.properties.name)}</span>
                  </li>
                ))}
              </ul>
            );
          } else {
            // Single point
            popupContent = (
              <ul className="list">
                <li>
                  <I18nProvider i18n={i18n(locales, phrases)}>
                    <EmittProvider emitter={emitter}>
                      <ResourcePreview
                        about={Object.assign(hoveredPoints[0].properties, {
                          name: hoveredPoints[0].properties.name,
                          location: [hoveredPoints[0].properties.location],
                          additionalType: hoveredPoints[0].properties.additionalType,
                          alternateName: hoveredPoints[0].properties.alternateName
                        })}
                      />
                    </EmittProvider>
                  </I18nProvider>
                </li>
              </ul>
            );
          }
        } else if (this.hoveredRegion) {
          // Hover a region
          const bucket = this.getBucket(
            this.hoveredRegion,
            "sterms#feature.properties.location.address.addressRegion"
          );
          popupContent = (
            <ul>
              <li>
                <span className="tooltipTitle">
                  {translate(hasc)}
                  &nbsp;(
                  {translate(this.hoveredCountry)})
                </span>
                {bucket && !region && (
                  <>
                    <div className="buckets">{renderTypes(bucket["sterms#by_type"].buckets)}</div>
                  </>
                )}
              </li>
            </ul>
          );
        } else if (iso3166 && this.hoveredCountry !== iso3166) {
          // Not the country that is selected
          popupContent = (
            <ul>
              <li>
                <span className="tooltipTitle">{translate(this.hoveredCountry)}</span>
              </li>
            </ul>
          );
        } else {
          // Hover a country
          const bucket = this.getBucket(
            this.hoveredCountry,
            "sterms#feature.properties.location.address.addressCountry"
          );
          popupContent = (
            <ul>
              <li>
                <span className="tooltipTitle">{translate(this.hoveredCountry)}</span>
                {bucket && (
                  <>
                    <br />
                    <div className="buckets">{renderTypes(bucket["sterms#by_type"].buckets)}</div>
                  </>
                )}
              </li>
            </ul>
          );
        }
        const div = document.createElement("div");
        const root = createRoot(div);
        root.render(
          <div
            className="tooltip noEvents"
            style={{
              zIndex: 9,
              pointerEvents: "none"
            }}
          >
            {popupContent}
          </div>
        );
        this.tooltip.setLatLng(event.latlng);
        // The delay by setTimeout() - even with a delay of 0 - is enough so the HTML
        // is ready to be shown. Necessary to prevent an annoying flickering effect.
        setTimeout(() => {
          if (
            !this.tooltip.getContent() ||
            this.tooltip.getContent().innerText?.replaceAll("\n", "") !== div.innerText
          ) {
            this.tooltip.setContent(div);
          }
          if (!this.tooltip.isOpen()) {
            this.tooltip.openOn(this.map);
          }
        }, 0);
      }
    }
  }

  moveEnd(e) {
    const { iso3166 } = this.props;

    if (!iso3166) {
      const { lng, lat } = e.target.getCenter();
      const zoom = e.target.getZoom();

      const url = new URL(window.location.href);
      url.searchParams.set("map", `${lng.toFixed(5)},${lat.toFixed(5)},${Math.floor(zoom)}`);
      window.history.replaceState(null, null, url.href);
    }
  }

  mouseOut(e) {
    if (this.tooltip) {
      this.tooltip.close();
    }
  }

  updateActiveCountry(iso3166, region) {
    if (iso3166) {
      if (region) {
        this.flyToCountry(iso3166);
        this.selectRegion(iso3166, region);
      } else {
        this.selectCountry(iso3166);
      }
    } else {
      this.map.setView(initialLatLng, initialZoom);
      this.removeRegions();
    }
    const { setCollapsed } = this.props;
    setCollapsed(false);
  }

  choroplethStopsFromBuckets(buckets) {
    const counts = buckets.map((bucket) => bucket.doc_count);
    const range = ["#8cc857", "#fcffa4"];

    const getColor = (count) => {
      const rgb = scaleLog()
        .range([range[range.length - 1], range[0]])
        .interpolate(interpolateHcl)
        .domain([quantile(counts, 0.01), quantile(counts, 0.99)]);
      return d3color(rgb(count)).formatHex();
    };

    return buckets.length
      ? buckets.map((bucket) => [bucket.key, getColor(bucket.doc_count)])
      : [["", "#fff"]];
  }

  updateChoropleth(aggregations) {
    const { emitter } = this.props;
    if (aggregations) {
      const aggregation =
        aggregations["sterms#feature.properties.location.address.addressRegion"] ||
        aggregations["sterms#feature.properties.location.address.addressCountry"];
      const stops = this.choroplethStopsFromBuckets(aggregation.buckets);
      const colors = stops
        .map((stop) => stop[1])
        .filter((value, index, self) => self.indexOf(value) === index)
        .concat("#fff")
        .reverse();
      const layer = aggregations["sterms#feature.properties.location.address.addressRegion"]
        ? "Regions"
        : "countries";

      const colorsObject = {};
      stops.forEach((stop) => {
        colorsObject[stop[0]] = stop[1];
      });

      if (layer === "countries") {
        this.countriesGeojson.setStyle((feature) => {
          if (feature.properties["ISO_A2"] !== -99 && colorsObject[feature.properties["ISO_A2"]]) {
            return {
              fillColor: colorsObject[feature.properties["ISO_A2"]],
              fillOpacity: 1
            };
          } else {
            return {
              fillColor: "#FFFFFF",
              fillOpacity: 1
            };
          }
        });
      } else {
        this.countriesGeojson.setStyle({
          fillColor: unselectedCountryFillColor,
          fillOpacity: unselectedCountryFillOpacity
        });
        this.regionsGeojson.setStyle((feature) => {
          if (
            feature.properties["code_hasc"].split(".")[1] !== -99 &&
            colorsObject[feature.properties["code_hasc"]]
          ) {
            return {
              fillColor: colorsObject[feature.properties["code_hasc"]],
              fillOpacity: 1
            };
          } else {
            return {
              fillColor: "#FFFFFF",
              fillOpacity: 1
            };
          }
        });
      }

      const max = (aggregation.buckets.length && aggregation.buckets[0].doc_count) || 0;
      emitter.emit("updateColors", { colors, max });
    }
  }

  clickPoints(e) {
    const features = this.updateHoveredPoints(e);
    const { translate, emitter } = this.props;
    if (features.length > 6 && this.map.getZoom() !== this.map.getMaxZoom()) {
      this.map.flyTo(e.latlng, this.map.getZoom() + 5);
    } else if (features.length > 1) {
      const list = features.map((feature) => (
        <li key={feature.properties["@id"]}>
          <Link href={feature.properties["@id"]}>
            <Icon type={feature.properties["@type"]} />
            <span>{translate(feature.properties.name)}</span>
          </Link>
        </li>
      ));

      // Show overlay
      const popupDOM = document.createElement("div");
      popupDOM.classList.add("tooltip-container");
      const root = createRoot(popupDOM);
      root.render(
        <EmittProvider emitter={emitter}>
          <div
            className="tooltip"
            style={{
              zIndex: 9,
              pointerEvents: "all"
            }}
          >
            <ul className="list">{list}</ul>
          </div>
        </EmittProvider>
      );

      if (this.popup) {
        if (this.popup.isOpen()) {
          this.popup.close();
        }
      } else {
        this.popup = new this.L.popup({
          opacity: 1,
          permanent: true
        });
      }

      this.popup.on("remove", () => {
        this.setState({ overlayList: false });
      });

      this.popup.setLatLng(e.latlng);
      // The delay by setTimeout() - even with a delay of 0 - is enough so the HTML
      // is ready to be shown. Necessary to prevent a wrong placed popup.
      setTimeout(() => {
        if (
          !this.popup.getContent() ||
          this.popup.getContent().innerText?.replaceAll("\n", "") !== popupDOM.innerText
        ) {
          this.popup.setContent(popupDOM);
        }
        if (!this.tooltip.isOpen()) {
          this.popup.openOn(this.map);
        }
      }, 0);

      this.tooltip.close();

      this.setState({
        overlayList: true
      });
    } else {
      // Click on a single resource
      const url = `/resource/${features[0].properties["@id"]}`;
      emitter.emit("navigate", url);
    }
  }

  clickCountries(e, features) {
    if (this.tooltip && this.tooltip.isOpen()) return;

    const { emitter } = this.props;

    this.removeRegions();

    if (features[0].properties.iso_3166_1 !== "-99") {
      emitter.emit(
        "navigate",
        `/country/${features[0].properties.iso_3166_1.toLowerCase()}${window.location.search}`
      );
    }
  }

  clickRegions(e, features) {
    if (this.tooltip && this.tooltip.isOpen()) return;

    const { emitter } = this.props;
    const [country, region] = features[0].properties["HASC_1"].toLowerCase().split(".");
    if (features[0].properties.iso_3166_1 !== "-99") {
      emitter.emit("navigate", `/country/${country}/${region}${window.location.search}`);
    }
  }

  /**
   * @param {String} region: Code like "RZ"
   * @returns a Leaflet layer for the given region
   */
  getRegionLayer(region) {
    return this.regionsGeojson.getLayers().filter((layer) => {
      return layer.feature.properties.code_hasc.split(".")[1] === region;
    })[0];
  }

  updatePoints(features) {
    if (this.pointsGeojson) {
      this.pointsGeojson.remove();
    }
    if (!features || (features.length !== undefined && features.length === 0)) {
      return;
    }

    const { iso3166, region } = this.props;
    let selectedLayer;
    if (region) {
      selectedLayer = this.getRegionLayer(region);
    } else {
      selectedLayer = this.countriesGeojson.getLayers().filter((layer) => {
        return layer.feature.properties.ISO_A2 === iso3166;
      })[0];
    }

    if (selectedLayer) {
      const clonedFeature = structuredClone(selectedLayer.feature);
      // Need to reverse the order because Leaflet and d3 use different orders:
      // https://stackoverflow.com/questions/59864895/point-in-polygon-using-leaflet-pip-and-d3-geocontains
      if (clonedFeature.geometry.coordinates.length === 1) {
        clonedFeature.geometry.coordinates[0] = clonedFeature.geometry.coordinates[0].toReversed();
      } else if (clonedFeature.geometry.coordinates.length > 1) {
        for (let i = 0; i < clonedFeature.geometry.coordinates.length; i++) {
          for (let j = 0; j < clonedFeature.geometry.coordinates[i].length; j++) {
            clonedFeature.geometry.coordinates[i][j] =
              clonedFeature.geometry.coordinates[i][j].toReversed();
          }
        }
      }
      const selectedGeojson = this.L.geoJSON({
        type: "FeatureCollection",
        features: [clonedFeature]
      });
      for (let i = 0; i < features.length; i++) {
        // We have to remove points of MultiPoint-features that are outside of the selected
        // country or region to prevent confusion of the users.
        if (features[i].geometry?.type === "MultiPoint") {
          if (features[i].geometry.coordinates) {
            features[i].geometry.coordinates = features[i].geometry.coordinates.filter((c) => {
              return geoContains(selectedGeojson.toGeoJSON(), c);
            });
          }
          if (features[i].properties?.location) {
            features[i].properties.location = features[i].properties.location.filter((l) => {
              return l.geo && geoContains(selectedGeojson.toGeoJSON(), [l.geo.lon, l.geo.lat]);
            });
          }
        }
      }
    }

    const pointsCollection = {
      type: "FeatureCollection",
      features
    };
    this.tooltip && this.tooltip.close();
    const self = this;

    var eventMarkerOptions = {
      radius: this.initialRadius,
      fillColor: "#051d2e",
      color: "#fff",
      weight: 1,
      opacity: 1,
      fillOpacity: this.initialOpacity
    };

    const liveEventMarkerOptions = {
      ...eventMarkerOptions,
      fillColor: "#f93",
      className: "live-event"
    };

    this.pointsGeojson = this.L.geoJSON(pointsCollection, {
      pointToLayer: function (feature, latlng) {
        const about = feature.properties.about;
        const isLiveEvent =
          about &&
          about.startTime &&
          about.startTime <= date &&
          about.endTime &&
          about.endTime >= date;
        let marker;
        if (isLiveEvent) {
          marker = self.L.circleMarker(latlng, liveEventMarkerOptions);
        } else {
          marker = self.L.circleMarker(latlng, eventMarkerOptions);
        }
        return marker;
      }
    });
    this.pointsGeojson.addTo(this.map);

    this.pointsGeojson.on("click", (event) => {
      this.clickPoints(event);
    });

    this.pointsGeojson.on("mousemove", (event) => {
      this.mouseMove(event);
    });
  }

  handleClick(e) {
    // Necessary because otherwise event will be handled even if users want to
    // use buttons in front of map
    if (e.originalEvent.target.id !== "map") return;
    this.removeRegions();
    const { emitter } = this.props;
    emitter.emit("navigate", "/resource/?view=map&size=20&sort=dateCreated");
  }

  render() {
    const { initPins, iso3166, emitter, translate } = this.props;
    const { overlayList } = this.state;

    return (
      <div
        ref={(map) => {
          this.Map = map;
        }}
        id="map"
        onKeyDown={(e) => {
          if (e.keyCode === 27 && iso3166) {
            emitter.emit("navigate", "/resource/");
          }
        }}
        role="presentation"
      >
        {overlayList && <div className="overlayList" />}

        <QuickFilter />

        <div id="custom-map-elements">
          <TogglePoints emitter={emitter} initPins={initPins} translate={translate} />
          <MapLeyend emitter={emitter} iso3166={iso3166} translate={translate} />
        </div>

        <a className="imprintLink" href="/imprint">
          {translate("main.imprintPrivacy")}
        </a>
      </div>
    );
  }
}

Map.propTypes = {
  config: PropTypes.objectOf(PropTypes.any).isRequired,
  emitter: PropTypes.objectOf(PropTypes.any).isRequired,
  locales: PropTypes.arrayOf(PropTypes.any).isRequired,
  initPins: PropTypes.bool,
  iso3166: PropTypes.string,
  setCollapsed: PropTypes.func.isRequired,
  translate: PropTypes.func.isRequired,
  map: PropTypes.string,
  home: PropTypes.bool.isRequired,
  phrases: PropTypes.objectOf(PropTypes.any).isRequired,
  region: PropTypes.string
};

Map.defaultProps = {
  iso3166: null,
  map: null,
  region: null
};

export default withConfig(withI18n(Map));
