import React from "react";

import withI18n from "./withI18n";
import Link from "./Link";
import { formatDate } from "../common";

import "../styles/components/Topline.pcss";

const Topline = ({ translate, moment, about, className = "" }) => (
  <div className={`Topline ${className}`}>
    {about["@type"] === "Service" && about.provider && (
      <React.Fragment>
        <hr />
        <div className="toplineEntry">
          <span>
            {translate("Service.provider")}
            :&nbsp;
          </span>
          <ul className="commaSeparatedList">
            {about.provider.map((provider) => (
              <li key={provider["@id"]}>
                <Link href={`/resource/${provider["@id"]}`}>{translate(provider.name)}</Link>
              </li>
            ))}
          </ul>
        </div>
      </React.Fragment>
    )}

    {about["@type"] === "Event" && about.startTime && (
      <React.Fragment>
        <hr />
        <div className="toplineEntry">
          <span className="showInTooltip">
            {translate("Date")}
            :&nbsp;
          </span>
          <ul className="commaSeparatedList">
            <li>
              {formatDate(about.startTime, moment)}
              {about.endDate && (
                <span>
                  &nbsp;-&nbsp;
                  {formatDate(about.endTime, moment)}
                </span>
              )}
            </li>
          </ul>
        </div>
      </React.Fragment>
    )}

    {about["@type"] === "Event" &&
      about.location &&
      about.location[0] &&
      about.location[0].address && (
        <React.Fragment>
          <hr />
          <div className="toplineEntry">
            <span className="showInTooltip">
              {translate("Location:")}
              &nbsp;
            </span>
            <ul className="commaSeparatedList">
              <li>
                {about.location[0].address.addressLocality}
                {about.location[0].address.addressLocality &&
                  about.location[0].address.addressCountry && <span>,&nbsp;</span>}
                {translate(about.location[0].address.addressCountry)}
              </li>
            </ul>
          </div>
        </React.Fragment>
      )}

    {about.agent && about["@type"] === "Action" && (
      <React.Fragment>
        <hr />
        <div className="toplineEntry">
          <span>
            {translate("Action.agent")}
            :&nbsp;
          </span>
          <ul className="commaSeparatedList">
            {about.agent.map((agent) => (
              <li key={agent["@id"]}>
                <Link href={`/resource/${agent["@id"]}`}>{translate(agent.name)}</Link>
              </li>
            ))}
          </ul>
        </div>
      </React.Fragment>
    )}

    {about["@type"] === "Action" &&
      about.isFundedBy &&
      about.isFundedBy.some((grant) => grant.isAwardedBy) && (
        <React.Fragment>
          <hr />
          <div className="toplineEntry">
            <span>
              {translate("Action.isFundedBy")}
              :&nbsp;
            </span>
            <ul className="commaSeparatedList">
              {[]
                .concat(
                  ...about.isFundedBy
                    .filter((grant) => grant.isAwardedBy)
                    .map((grant) => grant.isAwardedBy)
                )
                .map((awarded) => (
                  <li key={awarded["@id"]}>
                    <Link href={`/resource/${awarded["@id"]}`}>{translate(awarded.name)}</Link>
                  </li>
                ))}
            </ul>
          </div>
        </React.Fragment>
      )}

    {about.inLanguage && (
      <div className="showInTooltip">
        <hr />
        <div className="toplineEntry">
          <span>
            {translate("inLanguage")}
            :&nbsp;
          </span>
          <ul className="commaSeparatedList">
            {about.inLanguage.map((lang) => (
              <li key={lang}>
                <Link href={`/resource/?filter.about.inLanguage=${lang}`}>{translate(lang)}</Link>
              </li>
            ))}
          </ul>
        </div>
      </div>
    )}

    {about.keywords && (
      <div className="showInTooltip">
        <hr />
        <div className="toplineEntry">
          <span>
            {translate("keywords")}
            :&nbsp;
          </span>
          <ul className="spaceSeparatedList">
            {about.keywords
              .sort((a, b) => a > b)
              .map((keyword) => (
                <li key={keyword}>
                  <Link
                    href={`/resource/?filter.about.keywords=${encodeURIComponent(keyword.toLowerCase())}`}
                  >
                    {keyword}
                  </Link>
                </li>
              ))}
          </ul>
        </div>
      </div>
    )}
  </div>
);

export default withI18n(Topline);
