import React from "react";
import { FilterContext } from "../contexts";
import { isNode } from "../common";

const FilterProvider = ({ children, translate }) => {
  const sorts = [
    {
      label: translate("ClientTemplates.filter.dateCreated"),
      dataField: "dateCreated",
      sortBy: "desc"
    },
    {
      label: translate("ClientTemplates.filter.relevance"),
      dataField: "_score",
      sortBy: "desc"
    },
    {
      label: translate("ClientTemplates.filter.alphabetical"),
      dataField: "about.name.en.sort",
      sortBy: "asc"
    },
    {
      label: translate("ClientTemplates.filter.lighthouseCount"),
      dataField: "lighthouse_count",
      sortBy: "desc"
    },
    {
      label: translate("ClientTemplates.filter.likeCount"),
      dataField: "like_count",
      sortBy: "desc"
    }
  ];

  const getUrlParams = () => {
    if (isNode()) {
      return {
        view: "list",
        size: 20,
        sort: sorts[0].dataField
      };
    }
    const url = new URL(window.location.href);
    return {
      view: url.searchParams.get("view") || "map",
      size: url.searchParams.get("size") || 20,
      sort: url.searchParams.get("sort") || sorts[url.searchParams.get("q") ? 1 : 0].dataField
    };
  };

  const setUrlParams = ({ view = "list", size = 20, sort = sorts[0].dataField }) => {
    const url = new URL(window.location.href);
    url.searchParams.set("view", view);
    url.searchParams.set("size", size);
    url.searchParams.set("sort", sort);
    window.history.pushState(null, null, url.href);
    window.dispatchEvent(new window.PopStateEvent("popstate"));
  };

  return (
    <FilterContext.Provider value={{ getUrlParams, setUrlParams, sorts }}>
      {React.Children.only(children)}
    </FilterContext.Provider>
  );
};

export default FilterProvider;
