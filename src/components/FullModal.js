/* global document */
import React from "react";

import Link from "./Link";
import withEmitter from "./withEmitter";

import "../styles/components/FullModal.pcss";

const FullModal = ({ className, children, closeLink, emitter }) => {
  const params = window.location.search;
  return (
    <div
      className={`FullModal ${className || ""}`}
      role="presentation"
      onClick={(e) => {
        const modalDialog = document.querySelector(".modalDialog");
        if (!modalDialog.contains(e.target)) {
          emitter.emit("navigate", closeLink || Link.home);
        }
      }}
    >
      <div className="modalDialog">
        {children}
        <Link href={`${closeLink}${params}` || Link.home} className="closeModal">
          &times;
        </Link>
      </div>
    </div>
  );
};

export default withEmitter(FullModal);
