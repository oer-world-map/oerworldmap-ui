import React from "react";

import withI18n from "./withI18n";
import Error from "./Error";

import "../styles/components/ErrorPage.pcss";

const ErrorPage = ({ intro, message, translate, details, title }) => {
  const header = title ?? translate("ClientTemplates.http_error.title");
  return (
    <div className="ErrorPage">
      <div className="ErrorPageContainer">
        <h3>{header}</h3>
        <Error error={{ message, details }} intro={intro} translate={translate}></Error>
      </div>
    </div>
  );
};

export default withI18n(ErrorPage);
