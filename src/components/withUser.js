import React, { useContext } from "react";
import { UserContext } from "../contexts";

const withUser = (BaseComponent) => {
  const UserComponent = (props, context) => {
    const user = useContext(UserContext);
    return <BaseComponent user={user} {...props} />;
  };

  return UserComponent;
};

export default withUser;
