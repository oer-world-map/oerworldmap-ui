import React, { useContext } from "react";
import { ConfigContext } from "../contexts";

const withConfig = (BaseComponent) => {
  const ConfigComponent = (props, context) => {
    const config = useContext(ConfigContext);
    return <BaseComponent config={config} {...props} />;
  };

  return ConfigComponent;
};

export default withConfig;
