import React, { useContext } from "react";
import { EmitterContext } from "../contexts";

const withEmitter = (BaseComponent) => {
  const EmitterComponent = (props, context) => {
    const emitter = useContext(EmitterContext);
    return <BaseComponent emitter={emitter} {...props} />;
  };

  return EmitterComponent;
};

export default withEmitter;
