/* global document */
/* global confirm */
/* global _paq */
import React, { useState, useEffect } from "react";
import { uniqueId } from "lodash";

import JsonSchema from "./JSONPointerForm/JsonSchema";
import Form from "./JSONPointerForm/Form";
import Builder from "./JSONPointerForm/Builder";
import validate from "./JSONPointerForm/validate";
import withFormData from "./JSONPointerForm/withFormData";

import withI18n from "./withI18n";
import withEmitter from "./withEmitter";
import withUser from "./withUser";
import Link from "./Link";

import expose from "../expose";
import { types } from "../common";

const WebPageEdit = ({
  about,
  emitter,
  translate,
  user,
  schema,
  closeLink,
  action = "edit",
  _self,
  showOptionalFields = true,
  onSubmit = (formData) => console.log(formData)
}) => {
  const [type, setType] = useState(about["@type"]);
  useEffect(() => setType(about["@type"]), [about]);

  const TypeSwitcher = withFormData(({ setValue }) => (
    <select
      value={type}
      onChange={(e) => {
        setType(e.target.value);
        setValue(e.target.value);
      }}
    >
      {types
        .filter((t) => t !== "Person")
        .map((type) => (
          <option key={type} value={type}>
            {translate(type)}
          </option>
        ))}
    </select>
  ));

  return (
    <Form
      data={about}
      validate={validate(JsonSchema(schema).get(`#/definitions/${type}`))}
      onSubmit={(data) => {
        if (_self && _self.includes("?add")) {
          typeof _paq !== "undefined" &&
            _paq.push(["trackEvent", "AddFormOverlay", "SubmitButtonClick"]);
        } else {
          typeof _paq !== "undefined" &&
            _paq.push(["trackEvent", "EditFormOverlay", "SubmitButtonClick"]);
        }
        onSubmit(data);
      }}
      onError={() => {
        document.querySelector(".error")?.scrollIntoView({ behavior: "smooth", block: "center" });
        const errorElements = document.getElementsByClassName("error");
        for (var i = 0; i < errorElements.length; i++) {
          const element = errorElements[i];
          element.classList.add("pulsating");
          setTimeout(() => element.classList.remove("pulsating"), 15000);
        }
      }}
    >
      {_self.endsWith("/user/profile") ? (
        <React.Fragment>
          <h2>{translate("main.myProfile")}</h2>
          <p>{translate("ResourceIndex.Person.edit.message")}</p>
          {user && !user.persistent && (
            <p>
              <Link href="/resource/" className="btn">
                {translate("main.skipProfile")}
              </Link>
            </p>
          )}
        </React.Fragment>
      ) : (
        <React.Fragment>
          <h2>
            {translate(action)}
            :&nbsp;
            {action === "edit" && expose("changeType", user, about) ? (
              <TypeSwitcher property="@type" />
            ) : (
              translate(type)
            )}
          </h2>
          <a href="/editorsFAQ" target="_blank" rel="noopener noreferrer" className="needHelp">
            {translate("needHelp")}
          </a>
        </React.Fragment>
      )}
      <Builder
        schema={JsonSchema(schema).get(`#/definitions/${type}`)}
        key={uniqueId()}
        showOptionalFields={showOptionalFields}
      />
      <p
        className="agree"
        dangerouslySetInnerHTML={{ __html: translate("ResourceIndex.index.agreeMessage") }}
      />

      <div className="formButtons">
        <div className="primaryButtons">
          <button className="btn prominent" type="submit">
            {translate("publish")}
          </button>
          <Link href={closeLink || Link.home} className="btn">
            {translate("cancel")}
          </Link>
        </div>
        {expose("deleteEntry", user, about) && (
          <button
            className="btn delete"
            type="button"
            onClick={(e) => {
              e.preventDefault();
              confirm(translate("other.deleteResource")) &&
                emitter.emit("delete", {
                  url: `/resource/${about["@id"]}`
                });
            }}
          >
            {translate("ResourceIndex.read.delete")}
          </button>
        )}
      </div>
    </Form>
  );
};

export default withI18n(withEmitter(withUser(WebPageEdit)));
