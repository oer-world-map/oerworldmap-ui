import React from "react";
import PropTypes from "prop-types";
import { EmitterContext } from "../contexts";

class EmittProvider extends React.Component {
  render() {
    const { children, emitter } = this.props;
    return (
      <EmitterContext.Provider value={emitter}>
        {React.Children.only(children)}
      </EmitterContext.Provider>
    );
  }
}

EmittProvider.propTypes = {
  children: PropTypes.node.isRequired,
  emitter: PropTypes.objectOf(PropTypes.any).isRequired
};

export default EmittProvider;
