import React from "react";
import { parseTemplate } from "url-template";

import Link from "./Link";
import Icon from "./Icon";
import withI18n from "./withI18n";

const ConceptBlock = ({ type, conceptScheme, typeLink, additionalTypeTemplate, translate }) => (
  <div className="ConceptBlock">
    <h3 className="border-bottom">
      <Link className="iconItem" href={typeLink}>
        <Icon type={type} />
        {translate(type)}
      </Link>
    </h3>
    {conceptScheme && (
      <ul className="linedList border-bottom">
        {conceptScheme.map((concept) => (
          <li key={concept["@id"]}>
            <Link href={parseTemplate(additionalTypeTemplate).expand(concept)}>
              {translate(concept.name)}
            </Link>
          </li>
        ))}
      </ul>
    )}
  </div>
);

export default withI18n(ConceptBlock);
