import React from "react";
import { parseTemplate } from "url-template";
import removeMd from "remove-markdown";

import Icon from "./Icon";
import Link from "./Link";

import "../styles/components/ItemList.pcss";

import withI18n from "./withI18n";
import { formatDate } from "../common";
import ResourceImage from "./ResourceImage";

const ItemList = ({
  translate,
  listItems,
  count,
  moment,
  linkTemplate = "/resource/{@id}",
  className = ""
}) => (
  <ul className={`ItemList linedList ${className}`}>
    {listItems
      .filter((listItem) => typeof listItem !== "undefined")
      .map((listItem) => (
        <li id={listItem["@id"]} key={listItem["@id"]}>
          <div className="item">
            {listItem.image || listItem.sameAs ? (
              <ResourceImage about={listItem} className="itemListImage" />
            ) : (
              <Icon type={listItem["@type"]} />
            )}
            <Link href={parseTemplate(linkTemplate).expand(listItem)}>
              <span>
                {translate(listItem.name) || translate(listItem["@id"])}
                {listItem.alternateName ? ` (${translate(listItem.alternateName)})` : ""}

                {listItem.description && (
                  <p className="itemListDescription">
                    {removeMd(translate(listItem.description)).slice(0, 200)}
                    ...
                  </p>
                )}

                {listItem["@type"] === "Event" && listItem.startTime ? (
                  <React.Fragment>
                    ,&nbsp;
                    <i aria-hidden="true" title={translate("Event.startTime")}>
                      {formatDate(listItem.startTime, moment)}
                    </i>
                  </React.Fragment>
                ) : (
                  ""
                )}
                {count && ` (${count(listItem)})`}
              </span>
            </Link>
          </div>
        </li>
      ))}
  </ul>
);

export default withI18n(ItemList);
