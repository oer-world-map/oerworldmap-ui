/* global document */
/* global window */
/* global ENVIRONMENT */
/* global LANG */
/* global $ */
/* global DOMParser */
/* global SUPPORTED_LANGUAGES */

import React from "react";
import { createRoot } from "react-dom/client";
import fetch from "isomorphic-fetch";
import mitt from "mitt";
import "normalize.css";
import { gsap } from "gsap/all";

import { updateUser } from "./common";
import Header from "./components/Header";
import Link from "./components/Link";
import I18nProvider from "./components/I18nProvider";
import EmittProvider from "./components/EmittProvider";
import UserProvider from "./components/UserProvider";
import i18n from "./i18n";
import ItemList from "./components/ItemList";
import Overview from "./components/Overview";
import dotenv from "dotenv";
import getenv from "getenv";
import getI18n from "./i18ns";

import "@fortawesome/fontawesome-free/css/all.css";
import "./styles/main.pcss";
import "./styles/static.pcss";
import "./styles/fonts.pcss";
import "./styles/components/Header.pcss";

dotenv.config();
const locales = [LANG];
const emitter = mitt();
const i18ns = getI18n(SUPPORTED_LANGUAGES);

const navigate = (url) => {
  const parser = document.createElement("a");
  parser.href = url;
  window.open(url, "_self");
};

emitter.on("navigate", navigate);

window.addEventListener("beforeunload", () => {
  emitter.off("navigate", navigate);
});

const injectHeader = (() => {
  function init() {
    Link.self = window.location.href;
    const target = document.querySelector("[data-inject-header]");

    if (target) {
      const root = createRoot(target);
      root.render(
        <I18nProvider i18n={i18n(locales, i18ns[locales[0]])}>
          <EmittProvider emitter={emitter}>
            <UserProvider>
              <Header locales={locales} />
            </UserProvider>
          </EmittProvider>
        </I18nProvider>
      );
    }
  }

  return { init };
})();

const animateScrollToFragment = (() => {
  const initOne = (one) => {
    one.addEventListener("click", (event) => {
      event.preventDefault();
      const id = one.hash.substr(1);
      const element = document.getElementById(id);
      element.scrollIntoView({
        behavior: "smooth",
        block: "start"
      });
      // See https://stackoverflow.com/a/1489802
      element.id = "";
      window.location.hash = one.hash;
      element.id = id;
    });
  };

  const init = () =>
    document.querySelectorAll("[data-animate-scroll-to-fragment]").forEach((el) => initOne(el));

  return { init };
})();

const injectStats = (() => {
  function init() {
    if (target) {
      const root = createRoot(document.createElement("div"));
      fetch(`/resource.json?size=0`)
        .then((response) => response.json())
        .then((json) => {
          root.render(<Overview buckets={json.aggregations["sterms#about.@type"].buckets} />);
        });
    }
  }

  return { init };
})();

const createAccordeon = (() => {
  const init = () => {
    if (window.location.pathname.includes("FAQ")) {
      const titles = document.querySelectorAll("h2");
      titles.forEach((title) => {
        const accordion = document.createElement("div");
        accordion.classList.add("accordion");

        const accordionContainer = document.createElement("div");
        accordionContainer.classList.add("accordionContainer");

        let currentChild = title.nextElementSibling;

        while (
          currentChild &&
          currentChild.nodeName !== "H2" &&
          currentChild.nodeName !== "SECTION"
        ) {
          const next = currentChild.nextElementSibling;
          accordionContainer.appendChild(currentChild);
          currentChild = next;
        }

        title.addEventListener("click", (e) => {
          document
            .querySelectorAll(".active")
            .forEach((active) => active.classList.remove("active"));
          e.target.parentElement.classList.toggle("active");
        });

        title.parentNode.insertBefore(accordion, title);

        accordion.appendChild(title);
        accordion.appendChild(accordionContainer);
      });
    }
  };

  return { init };
})();

const createPoliciesFeed = (() => {
  const init = async () => {
    if (window.location.pathname.includes("oerpolicies")) {
      // Request data for policies
      // ADD carry a tag called policy
      const rawResponse = await fetch(
        `/resource/?q=about.@type:"Policy"&sort=dateCreated:DESC&ext=json`,
        {
          headers: {
            accept: "application/json"
          }
        }
      );

      const content = await rawResponse.json();

      if (content) {
        const feedContainer = document.querySelector("[data-inject-feed]");
        const root = createRoot(feedContainer);
        root.render(
          <I18nProvider i18n={i18n(locales, i18ns[locales[0]])}>
            <EmittProvider emitter={emitter}>
              <ItemList listItems={content.member.map((member) => member.about)} />
            </EmittProvider>
          </I18nProvider>
        );
      }
    }
  };

  return { init };
})();

const createPolicyRelated = (() => {
  const init = async () => {
    if (window.location.pathname.includes("oerpolicies")) {
      const rawResponse = await fetch(
        `/resource/?q=NOT%20about.@type:"Policy"%20AND%20about.keywords:policy&sort=dateCreated:DESC&ext=json`,
        {
          headers: {
            accept: "application/json"
          }
        }
      );

      const content = await rawResponse.json();

      if (content) {
        const feedContainer = document.querySelector("[data-inject-policy-related]");
        const root = createRoot(feedContainer);
        root.render(
          <I18nProvider i18n={i18n(locales, i18ns[locales[0]])}>
            <EmittProvider emitter={emitter}>
              <ItemList listItems={content.member.map((member) => member.about)} />
            </EmittProvider>
          </I18nProvider>
        );
      }
    }
  };

  return { init };
})();

import("jquery").then(async (importedJquery) => {
  const { default: $ } = importedJquery;
  await updateUser();
  animateScrollToFragment.init();
  injectHeader.init();
  //injectStats.init()
  createAccordeon.init();
  createPoliciesFeed.init();
  createPolicyRelated.init();
});
