/* global Headers */

import fetch from "isomorphic-fetch";
import promise from "es6-promise";
import linkHeader from "http-link-header";

promise.polyfill();

export class APIError {
  constructor(message, status, details) {
    this.name = "APIError";
    this.message = message;
    this.status = status;
    this.details = details;
  }
}

const checkStatus = async (response) => {
  if (response.status >= 200 && response.status < 300) {
    return Promise.resolve(response);
  }
  if (response.status === 404) {
    return Promise.reject(new APIError(`Page not found: ${response.url}`, response.status));
  }
  try {
    const isJson = response.headers.get("content-type")?.includes("application/json");
    const details = isJson ? await response.json() : undefined;
    return Promise.reject(
      new APIError(`${response.url} returned ${response.statusText}`, response.status, details)
    );
  } catch (error) {
    if (error.name === "FetchError") {
      return Promise.reject(
        new APIError(`${response.url} returned ${error.name}`, response.status)
      );
    }
    return Promise.reject(
      new APIError(`${response.url} returned ${response.statusText}`, response.status)
    );
  }
};

const toJson = (response) =>
  response.json().then((json) => {
    if (response.headers.has("Link")) {
      json._links = linkHeader.parse(response.headers.get("Link"));
    }
    if (response.headers.has("Location")) {
      json._location = response.headers.get("Location");
    }
    // we can't access the publicConfig here, so just use relative URLs
    json._self = new URL(response.url).pathname;
    json._status = response.statusText;
    return json;
  });

class Api {
  constructor(config) {
    this.url = config.url;
  }

  // eslint-disable-next-line class-methods-use-this
  fetch(url, options) {
    return fetch(url, options).then(checkStatus).then(toJson);
  }

  post(url, data, headers = new Headers()) {
    headers.set("Content-Type", "application/json");
    headers.set("Accept", "application/json");
    return fetch(`${this.url}${url}`, {
      headers,
      method: "POST",
      mode: "cors",
      credentials: "include",
      body: JSON.stringify(data)
    })
      .then(checkStatus)
      .then(toJson);
  }

  get(url, headers = new Headers()) {
    headers.set("Accept", "application/json");
    return fetch(`${this.url}${url}`, {
      headers,
      method: "GET",
      mode: "cors",
      credentials: "include"
    })
      .then(checkStatus)
      .then(toJson);
  }

  delete(url, headers = new Headers()) {
    headers.set("Accept", "application/json");
    return fetch(`${this.url}${url}`, {
      headers,
      method: "DELETE",
      mode: "cors",
      credentials: "include"
    })
      .then(checkStatus)
      .then(toJson);
  }

  // eslint-disable-next-line class-methods-use-this
  vocab(url) {
    switch (url) {
      case "https://w3id.org/class/esc/scheme":
        return Promise.resolve({
          member: require("./json/esc.json").hasTopConcept.map((entry) => ({ about: entry }))
        });
      case "https://w3id.org/isced/1997/scheme":
        return Promise.resolve({
          member: require("./json/isced-1997.json").hasTopConcept.map((entry) => ({ about: entry }))
        });
      case "https://oerworldmap.org/assets/json/licenses.json":
        return Promise.resolve({
          member: require("./json/licenses.json").hasTopConcept.map((entry) => ({ about: entry }))
        });
      case "https://oerworldmap.org/assets/json/sectors.json":
        return Promise.resolve({
          member: require("./json/sectors.json").hasTopConcept.map((entry) => ({ about: entry }))
        });
      case "https://oerworldmap.org/assets/json/persons.json":
        return Promise.resolve({
          member: require("./json/persons.json").hasTopConcept.map((entry) => ({ about: entry }))
        });
      case "https://oerworldmap.org/assets/json/services.json":
        return Promise.resolve({
          member: require("./json/services.json").hasTopConcept.map((entry) => ({ about: entry }))
        });
      case "https://oerworldmap.org/assets/json/organizations.json":
        return Promise.resolve({
          member: require("./json/organizations.json").hasTopConcept.map((entry) => ({
            about: entry
          }))
        });
      case "https://oerworldmap.org/assets/json/projects.json":
        return Promise.resolve({
          member: require("./json/projects.json").hasTopConcept.map((entry) => ({ about: entry }))
        });
      case "https://oerworldmap.org/assets/json/activities.json":
        return Promise.resolve({
          member: require("./json/activities.json").hasTopConcept.map((entry) => ({ about: entry }))
        });
      case "https://oerworldmap.org/assets/json/policies.json":
        return Promise.resolve({
          member: require("./json/policies.json").hasTopConcept.map((entry) => ({ about: entry }))
        });
      case "https://oerworldmap.org/assets/json/policyTypes.json":
        return Promise.resolve({
          member: require("./json/policyTypes.json").hasTopConcept.map((entry) => ({
            about: entry
          }))
        });
      default:
        return Promise.resolve({
          member: []
        });
    }
  }
}

export default Api;
