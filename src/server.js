/* global Headers */

import { renderToPipeableStream } from "react-dom/server";
import path from "path";
import express from "express";
import compression from "compression";
import userAgent from "express-useragent";
import cookieParser from "cookie-parser";

import template from "./views/index";
import router from "./router";
import Api from "./api";
import getI18n from "./i18ns";
import i18n from "./i18n";
import { createGraph } from "./components/imgGraph";
import { logMessage } from "./common";

import Config, {
  internalApiConfig,
  uiConfig,
  matomoConfig,
  publicConfig,
  i18nConfig,
  elasticsearchConfig,
  internalElasticsearchConfig
} from "../config";

global.URL = require("url").URL;

const i18ns = getI18n();
const server = express();
const api = new Api(internalApiConfig);

const getHeaders = (headers) =>
  new Headers(
    Object.entries(headers).reduce((acc, [key, value]) => Object.assign(acc, { [key]: value }), {})
  );
//  .filter(([key]) => !key.startsWith('oidc'))

server.use((req, res, next) => {
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

server.use(compression());
server.use(cookieParser());

server.use(express.static(path.resolve(process.env.NODE_ENV === "production" ? "dist" : "dev")));

// Middleware to check browser support
server.use((req, res, next) => {
  const ua = req.headers["user-agent"] && userAgent.parse(req.headers["user-agent"]);
  (ua && ua.isIE && res.send("Sorry, your browser is not supported")) || next();
});

// Middleware to fetch labels
server.use((req, res, next) => {
  api
    .get("/label/")
    .then((labels) => {
      labels.results.bindings.forEach((label) => {
        i18ns[label.label["xml:lang"]] || (i18ns[label.label["xml:lang"]] = {});
        i18ns[label.label["xml:lang"]][label.uri.value] ||
          (i18ns[label.label["xml:lang"]][label.uri.value] = label.label.value);
      });
      next();
    })
    .catch((err) => logMessage(err, "error") || res.status(err.status).send(err.message));
});

// Middleware to fetch JSON schema
server.use((req, res, next) => {
  api
    .get("/assets/json/schema.json")
    .then((schema) => (req.schema = schema) && next())
    .catch((err) => {
      logMessage(err, "error");
      res.status(err.status).send(err.message);
    });
});

/**
 * @param {*} header Something like "en-US,en;q=0.5"
 * @returns Array of strings, e.g. ["de", "en"]
 */
const extractLanguagesFromHeaders = (header) => {
  const languages = [];
  const acceptLanguages = header.split(",");
  acceptLanguages.forEach((lang) => {
    const withoutWeight = lang.split(";")[0];
    // Split off language range like "-US"
    const withoutLanguageRange = withoutWeight.split("-")[0];
    if (!languages.includes(withoutLanguageRange)) {
      languages.push(withoutLanguageRange);
    }
  });
  return languages;
};

// Middleware to extract locales
const supportedLanguages = i18nConfig.supportedLanguages.trim().split(/\s+/);
const { defaultLanguage } = i18nConfig;

server.use((req, res, next) => {
  const requestedLanguages = [];
  if (req.headers["accept-language"]) {
    requestedLanguages.push(extractLanguagesFromHeaders(req.headers["accept-language"]));
  } else {
    requestedLanguages.push(defaultLanguage);
  }
  req.query && req.query.language && requestedLanguages.unshift(req.query.language);
  const locales = requestedLanguages.filter((language) => supportedLanguages.includes(language));
  if (!locales.includes(defaultLanguage)) {
    locales.push(defaultLanguage);
  }
  req.locales = locales;
  req.supportedLanguages = supportedLanguages;
  req.phrases = locales
    .slice(0)
    .reverse()
    .reduce((acc, curr) => Object.assign(acc, i18ns[curr]), {});

  next();
});

// Middleware to set public URL
server.use((req, res, next) => {
  const protocol = req.get("x-forwarded-proto") || req.protocol;
  // the header can contain multple hosts if the request forwarded multiple times, use the first host https://gitlab.com/oer-world-map/oerworldmap/-/issues/64
  const host = req.get("x-forwarded-host")?.split(", ")[0] || req.get("host");
  req.location = new URL(`${protocol}://${host}${req.originalUrl}`);
  next();
});

// Handle login
server.get("/.login", (req, res) => {
  if (req.headers.oidc_claim_profile_id) {
    res.redirect(req.query.continue ? req.query.continue : "/resource/");
  } else {
    res.redirect("/user/profile#edit");
  }
});

server.get("/stats", async (req, res) => {
  const { translate } = i18n(req.locales, req.phrases);
  const {
    field,
    q,
    subField,
    sub,
    size,
    subSize,
    include,
    subInclude,
    w,
    download,
    filename,
    basePath
  } = req.query;
  const filters = Object.entries(req.query)
    .filter(([param]) => param.startsWith("filter."))
    .map(([param, value]) => [param.replace(/^filter./, ""), JSON.parse(value)]);

  const image = await createGraph({
    field,
    q,
    subField,
    sub,
    size,
    subSize,
    translate,
    url: internalElasticsearchConfig.url,
    index: elasticsearchConfig.index,
    include,
    subInclude,
    filters,
    w,
    basePath
  });

  if (image) {
    res.setHeader("content-type", "image/svg+xml");
    if (download && filename) {
      res.setHeader("Content-Disposition", `attachment; filename=${filename}.svg`);
    }
    res.send(image);
  } else {
    res.sendStatus(404);
  }
});

// Server-side render request
server.get(/^(.*)$/, (req, res) => {
  const headers = getHeaders(req.headers);
  headers.delete("Host");
  headers.delete("If-None-Match");
  const { locales, supportedLanguages, phrases } = req;
  const config = {
    elasticsearchConfig,
    publicConfig
  };
  const { schema } = req;
  const context = {
    supportedLanguages,
    locales,
    headers,
    config,
    phrases,
    schema
  };
  // TODO: use actual request method
  router(api, null, req.location)
    .route(req.path, context)
    .get(req.query)
    .then(({ title, data, render, err, metadata }) => {
      console.info("Render from Server:", req.url);
      res.send(
        template({
          env: process.env.NODE_ENV,
          body: renderToPipeableStream(render(data)),
          initialState: JSON.stringify({
            supportedLanguages,
            config,
            locales,
            data,
            err,
            phrases,
            schema
          })
            .replace(/\u2028/g, "\\u2028")
            .replace(/\u2029/g, "\\u2029"),
          title,
          matomoConfig,
          metadata,
          locales
        })
      );
    });
});

server.listen(uiConfig.port, uiConfig.host, () => {
  console.info(`oerworldmap-ui server listening on http://${uiConfig.host}:${uiConfig.port}`);
});
