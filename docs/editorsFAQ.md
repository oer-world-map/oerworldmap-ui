---
title: FAQs for editors
layout: page
class: faq
---

# FAQs for OER World Map editors

## How do I log in?

[Please register](/.login) and choose a personal password. The registration and login mask can be found in the upper right corner of the screen. The registration includes getting editing rights for the OER World Map.

## What do I have to consider before I enter new data?

Before you enter new data, please make sure, that the data set is not yet included in the world map. In order to do so, please use the search function of the world map. You can also update and complete existing entries.

## How can I add or modify entries?

Please log in. Move your cursor to the menu item Add. A menu opens where you can enter the following data types:
- Organization
- Service
- Project
- Event
- Tool
- Policy

More information on our data types can be found on the [FAQ page](/FAQ) under "What belongs on the OER World Map?"

Clicking on one of the fields will open the corresponding template. After entering the data, please make sure that you click on "save data" at the lower end of the template.

In order to modify an existing data set, please navigate to the data set and click on the pen icon in the upper right.

## Which data can I add or modify?

All registered users are allowed to input all data types for all countries.

## Service, Organization or Project?

One lessons learned from our experience so far with mapping the world of OER is, that it is sometimes difficult to decide on the data type on an entry. Especially it can be hard to say if something is a service, an organization or a project.

The logical relation is as following: A service is an online offer that provides OER-related contents or functionalities (e.g. it helps finding, assessing, creating or publishing OER). The most important subsection of services are OER repositories. But there are many other kinds of services available (or thinkable) like authoring tools, search engines, community platforms, etc. A service is often run by a providing organization or in the case of grassroots by a person or a group of persons. Normally the service has been developed within a project, which is often run by the same organization, which later on provided the service. So if you aim at completeness, it can be necessary to record all three types.

Example: *DIPF | Leibniz Institut for Research and Information in Education* drives a project called "OERinfo". The project aims at further developing the service "OER World Map". Since the relaunch of the service the "OER World Map" is provided by OERinfo.

Nevertheless we believe that this is not always necessary. Rather it is preferable, to decide from case to case, which "degree of resolution" makes most sense. Indicators are:

We believe that services are the most important data types, because services provide direct value to users. So in most cases, it will be most important to record the service.

In most cases it will also make sense to capture the providing organization as well. This is especially true, if the organization is a major OER agent, which offers several services and/or runs multiple projects. As a rule, locations should be added to organizations. Once a service (or a project) is connected to an organization (via the "provider" field) the service is displayed on the map at the location of the providing organization.

Including the project makes special sense if either the project does not aim at delivering a service or has not delivered the service yet. It can also make sense to add a project, if you want to provide additional information, which is related specifically to the project, like its start and end date, its budget or its funder.

If you find it hard to decide on how to model a special constellation, as always, please do not hesitate to contact us at [support@oerworldmap.org](mailto:support@oerworldmap.org)!

## Should this service be on the map?

Sometimes it is difficult to decide if a service should be put on the map. To support your decision ask yourself following questions:
- Are the available resources explicitly designed for education?
- How many resources are available?
- Does the site offer an search function for it`s resources?
- Are the materials openly licensed?
- Does the service have a name and a dedicated URL, or is it just a general portal with some resources on it?
- How up-to-date are the materials and the website?
- Is the service in use or does it look rather inactive?

All this questions serve only as indicators. If in doubt, please contact us!

## How to add locations?

Under Location, click Show and select Country and Region. Use the address search or enter the location manually. An exact address is not necessary, but it improves the map view. If the exact address is not suggested, leave out the house number or just enter the city. Optionally, you can move the pin on the small map to the correct location and complete the address information under the map.

Assigning a location to services and projects is very quick: the point on the map then automatically refers to the organization that provides the service or carries out the project. Add the associated organization and link the service to the organization using the “Provider” field or the project using the “Operator” field.

An address can also be assigned to a service or a project; the address field is also available in the input forms of services and projects. In this way, a project e. g. focused on OER in Africa may appear on the map in Africa even though the implementing organization is located in another part of the world.

## How to add levels and subjects of education?

We are using [ISCED levels and ISCED based fields of education](https://uis.unesco.org/en/topic/international-standard-classification-education-isced) to describe services. This should allow all users to quickly find what they are looking for. Especially if the service offers materials for many different areas, it may make sense to limit yourself to the first level and skip the following levels.

## How can I include formatting to texts?

The "description" field for organizations, services can be formatted using common html tags or markdown syntax.

## How do I provide feedback to the developing team?

If you are registered on GitHub, you can easily provide feedback by [opening an issue](https://gitlab.com/oer-world-map/oerworldmap/-/issues/new) at. If you prefer contacting us by mail, we invite you to send your request to [support@oerworldmap.org](mailto:support@oerworldmap.org).

## How can I delete entries?

At the moment users cannot delete entries. If you created a data set by error and want to delete it, please send us a mail to [support@oerworldmap.org](mailto:support@oerworldmap.org).
