---
title: Mithelfen
layout: page
class: text
---

# Die OER World Map bekannter machen

## Schreiben und sprechen Sie über die OER World Map

Möglichst viele Menschen sollen erfahren, dass die OER World Map einen guten Überblick über OER-Akteure und -Aktivitäten gibt. Dazu müssen wir gemeinsam ein Netzwerk aufbauen. Bitte sprechen Sie mit Ihren Freunden und Kollegen über die OER World Map, persönlich oder über Social Media ([#oerworldmap](https://twitter.com/hashtag/oerworldmap)).

[Registrieren](/.login) Sie sich auf der OER World Map und erstellen Sie Ihr persönliches Profil. Lassen Sie uns gemeinsam OER-Akteure und Aktivitäten sammeln.

Fügen Sie [Folien zur OER World Map](assets/documents/OER World Map_DE.pptx) in ihre Präsentationen ein oder teilen Sie unsere [OER World Map Postkarte](/assets/documents/OER World Map_Postkarte_DE.pdf).

## Übersetzen Sie die Website

Um wirklich global genutzt werden zu können, muss die OER World Map in vielen Sprachen verfügbar sein. Wenn Sie die OER World Map in Ihre Muttersprache übersetzen können, nehmen Sie bitte über <support@oerworldmap.org> Kontakt mit uns auf.

# Daten sammeln und nutzen

## Daten auf der OER World Map sammeln

Wenn Sie Daten für die OER World Map sammeln möchten, können Sie diese direkt auf der Plattform über entsprechende Formulare eingeben. Dazu müssen Sie sich nur auf der World Map registrieren, dann können Sie loslegen. Lesen Sie auch die [FAQs für OER World Map Editoren](/editorsFAQ.de).

## Bereits gesammelte Daten auf die OER World Map hochladen

Vorhandene OER Daten können über die API der OER World Map hochgeladen werden. Wenn Sie bereits gesammelte Informationen haben, nehmen Sie bitte über [support@oerworldmap.org](mailto:support@oerworldmap.org) mit uns Kontakt auf.

## Daten exportieren

Über die API der OER World Map können Daten direkt exportiert werden. Die gesamten Daten oder ein herausgefilterter Datensatz können auf anderen Websites verwendet werden. Weitere detaillierte Informationen zu den Schnittstellen finden sie in der [API-Dokumentation](https://gitlab.com/oer-world-map/oerworldmap/-/blob/develop/docs/api.md). Wenn Sie Daten von der OER World Map weiterverwenden wollen, wenden Sie sich bitte per Mail über [support@oerworldmap.org](mailto:support@oerworldmap.org) an uns.

# Technische Unterstützung

## Fehler melden

Bitte melden Sie uns technische Fehler. Wenn Sie auf [Gitlab](https://gitlab.com/oer-world-map/oerworldmap/) registriert sind, können Sie direkt ein Ticket anlegen, andernfalls schreiben Sie uns an [support@oerworldmap.org](mailto:support@oerworldmap.org).

## Neue Funktionen vorschlagen

Die OER World Map wird agil entwickelt. Das bedeutet, dass Sie sich an der Entwicklung beteiligen können, indem Sie neue Funktionen vorschlagen. Wenn Sie eine Funktion vermissen, können Sie ein neues Ticket auf Gitlab anlegen oder uns eine Mail an [support@oerworldmap.org](mailto:support@oerworldmap.org) schicken.

## Schreiben Sie Code und Anwendungen für die OER World Map

Die OER World Map ist zu 100% offen, der gesamte Quellcode ist auf Gitlab verfügbar. Entwickler können an der Weiterentwicklung des Codes mitarbeiten oder Anwendungen entwickeln, die mit der OER World Map interagieren.
