---
title: FAQ
layout: page
class: faq
---

# FAQ

## What is OER?
'OER' stands for 'Open Educational Resources' and refers to freely accessible materials that can be used for a [range of activities around teaching and learning](https://www.opencontent.org/definition/). What makes them open is that they are typically published with an open license instead of a traditional copyright license. One key feature of OER is that they afford new opportunities for collaboration and innovation in teaching and learning.

According to the [UNESCO definition](http://www.unesco.org/new/fileadmin/MULTIMEDIA/HQ/CI/CI/pdf/Events/English_Paris_OER_Declaration.pdf), OER are “teaching, learning and research materials in any medium, digital or otherwise, that reside in the public domain or have been released under an open license that permits no-cost access, use, adaptation and redistribution by others with no or limited restrictions.”

Following the definition by [The William and Flora Hewlett Foundation](http://www.hewlett.org/programs/education/open-educational-resources) OER can be (or include) full courses, course materials, modules, textbooks, streaming videos, tests, software, and any other tools, materials, or techniques used to support access to knowledge.

Although comprehensive definitions like these are well-known, the detail of how to arrive at a final definition for OER remains debated. The OER World Map project takes a broad and inclusive approach, providing information about actors and activities from the field of OER in the strict sense of the term, but also about related subjects like Open Education, Open Access and MOOCs. For more information on our collection strategy, please see “What belongs on the OER World Map?”

## Who is this website for?
The OER World Map provides helpful information for everyone who is interested in or connected to OER and Open Education. The system is designed to provide real-time answers to questions like:
- "Where can I find OER?"
- "Which projects are working on a similar topic to our project?"
- "Which experts have the knowledge to answer a question related to a special area of OER?"

We believe that this set of data is of special importance, since it provides a solid foundation for future attempts to collect knowledge about OER and Open Education in a systematic way.

Our platform provides relevant data and information to all stakeholder groups from the field of OER. This diversity provides a major challenge for system design since user oriented interfaces are typically informed by the clearly defined needs of user types.

At the moment, we focus on three main use cases (or "epics"):
- Teachers and learners should be provided with qualified lists of OER services (e.g. OER repositories) and tools in order to help them finding and produce resources more efficiently;
- OER actors should be supported in their communication and collaboration through  connecting to relevant OER experts and projects working on similar tasks
- OER Policy makers should be supported by reliable statistical data.

In addition to browser access The OER World Map makes data available to other systems through an Application Programming Interface (API), which will allow other services to reuse our data easily.

Our use - which are used to design the services we provide - cases are collected on [Gitlab](https://gitlab.com/oer-world-map/oerworldmap/-/issues?q=is%3Aopen+is%3Aissue+label%3Astory). If you think anything is missing here, please [contact us](mailto:support@oerworldmap.org)!

## How can I participate?
The success of the project is dependent on the participation of the OER community.  There are many ways to contribute. The most important ways is collecting and entering data using our input templates.

There are many other ways of contributing to the project. A complete overview can be found [here](/contribute).

## How can I input and edit data?
To be able to enter data, you just have to register yourself to the OER World Map. You can find further information on how to use our input templates in our [FAQ for editors](/editorsFAQ).

## What belongs on the OER World Map?
The OER World Map collects data about actors and activities related to OER and Open Education. We concentrate on elements of OER infrastructure:

<table class="table">
<tr> <th>Data type</th><th>Definition</th> <th>Examples</th></tr>
<tr> <td>organizations</td><td>An organization is a steady group of people sharing collective goals. The spectrum reaches from fully incorporated legal persons on the one hand to loosely connected communities on the other.</td> <td>universities, corporations, associations, communities</td></tr>
<tr> <td>projects</td><td>A project is a temporary undertaking (within or across organizations) intended to accomplish particular tasks under time constraints.</td> <td>The OER World Map project</td></tr>
<tr> <td>services</td><td>A service is an online offer, which exists on a steady basis, and provides functionality and value related to OER to its users.</td> <td>repositories, aggregators, authoring tools, communication platforms</td></tr>
<tr> <td>events</td><td>An events is a gathering of people, which take place at a certain location (virtual or physical) at a certain time.</td><td>conferences, workshops, barcamps, moocs</td></tr>
</table>

## What do I have to keep in mind when I set a lighthouse?
The Ljubljana Action Plan highlights the need to identify models of good practice, which have proven to be sustainable ways to organize production, sharing and use of OER. We believe that learning from experiences made in other countries will be key for fast global mainstreaming of OER, so please consider setting some lighthouses.

Setting a lighthouse is pretty easy: When you are logged in, you simply click on the lighthouse symbol on the profile page of the entry you want to mark. Then you have to explain in the comment field what the community can learn from this particular example.

Please set lighthouses only in case you are not affiliated with the entry in any way!

## Where are the OER?
We believe that the discovery of individual OER has to be provided by search engine based services. The OER World Map facilitates the discovery of OER indirectly by providing qualified lists of OER services.

## What is an open license?
The widely accepted OER definitions by UNESCO and The William and Flora Hewlett Foundation require the use of a special ‘open’ license, which transforms a conventional educational resource into an open educational resource. Traditional copyright prescribes what can and can’t be done with materials that one has authored (or owns the rights to). Rather, it acknowledges and expresses these rights by recognising the author and prescribing specific conditions for use and reuse.

An open licence doesn’t undermine someone’s right to be recognised as the author of their work.  Rather, it recognises the author and sets out more specific conditions for use and reuse.

Depending on the specific license this allows for a range of behaviours known as the '5 Rs' of openness by David Wiley:

- **Retain** – the right to make, own, and control copies of the content
- **Reuse** – the right to use the content in a wide range of ways (e.g., in a class, in a study group, on a website, in a video)
- **Revise** – the right to adapt, adjust, modify, or alter the content itself (e.g., translate the content into another language)
- **Remix** – the right to combine the original or revised content with other open content to create something new (e.g., incorporate the content into a mashup)
- **Redistribute** – the right to share copies of the original content, your revisions, or your remixes with others (e.g., give a copy of the content to a friend)

A slightly different focus is provided by the [Open Definition](http://opendefinition.org/) which states that:
"Open data and content can be freely used, modified, and shared by anyone for any purpose"

Though there are many different open licenses available, [Creative Commons](https://creativecommons.org/licenses/) provides the by far most used open license family. We generally recommend the use of CC BY or CC BY-SA licenses for OER. Both licenses meet the strict requirement of the open definition and also have been approved for free cultural works by Creative Commons. More information about the use of open content licenses can be found [here](https://www.unesco.de/fileadmin/medien/Dokumente/Kommunikation/Open_Content_A_Practical_Guide_to_Using_Open_Content_Licences_web.pdf).

## Can I add services which provide non open resources to the map?
The most common OER definitions by UNESCO and by The William and Flora Hewlett Foundation compulsory require the use of open licenses. "Free" or "gratis" resources (resources which are accessible without any cost on the internet but not openly licensed) are not OER according to these definitions. We take openness very serious since the idea of reusing and modifying resources is central to the concept of OER. By focusing on openly licensed material the OER World Map provides real value to users, who are especially looking for reusable material.

Nevertheless, since the gratis offerings of today might become the openly licensed services of tomorrow, we don`t think that it makes sense to exclude non-open services dogmatically. Therefore, the OER World Map records and shares services providing material under a variety of licenses, with a minimum requirement that the material must be freely available online (“gratis”).

Projects are handled more flexibly than services. Many projects aim at goals related to Open Education in the wider sense and provide results which are valuable to the OER movement, even if they do not provide openly licensed materials. To avoid excluding valuable experiences, we believe that the OER World Map should be open to all projects which believe they make a contribution to the field of Open Education.

## Should MOOCs (Massive Open Online Courses) be added to the OER World Map?
This is an interesting question. The simple answer is "Yes!" But things are more complicated, if you look at it in more detail:

A criticism that has frequently been made is that many [MOOCs](https://en.wikipedia.org/wiki/Massive_open_online_course) are not using openly licensed material (which arguably makes them "free" or “gratis” but not really "open"). As stated above, the World Map can also include gratis services and materials. This is true for MOOCs as well.

One could alternatively argue that MOOCs are courses, which are using and providing OER (if open licenses are used), but that this does not make them OER themselves. Indeed it could be argued that MOOCs might be better classified as Open Educational Practices (OEP), than OER. Nevertheless, since there is an obvious connection between MOOCs and OER - and there is much interest in MOOCs - we believe that it makes sense to include MOOCs as well.

Concretely, the OER World Map offers the possibility of including MOOC providers as services and individual MOOCs as events.

## How will you use any information I share?
We believe data privacy is important. You can find information about our privacy policy [here](https://open-educational-resources.de/datenschutz/). We follow a privacy by design approach, which makes sure that data privacy is taken into account from the beginning of the development process. This is true for all personal data provided to us. Nevertheless it has to be understood that all data published on the platform, including your personal profiles will be available to everyone as [open data](https://en.wikipedia.org/wiki/Open_data).

## Will the OER World Map be available in several languages?
The initial version of the website was developed in English. A German translation is available.Nevertheless, we believe that the site should be available in many languages and the system is prepared to support multilingualism. At the time being the platform supports data input in all major languages of the world and the system will show you data within the language chosen in your browser, if available. If you are interested in translating the website into your own language, please [contact us](mailto:support@oerworldmap.org).

## Why are countries displayed in different shades of green?
Countries with documented OER activity are displayed green. The more activity there is, the darker the colour. We call this mechanism “heat map”. The heat map is based right now on a very simple algorithm: the number of entries. We are thinking about refining the algorithm in the future (e.g. by representing the number of entries into relation to the number of inhabitants).
