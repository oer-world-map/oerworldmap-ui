---
title: About
layout: page
class: text
---

# About the OER World Map

The OER World Map has been collecting and illustrating data on the growing number of actors and activities in the field of open education worldwide since 2014. Its aim is to support the development of the OER ecosystem by showing the most comprehensive and up-to-date picture of the OER movement possible.

The OER World Map records and shows relevant people, projects and organizations, services and tools, as well as events and policies. The information presented can be used for a variety of purposes, for example to:

- provide qualified lists of repositories and other OER services and tools to help teachers and learners find and create suitable educational materials;
- connect different actors to facilitate collaboration and share knowledge and resources;
- support decision-makers in making and representing strategic decisions through meaningful statistics and overviews of the OER movement and its impact.

The OER World Map provides information for the self-organized development of the OER movement. As a whole, the OER World Map can be seen as a kind of "monitoring tool" for the Open Education community.

<img style="width:100%;" src="/assets/images/open-control-room.jpg" title="Die OER World Map als &quot;Steuerzentrale&quot; für die Open Education Community" />

Since 2023, the OER World Map, funded by the [Federal Ministry of Education and Research](https://www.bmbf.de/bmbf/en/home/home_node.html), is further developed by [OERinfo](https://open-educational-resources.de/) and [Intevation GmbH](https://www.intevation.de/index.en.html).

Until 2022, the construction and operation, funded by the [William and Flora Hewlett Foundation](https://hewlett.org/), was carried out by [hbz](https://www.hbz-nrw.de/) and [graphthinking GmbH](http://www.graphthinking.com) in cooperation with the [Open University (UK)](http://www.open.ac.uk/).

<p style="vertical-align: middle; margin: 1em auto;">
<img src="assets/images/oer_info_logo_4c.jpg" style="width: 30%" />
<img src="assets/images/Gefördert_vom_BMBF.svg" style="width:20%; margin: 0 2.5%" />
</p>

## About the platform

The OER World Map platform uses software and services provided or developed by external parties:

- The map view is based on the Software [leafletjs](https://leafletjs.com/)
- Country and region map data is derived from [Natural Earth](https://www.naturalearthdata.com/)
- Address search is provided by [Nominatim](https://nominatim.org/)
