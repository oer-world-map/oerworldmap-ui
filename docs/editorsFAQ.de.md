---
title: FAQs für Editoren
layout: page
class: faq
---

# FAQs für OER World Map Editoren

## Wie logge ich mich ein?

Um sich zu registrieren und später einzuloggen verwenden Sie den Login-Button oben rechts. Bitte [registrieren Sie sich](/.login) und wählen Sie ein Passwort. Wenn Sie registriert sind, haben Sie automatisch Schreibrechte auf der OER World Map.

## Was muss ich beachten, bevor ich neue Daten eingebe?

Bevor Sie neue Daten eingeben, prüfen Sie bitte, ob der Eintrag, den Sie anlegen möchten, auf der OER World Map nicht bereits vorhanden ist. Verwenden Sie dazu bitte die Suchfunktion der Map. Sie können bestehende Einträge auch aktualisieren und vervollständigen.

## Wie kann ich Einträge hinzufügen oder verändern?

Loggen Sie sich ein. Bewegen Sie ihren Cursor auf den Menüpunkt hinzufügen. Es öffnet sich ein Menü, über das Sie die folgenden Datentypen eingeben können:
- Organisation
- Service
- Projekt
- Veranstaltung
- Tool
- Policy

Informationen zu unseren Datentypen finden Sie auf der [FAQ-Seite](/FAQ) unter "Was gehört auf die OER World Map?"

Wenn Sie auf einen der Punkte klicken, öffnet sich das zugehörige Eingabe-Formular. Bitte stellen Sie sicher, nach der Dateneingabe am unteren Ende des Formulars auf "Veröffentlichen" zu klicken.

Um einen bereits vorhandenen Datensatz zu bearbeiten, navigieren Sie zu diesem Datensatz und klicken Sie auf das kleine Stift-Icon rechts oben.

## Welche Daten kann ich hinzufügen oder ändern?

Alle registrierten Nutzer dürfen alle Datentypen für alle Länder anlegen und ändern.

## Service, Organisation oder Projekt?

Unsere Erfahrung beim Eingeben von Daten in die OER World Map hat uns gelehrt, dass es oft schwierig ist zu entscheiden, welchem Datentyp ein Eintrag zugeordnet werden soll. Besonders schwierig ist es manchmal zu unterscheiden, ob etwas als Service, als Organisation oder als Projekt eingetragen werden sollte.

Die Logik hinter dieser Unterteilung ist die Folgende: Ein Service ist ein Web-Angebot, das OER-Inhalte oder OER-bezogene Funktionalitäten bereit stellt (z.B. hilft es OER zu finden, zu bewerten, zu erstellen oder zu veröffentlichen). Die wichtigsten Typen von Services sind OER-Repositorien. Aber es gibt noch viele andere Arten von Services wie Autorentools, Suchmaschinen, Community-Plattformen etc. Ein Service wird oft betrieben von einer Organisation (oder bei Graswurzel-Projekten von einer Person oder einer Gruppe von Personen). Normalerweise wurde ein Service während eines Projektes entwickelt, das von der gleichen Organisation durchgeführt wurde, die später den Service zur Verfügung stellt. Deshalb ist es häufig im Sinne der Vollständigkeit notwendig, alle drei Datentypen einzugeben.

Als Beispiel: Das DIPF | Leibnizinstitut für Bildungsforschung und Bildungsinformation führt das Projekt "OERinfo" durch. Das Projekt zielt darauf ab, den Service "OER World Map" weiterzuentwickeln. Seit dem Relaunch  wird der Service "OER World Map" von OERinfo bereit gestellt.

Allerdings glauben wir nicht, dass alle drei Datentypen in jedem Falle notwendig sind. Entscheiden Sie von Fall zu Fall was sinnvoll ist. Indikatoren sind:

Wir halten Services für den wichtigsten Datentyp, weil sie für die meisten Besucher direkt von Nutzen sind. In den meisten Fällen ist es folglich am wichtigsten, einen Service einzutragen.

In den meisten Fällen wird es sinnvoll sein, die zugehörige Organisation ebenfalls einzutragen. Das ist besonders dann der Fall, wenn die Organisation eine der großen OER-Organisationen ist, die verschiedene Services anbieten bzw. mehrere Projekte durchführen. Eine wichtige Regel ist, die Adresse sollte bei der Organisation eingegeben werden. Sobald ein Service (oder ein Projekt) mit einer Organisation verbunden ist (über das Feld „Anbieter“), wird der Dienst auf der Karte am Standort der bereitstellenden Organisation angezeigt.

Es ist dann besonders sinnvoll ein Projekt einzutragen, wenn das Projekt entweder nicht darauf abzielt, einen Service zur Verfügung zu stellen, oder wenn es den Service noch nicht bereit stellt. Außerdem kann es sinnvoll sein, ein Projekt einzutragen, wenn projektspezifische Informationen dokumentiert werden sollen wie Projektstart und -ende, das Budget oder der Förderer.

Wenn Sie Schwierigkeiten haben, eine bestimmte Konstellation auf der OER World Map abzubilden, zögern Sie nicht, sich an uns zu wenden unter [support@oerworldmap.org](mailto:support@oerworldmap.org).

## Sollte dieser Service auf die OER World Map?

Manchmal ist es schwierig zu entscheiden, ob ein Service in die OER World Map eingetragen werden sollte oder nicht. Stellen Sie sich die folgenden Fragen, um sich die Entscheidung zu erleichtern:

- Sind die zur Verfügung gestellten Ressourcen explizit für die Bildung entwickelt worden?
- Wie viele Ressourcen sind verfügbar?
- Gibt es eine Suchfunktion für die Ressourcen auf der Website?
- Sind die Materialien offen lizenziert?
- Hat der Service einen Namen und eine eigene URL oder handelt es sich nur um ein allgemeines Portal mit einigen Ressourcen?
- Wie aktuell sind die Materialien auf der Website?
- Wird der Service genutzt?

Diese Fragen können als Indikatoren dienen, im Zweifel, nehmen Sie Kontakt zu uns auf.

## Wie ordne ich Einträgen einen Ort auf der Karte zu?

Klicken Sie unter Ort auf zeigen und Wählen Sie Land und Region aus. Nutzen Sie die Adresssuche oder geben Sie den Standort manuell ein. Eine exakte Adressangabe ist nicht notwendig, verbessert aber die Kartenansicht. Wenn die exakte Adresse nicht vorgeschlagen wird, lassen Sie die Hausnummer aus oder geben Sie nur die Stadt ein. Optional können Sie den Pin auf der kleinen Karte an die richtige Stelle verschieben und die Adressangaben unter der Karte vervollständigen.

Services und Projekten einen Ort zuordnen, geht dann ganz schnell: Der Punkt auf der Karte bezieht sich dann automatisch auf die Organisation, die den Service zur Verfügung stellt oder das Projekt durchführt. Die zugehörige Organisation hinzuzufügen und den Service mit der Organisation über das "Anbieter"-Feld, bzw. das Projekt über das "Betreiber"-Feld verlinken.

Eine Adresse kann aber auch einem Service oder einem Projekt zugeordnet werden, dafür gibt es das Adressfeld auch in den Eingabeformularen von Services und Projekten. Auf diese Weise kann z. B. ein Projekt, das sich mit OER in Afrika beschäftigt, auf der Karte in Afrika angezeigt werden, obwohl sich die durchführende Organisation in einem anderen Teil der Welt befindet.

## Wie füge ich Fächer und Bildungsstufen hinzu?

Da es sich bei der OER World Map um eine Weltkarte handelt, verwenden wir die [internationalen Standards (ISCED) der UNESCO](https://uis.unesco.org/en/topic/international-standard-classification-education-isced) zu Fächern und Bildungsstufen. Damit sollten alle Nutzer schnell finden, was sie suchen. Besonders wenn der Service Materialien für viele verschiedene Bereiche anbietet, kann es sinnvoll sein, sich auf die erste Ebene zu beschränken und die folgenden Ebenen auszulassen.

## Wie kann ich Text formatieren?

Die Beschreibungen für Organisationen, Projekte und Services können mit HTML-Tags oder über die Markdown-Syntax formatiert werden.

## Wie kann ich dem Entwickler-Team Feedback geben?

Wenn Sie bei GitHub registriert sind, können Sie Feedback geben, indem sie ein [Ticket öffnen](https://gitlab.com/oer-world-map/oerworldmap/-/issues/new). Oder schicken Sie uns ihr Feedback an [support@oerworldmap.org](mailto:support@oerworldmap.org).

## Wie kann ich Einträge löschen?

Im Moment können Nutzer keine Einträge löschen. Wenn Sie aus Versehen einen Datensatz angelegt haben und ihn löschen wollen, schicken Sie uns eine Mail an [support@oerworldmap.org](mailto:support@oerworldmap.org).
