---
title: Imprint
layout: page
class: text
---
# Contact and Imprint
## Contact
We welcome feedback, questions and relevant information. You can reach our support via [support@oerworldmap.org](mailto:support@oerworldmap.org).

## Imprint
The OER World Map is an offer from [OERinfo | Information Service Open Educational Resources](https://open-educational-resources.de/ueber-oerinfo/about-the-information-service-oer/) at the [German Education Server](https://www.eduserver.de/) (DIPF | Leibniz Institute for Research and Information in Education).

**DIPF \| Leibniz Institute for Research and Information in Education**

**Public law foundation**

Rostocker Straße 6\\
60323 Frankfurt am Main\\
Tel. +49 (0) 69 24708-0\\
Fax +49 (0) 69 24708-444\\
Email: [info@dipf.de](mailto:info@dipf.de)\\
Website: [www.dipf.de](https://www.dipf.de)

**Value added tax identification number: DE114237569**

Legal representation: Executive Director Prof. Dr. Kai Maaz, Deputy Executive Director Prof. Dr. Sabine Reh, Managing Director Susanne Boomkamp-Dahmen.

The [DIPF \| Leibniz Institute for Research and Information in Education](https://www.dipf.de/en/frontpage?set_language=en) is a public law foundation. DIPF supports researchers, policy-makers and practitioners in education by providing scientific infrastructure and diverse research activities. DIPF is a member of the [Leibniz Association](https://www.leibniz-gemeinschaft.de/en.html), and as such links knowledge-driven fundamental research with innovative developmental work and applications – for the benefit of society and its members.

## Editing and coordination of this website
Susanne Grimm ([OERinfo | Information Service Open Educational Resources](https://open-educational-resources.de/ueber-oerinfo/about-the-information-service-oer/))

## Realization and programming of this website
[Intevation GmbH](https://intevation.de/), Coordination: Sebastian Wagner
