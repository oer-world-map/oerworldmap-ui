---
title: Über uns
layout: page
class: text
---

# Über die OER World Map

Seit 2014 sammelt und veranschaulicht die OER World Map Daten zur wachsenden Zahl der Akteure und Aktivitäten im Bereich der offenen Bildung weltweit. Ihr Ziel ist es, die Entwicklung des OER-Ökosystems zu unterstützen, indem sie ein möglichst umfassendes und aktuelles Bild der OER-Bewegung zeigt.

Die OER World Map erfasst und zeigt relevante Personen, Projekte und Organisationen, Services und Tools,  sowie Veranstaltungen und Policies. Die dargestellten Informationen können für ganz verschiedene Zwecke verwendet werden, beispielsweise um:

- qualifizierte Listen von Repositorien und anderen OER-Services und Tools bereitzustellen, um Lehrenden und Lernenden zu helfen, passende Bildungsmaterialien zu finden und zu erstellen;
- verschiedene Akteure miteinander zu vernetzen, um die Zusammenarbeit zu erleichtern und um Wissen und Ressourcen zu teilen;
- Entscheider durch aussagekräftige Statistiken und Überblicksdarstellungen zur OER-Bewegung und deren Auswirkungen dabei zu unterstützen, strategische Entscheidungen zu treffen und zu vertreten.

Die OER World Map stellt Informationen für die selbstorganisierte Entwicklung der OER-Bewegung bereit. Als Ganzes kann die OER World Map als eine Art "Monitoring-Instrument" für die Open Education Community gesehen werden.

<img style="width:100%" src="/assets/images/open-control-room.jpg" title="Die OER World Map als &quot;Steuerzentrale&quot; für die Open Education Community" />

Seit 2023 wird die OER World Map von [OERinfo](https://open-educational-resources.de/) und [Intevation GmbH](https://www.intevation.de/) weiterentwickelt und gefördert durch das [Bundesministerium für Bildung und Forschung](https://www.bmbf.de).

Die OER World Map wurde zuvor von [hbz](http://www.hbz-nrw.de) und [graphthinking GmbH](http://www.graphthinking.com) in Verbindung mit der [Open University (UK)](http://www.open.ac.uk/) entwickelt und gefördert von der [William and Flora Hewlett Foundation](http://www.hewlett.org/).

<p style="vertical-align: middle; margin: 1em auto;">
<img src="assets/images/oer_info_logo_4c.jpg" style="width: 30%" />
<img src="assets/images/Gefördert_vom_BMBF.svg" style="width:20%; margin: 0 2.5%" />
</p>

## Über die Anwendung

Die OER World Map Anwendung nutzt Programme und Services die von Externen bereitgestellt oder betrieben werden:

- Die Kartenansicht basiert auf [leafletjs](https://leafletjs.com/)
- Die Kartendaten (Länder und Regionen) basieren auf [Natural Earth](https://www.naturalearthdata.com/)
- Die Adresssuche wird bereitgestellt von [Nominatim](https://nominatim.org/)
