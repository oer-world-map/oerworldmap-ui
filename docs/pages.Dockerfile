FROM jekyll/jekyll:4.2.0

WORKDIR /srv/oerworldmap-ui

gem install jekyll-environment-variables

ENTRYPOINT [ "jekyll", "serve", "--watch", "--incremental" ]
