---
title: Contribute
layout: page
class: text
---

# Raise awareness

## Spread the news
People need to know about the OER World Map if it is to provide a comprehensive overview of OER people and activities.  We need to build the network together, so please speak to your friends and colleagues or use social media to reach out more widely (use [#oerworldmap](https://twitter.com/hashtag/oerworldmap)).

[Register](/.login) to the map and create your profile. Lets collect OER actors and activities together.

Include [OER World Map slides](assets/documents/OER World Map_EN.pptx) in your presentations or share our [OER World Map postcard](/assets/documents/OER World Map_postcard_EN.pdf).

## Translate the web pages
To become a truly global resource, OER World Map web site pages need to be available in multiple languages. If you would be able to translate web pages into your native language please let us know at [support@oerworldmap.org](mailto:support@oerworldmap.org).

# Build and use the data

## Become an editor and contribute data
If you are interested in collecting data for the OER World Map, you can use templates to edit data directly on the platform. In order to do this, just register on the platform and start entering data. Please have a look at the [FAQs for OER World Map editors](/editorsFAQ).

## Upload existing data
Existing OER data can be uploaded to the OER World Map platform using a powerful API. If you think you have relevant information to add, please contact us at [support@oerworldmap.org](mailto:support@oerworldmap.org).

## Export data
The API of the OER World Map can be used to generate and export data with no delay.  Complete or filtered data can be used in other web sites. Further detailed informations on the interface are in the [API documentation](https://gitlab.com/oer-world-map/oerworldmap/-/blob/develop/docs/api.md). If you would like to reuse OER World Map data, please contact us at [support@oerworldmap.org](mailto:support@oerworldmap.org).

# Technical input

## Report bugs
Please report any bugs you identify by opening a new ticket if you are registered on [Gitlab](https://gitlab.com/oer-world-map/oerworldmap/). Otherwise you can send a message to [support@oerworldmap.org](mailto:support@oerworldmap.org).

## Propose new features
The OER World Map has been developed using an agile approach. This means that you can contribute to its development by proposing new features. We store new features [on GitLab](https://gitlab.com/oer-world-map/oerworldmap/-/issues). If you think something is missing, you can either directly open a new ticket on Gitlab or send us a message to [support@oerworldmap.org](mailto:support@oerworldmap.org).

## Contribute code and new applications
The OER World Map is 100% open – its complete source code is available on GitLab.  Developers can contribute to the code or even develop applications that interact with the OER World Map.
