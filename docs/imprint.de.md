---
title: Imprint
layout: page
class: text
---
# Kontakt und Impressum
## Kontakt
Wir freuen uns über Feedback, Fragen und sachdienliche Hinweise. Sie erreichen unseren support via [support@oerworldmap.org](mailto:support@oerworldmap.org).

## Impressum
Die OER World Map ist ein Angebot von [OERinfo \| Informationsstelle Open Educational Resources](https://www.o-e-r.de) am [Deutschen Bildungsserver](https://www.bildungsserver.de/) (DIPF | Leibniz-Institut für Bildungsforschung und Bildungsinformation).

**DIPF \| Leibniz-Institut für Bildungsforschung und Bildungsinformation**

**Stiftung des Öffentlichen Rechts**

Rostocker Str. 6\\
D-60323 Frankfurt am Main\\
Tel. +49 (0)69 2 47 08-0\\
Fax +49 (0)69 2 47 08-444\\
E-Mail: [info@dipf.de](mailto:info@dipf.de)\\
Internet: [www.dipf.de](https://www.dipf.de)

**Umsatzsteuer-Identifikationsnummer: DE114237569**

Gesetzliche Vertretung: Geschäftsführender Direktor Prof. Dr. Kai Maaz, stellvertretende Geschäftsführende Direktorin Prof. Dr. Sabine Reh, Geschäftsführerin Susanne Boomkamp-Dahmen.

Das [DIPF \| Leibniz-Institut für Bildungsforschung und Bildungsinformation](https://www.dipf.de) ist eine Stiftung des öffentlichen Rechts. Das DIPF unterstützt Forscher, politische Entscheidungsträger und Praktiker in der Bildung durch die Bereitstellung wissenschaftlicher Infrastruktur und vielfältiger Forschungsaktivitäten. Das DIPF ist Mitglied der [Leibniz-Gemeinschaft](https://www.leibniz-gemeinschaft.de/) und verknüpft als solche erkenntnisbasierte Grundlagenforschung mit innovativen Entwicklungsarbeiten und Anwendungen – zum Wohle der Gesellschaft und ihrer Mitglieder.

## Redaktion und Koordination
Susanne Grimm ([OERinfo \| Informationsstelle Open Educational Resources](https://www.o-e-r.de))

## Realisation und Programmierung dieser Internetseite
[Intevation GmbH](https://intevation.de/), Koordination: Sebastian Wagner
