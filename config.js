import dotenv from 'dotenv'
import getenv from 'getenv'

// load env file
dotenv.config()

export const elasticsearchConfig = getenv.multi({
  index: 'ELASTICSEARCH_INDEX',
  url: 'ELASTICSEARCH_URL',
})

export const internalElasticsearchConfig = getenv.multi({
  url: ['INTERNAL_ELASTICSEARCH_URL', "http://elasticsearch:9200"]
})

export const publicConfig = getenv.multi({
  url: 'PUBLIC_URL',
})

export const internalApiConfig = getenv.multi({
  url: ['INTERNAL_API_URL', 'http://scala:9000'],
})

export const uiConfig = getenv.multi({
  host: 'UI_SERVER_HOST',
  port: ['UI_SERVER_PORT', ''],
})

export const matomoConfig = getenv.multi({
  id: ['MATOMO_ID', ''],
  url: ['MATOMO_URL', ''],
})

export const i18nConfig = getenv.multi({
  supportedLanguages: ['LANG_SUPPORTED', 'en'],
  defaultLanguage: ['LANG_DEFAULT', 'en'],
})

// export server configuration
export default getenv.multi({
  env: 'NODE_ENV',
})
